import 'package:buchi/providers/auth.dart';
import 'package:buchi/screen/home/home_screen.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/dialog/content_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:buchi/models/http_exception.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  
final GlobalKey<FormState> _formKey = GlobalKey();
var usernameController = TextEditingController();
final passwdController = TextEditingController();
bool _isHidePassword = true;
bool _isLoading = false;
TextFormField email;

@override
void initState() {
    // TODO: implement initState
    email = TextFormField(  
    keyboardType: TextInputType.emailAddress,
    controller: usernameController,
    autofocus: false,
    validator: (value) {
    if (value.isEmpty) {
        return 'Please enter email address';
      }
      return null;
    },  
    style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
    decoration: InputDecoration(
      labelText: 'Email',
      hintText: 'Email',
      contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),    
    );
    super.initState();
}


    
void navigationPage() { 
  // Navigator.of(context).pushReplacementNamed('/home');
  Navigator.push(context,
  MaterialPageRoute(builder: (context) => HomeScreen()
  ));
 }

 void _togglePasswordVisibility(){
   setState(() {
     _isHidePassword = !_isHidePassword;
   });
 }


 Future<void> _submit() async{
    final username = usernameController.text;
    final password = passwdController.text;   
    print("user $username  pass $password");
    if(!_formKey.currentState.validate()){
      return;
    }

    _formKey.currentState.save();
    setState(() {
      _isLoading = true; 
    });
    Navigator.of(context).pushReplacementNamed('/home');
      // try {       
      //     await Provider.of<Auth>(context, listen: false).login(
      //     username, 
      //     password).then((val) {                    
      //       DataConfig().setLogin(true);    
      //       print("sukses login ${val.accessToken}");      
      //       if(val.accessToken != null){
      //         Navigator.of(context).pushReplacementNamed('/home');
      //       }else{
      //           showGeneralDialog(
      //           barrierLabel: "Label",
      //           barrierDismissible: true,           
      //           transitionDuration: Duration(milliseconds: 700),
      //           context: context,
      //           pageBuilder: (context, anim1, anim2) {
      //             return ContentDialog();
      //           },
      //                 transitionBuilder: (context, anim1, anim2, child) {
      //                   return SlideTransition(
      //                     position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
      //                     child: child,
      //                   );
      //                 },
      //         );    
      //       }            
                                   
      //     })
      //     .catchError((Object error) {
      //        print("Error boy $error"); 
      //         Utils().getMessage().then((val)
      //         { 
      //             print("Error Login $val");                                       
      //         });

      //     });        
      
      // } on HttpException catch (error) {
      //   print("http exception"+ error.toString());
      // } on Exception catch (error){
      //   print("exception"+ error.toString());
      // }
      // catch (error) {
      //   setState(() {
      //       _isLoading = false;
      //   });      
                
      //   print("Catch Err "+error.toString()); 
      //   Utils().getMessage().then((val)
      //   { 
      //       print("Error Login $val");                                     
      //   });     
        
      // }

    setState(() {
      _isLoading = false;
    });    
  }

  @override
  Widget build(BuildContext context) {
    return 
    Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
      children: <Widget>[    
      Container(
      color : Color.fromRGBO(148, 205, 12, 100),
      ),    
      Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Card(
            child :  Container(  
                          height: Utils().screenHeight(context, 85),                   
                          padding: EdgeInsets.only(right: 30.0, left : 30.0, top: 30.0, bottom: 0.0), 
                              child: Form(
                              key: _formKey, 
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(bottom : 8.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,    
                                    child : Center(
                                      child: Image.asset("assets/images/buchi_logo2.png",
                                                          width: Utils().width(context),
                                                          height: Utils().heightDivided(context, 14),),
                                    )
                                  ),
                                ), 

                                Align(
                                  alignment: Alignment.centerLeft,    
                                  child : Center(
                                    child: (
                                      Padding(
                                      padding: const EdgeInsets.only(bottom: 100,top: 20),
                                      child : Text('LOG IN', 
                                            style: TextStyle(fontWeight: FontWeight.bold,
                                                            fontSize: Utils().heightDivided(context, 20),)
                                            ),
                                      )
                                    ),
                                  ),
                                ),  
                                                    
                        
                                Padding(
                                  padding: const EdgeInsets.only(left : 8.0, bottom: 30.0),
                                  child: Text("Email Address", 
                                   style: TextStyle(
                                     fontSize: Utils().heightDivided(context, 40)
                                   ),),
                                ),
                                email,                                
                                Padding(
                                padding: const EdgeInsets.only(left : 8.0, bottom: 30.0,top: 80.0),
                                child: Text("Password", 
                                  style: TextStyle(
                                    fontSize: Utils().heightDivided(context, 40)
                                  ),),
                                ),
                                TextFormField(   
                                  style : new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),                           
                                  decoration: InputDecoration(
                                              labelText: 'Password',
                                              hintText: 'Password',
                                              suffixIcon: GestureDetector(
                                                onTap: () {
                                                  _togglePasswordVisibility();
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.only(right : 20.0),
                                                  child: Transform.scale(
                                                    scale: 2.0,                                                  
                                                    child: Icon(
                                                      _isHidePassword ? Icons.visibility_off : Icons.visibility,
                                                      color: _isHidePassword ? Colors.grey : Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
                                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                              isDense: true,
                                  ),                                  
                                  validator: (value) {
                                  if (value.isEmpty) {
                                      return 'Please enter password';
                                    }
                                    return null;
                                  },
                                  controller: passwdController,
                                  keyboardType: TextInputType.text,
                                  //onSubmitted: (_) => submitData(),
                                  //  onChanged: (val) => amountInput = val
                                  obscureText: _isHidePassword,                              
                                  ),  

                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0, bottom: 30),
                                  child: (_isLoading) ?
                                  Center(
                                    child: SizedBox(
                                      child : CircularProgressIndicator(),
                                      height:  Utils().screenHeight(context, 10),
                                      width:   Utils().screenWidth(context, 8), 
                                    ),
                                  )
                                  :   
                                  //Button  
                                  Padding(
                                  padding: const EdgeInsets.only(top: 120),   
                                  child : new SizedBox(      
                                  width : double.infinity,   
                                  height: Utils().screenHeight(context, 7),
                                  child : RaisedButton(
                                    shape : new RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(10.0),
                                        side: BorderSide(color: Colors.white)), 
                                    child : Padding( 
                                        child: Text('Login',style: TextStyle(
                                                                    fontWeight: FontWeight.normal,
                                                                    fontSize: 45.0,
                                                                    color: Colors.white
                                                                  ),
                                                                  ),
                                      padding: EdgeInsets.all(14),
                                    ),
                                    color: Color.fromRGBO(40, 167, 69, 100),
                                    textColor:  Colors.white, 
                                    onPressed: ()  {                                                                
                                      _submit();                                      
                                    }
                                    ),
                                    )
                                  ),
                                ),                           
                          ],
                        ),
                      ),
                    ),    
           ),
        ),
      ),      
      ],
    ) ,
    bottomNavigationBar: BottomAppBar(  
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Utils().widthDivided(context, 3.5),vertical: Utils().heightDivided(context, 40.0)),
                child: Text("Copyright © - PT. Buchi Indonesia",
                      style: TextStyle(color: Colors.black,
                                       fontWeight: FontWeight.bold,
                                       fontSize: 30.0),
                     ),
              ),
              color: Color.fromRGBO(148, 205, 12, 100),
      ),

  
    );
    
  }
}