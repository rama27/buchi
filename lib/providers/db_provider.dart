import 'dart:io';

import 'package:buchi/models/answer_model.dart';
import 'package:buchi/models/category_response.dart';
import 'package:buchi/models/dokumen_model.dart';
import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/feature_product_model.dart';
import 'package:buchi/models/konfigurasi_product_model.dart';
import 'package:buchi/models/folder_model.dart';
import 'package:buchi/models/kuisoner_model.dart';
import 'package:buchi/models/product_goods_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/models/product_model.dart';
import 'package:buchi/models/pricelist_response.dart';
import 'package:buchi/models/product_price_model.dart';
import 'package:buchi/models/question_model.dart';
import 'package:buchi/models/question_products_model.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:path/path.dart';
import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._(); 

  Future<Database> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();

    return _database; 
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
      // If you need to add a column      
      // if (newVersion > oldVersion) {      
      //   // db.execute("ALTER TABLE Answer ADD COLUMN status INTEGER");    
      //    await db.rawQuery("DROP TABLE IF EXISTS Answer");
      //    await db.execute('CREATE TABLE Answer(id INTEGER PRIMARY KEY,idquestion INTEGER,answer TEXT,img TEXT,status INTEGER,created_at TEXT,updated_at TEXT)');            
      // }           
  }

  // Create the database and the pricelist table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'newBuchi_db32.db');

    return await openDatabase(path, version: 4, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      //CREATE TABLE    
      await db.execute('CREATE TABLE Pricelist(id INTEGER PRIMARY KEY,code TEXT,name TEXT,price INTEGER,status INTEGER,created_at TEXT,updated_at TEXT)');
      await db.execute('CREATE TABLE Category(id INTEGER PRIMARY KEY,name TEXT,picture TEXT,status INTEGER,sort INTEGER,created_at TEXT,updated_at TEXT)');           
      await db.execute('CREATE TABLE Question(id INTEGER PRIMARY KEY,question TEXT,status INTEGER,created_at TEXT,updated_at TEXT)');
      await db.execute('CREATE TABLE QuestionProduct(id INTEGER PRIMARY KEY,idcategory INTEGER,idquestion INTEGER,yes INTEGER,nonote TEXT,yesnote TEXT,no INTEGER,start INTEGER,noend INTEGER,yesend INTEGER,status INTEGER,created_at TEXT,updated_at TEXT)');    
      await db.execute('CREATE TABLE Answer(id INTEGER PRIMARY KEY,idquestion INTEGER,answer TEXT,img TEXT,status INTEGER,created_at TEXT,updated_at TEXT)');
      await db.execute('CREATE TABLE Product(id INTEGER PRIMARY KEY,name TEXT,price INTEGER,keterangan TEXT,status INTEGER,created_at TEXT,updated_at TEXT)');        
      await db.execute('CREATE TABLE FeatureProduct(id INTEGER PRIMARY KEY,idproduct INTEGER,idfeature INTEGER,status INTEGER,created_at TEXT,updated_at TEXT)');        
      await db.execute('CREATE TABLE ProductGoods(id INTEGER PRIMARY KEY,idproduct INTEGER,idgoods INTEGER,status INTEGER,created_at TEXT,updated_at TEXT)'); 
      await db.execute('CREATE TABLE ProductPrice(id INTEGER PRIMARY KEY,code TEXT,name TEXT,price INTEGER,status INTEGER,created_at TEXT,updated_at TEXT)');            
      await db.execute('CREATE TABLE ProductImg(id INTEGER PRIMARY KEY,idproduct INTEGER,img TEXT,status INTEGER,created_at TEXT,updated_at TEXT)'); 
      await db.execute('CREATE TABLE Feature(id INTEGER PRIMARY KEY,name TEXT,status INTEGER,created_at TEXT,updated_at TEXT)'); 
      await db.execute('CREATE TABLE Kuisoner(id INTEGER PRIMARY KEY,nama TEXT,created_at TEXT,updated_at TEXT,status INTEGER,keterangan TEXT)'); 
      await db.execute('CREATE TABLE Folder(id INTEGER PRIMARY KEY,categoryid INTEGER,name TEXT,parent INTEGER,status INTEGER,userid INTEGER,created_at TEXT)'); 
      await db.execute('CREATE TABLE Document(id INTEGER PRIMARY KEY,idcategory INTEGER,name TEXT,type TEXT,status INTEGER,folderid INTEGER,userid INTEGER,created_at TEXT,updated_at TEXT)'); 

    }, 
    onUpgrade: _onUpgrade
    );      
     // UPGRADE DATABASE TABLES
  }

  // Insert pricelist on database
  createPricelist(PriceList newEmployee) async {
    await deleteAllEmployees();
    final db = await database;
    final res = await db.insert('Pricelist', newEmployee.toJson());
    print("createPriceList $res");     
    await DataConfig().setDataInt("sizeprice", res);
    return res;
  }

  // Delete all pricelist
  Future<int> deleteAllEmployees() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Pricelist');

    return res;
  }


  Future<List<PriceList>> getAllPricelist(String filterCriteria, int panjang) async {
    final db = await database;   
    var res = await db.rawQuery('''SELECT * FROM Pricelist 
    WHERE 
    name LIKE "%$filterCriteria%" or 
    code LIKE "%$filterCriteria%" or 
    price LIKE "%$filterCriteria%" 
    limit "$panjang" 
    ''');
    List<PriceList> list =
        res.isNotEmpty ? res.map((c) => PriceList.fromJson(c)).toList() : [];

    return list;
  }

   // Insert CategoryResp on database
  createCategory(CategoryResp newEmployee) async {
    await deleteAllCategoryResp();
    final db = await database;    
    final res = await db.insert('Category', newEmployee.toJson());
    print("createCategory $res");  
    return res;
  }

  // Delete all pricelist
  Future<int> deleteAllCategoryResp() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Category');

    return res;
  }
  
  Future<List<CategoryResp>> getAllCategory() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Category");

    List<CategoryResp> list =
        res.isNotEmpty ? res.map((c) => CategoryResp.fromJson(c)).toList() : [];

    return list;
  }

   // QUESTION 
   // Insert Question on database
  createQuestion(Question newQuestion) async {
    await deleteQuestion();
    final db = await database;
    final res = await db.insert('Question', newQuestion.toJson());
    print("createQuestion $res");   
    return res;
  }

  // Delete all pricelist
  Future<int> deleteQuestion() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Question'); 

    return res;
  }
  
  //embuh ii gak gelem   
  Future<List<Question>> getAllQuestion() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Question");
    print("get all question $res");

    List<Question> list =
        res.isNotEmpty ? res.map((c) => Question.fromJson(c)).toList() : [];  
    return list;
  }
      
   // Insert QuestionProduct on database
  createQuestionProduct(Questionproduct questionproduct) async {
    await deleteQuestionProduct();
    final db = await database;
    final res = await db.insert('QuestionProduct', questionproduct.toJson());
    print("createQuestionProduct $res");    
    return res;
  }

  // Delete all QuestionProduct
  Future<int> deleteQuestionProduct() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM QuestionProduct');
    return res;
  }

 // get all QuestionProduct
  Future<List<Questionproduct>> getAllQuestionProduct() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM QuestionProduct");
    print(res);

    List<Questionproduct> list =
        res.isNotEmpty ? res.map((c) => Questionproduct.fromJson(c)).toList() : [];  
    return list;
  }

  
  Future<List<Map<String, dynamic>>> getFirstQuestion(int idcat) async {
    final db = await database;
    final res = await db.rawQuery("SELECT idquestion FROM questionproduct where start=1 and idcategory=$idcat and status=1");  
    print("SQL FIRST QUESTION $res");   
    return res;
  }
 
  Future<List<Map<String, dynamic>>> getPertanyaan(int idcat, int idquestion) async{
    // print("getPertanayaan param $idcat $idquestion");
    final db = await database;
    final res = await db.rawQuery(
      "SELECT * FROM QuestionProduct qp left join Question q on qp.idquestion=q.id left join Answer a on q.id=a.idquestion where q.id=$idquestion and qp.idcategory=$idcat");  
    // print("SQL PERTANYAAN $res");   
    return res;
  }  

   // ANSWER 
   // Insert answer on database
  createAnswer(Answer newAnswer) async {
    await deleteQuestion();
    final db = await database;
    final res = await db.insert('Answer', newAnswer.toJson());     
    print("createAnswer $res");    
    return res;
  }

  // Delete all answer
  Future<int> deleteAnswer() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Answer');
    return res;
  }
  
  Future<List<Answer>> getAllAnswer() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Answer");
    print(res);

    List<Answer> list =
        res.isNotEmpty ? res.map((c) => Answer.fromJson(c)).toList() : [];  
    return list;
  }

  //create Product 
   createProduct(Product newProduct) async {
    await deleteAllProduct();
    final db = await database;
    final res = await db.insert('Product', newProduct.toJson());
    print("createProduct $res");    
    return res;
  }

  // Delete all product
  Future<int> deleteAllProduct() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Product');
    return res;
  }

  //get all product 
  Future<List<Product>> getAllProduct() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Product");
    print(res);

    List<Product> list =
        res.isNotEmpty ? res.map((c) => Product.fromJson(c)).toList() : [];  
    return list;
  }

   //create Feature Product 
   createFeatureProduct(Featureproduct newProduct) async {
    await deleteAllFeatureProduct();
    final db = await database;
    final res = await db.insert('FeatureProduct', newProduct.toJson());
    print("createFeatureProduct $res");   
    return res;
  }

  // Delete all Feature product
  Future<int> deleteAllFeatureProduct() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM FeatureProduct');
    return res;
  }

  //get all product 
  Future<List<Featureproduct>> getAllFeatureProduct() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM FeatureProduct");
    print(res);

    List<Featureproduct> list =
        res.isNotEmpty ? res.map((c) => Featureproduct.fromJson(c)).toList() : [];  
    return list;
  }

  //create Product Goods
   createProductGoods(Productgood newProductgood) async {
    await deleteAllProductGoods();
    final db = await database;
    final res = await db.insert('ProductGoods', newProductgood.toJson());
    print("createProductGoods $res");   
    return res;
  }

  // Delete all Product Goods
  Future<int> deleteAllProductGoods() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM ProductGoods');
    return res;
  }

  //get all Product Goods
  Future<List<Productgood>> getAllProductGoods() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM ProductGoods");
    print(res);

    List<Productgood> list =
        res.isNotEmpty ? res.map((c) => Productgood.fromJson(c)).toList() : [];  
    return list;
  }

   //create Product Price
   createProductPrice(Productprice newProductprice) async {
    await deleteAllProductPrice();
    final db = await database;
    final res = await db.insert('ProductPrice', newProductprice.toJson());
    print("createProductPrice $res");  
    return res;
  }

  // Delete all Product Price
  Future<int> deleteAllProductPrice() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM ProductPrice');
    return res;
  }

  //get all Product Price
  Future<List<Productprice>> getAllProductPrice(String filterCriteria, int panjang, int halaman) async {
    final db = await database;
    final res = await db.rawQuery("""
    SELECT * FROM ProductPrice
    WHERE 
    name LIKE "%$filterCriteria%" or 
    code LIKE "%$filterCriteria%" or 
    price LIKE "%$filterCriteria%" 
    limit "$panjang" 
    offset "$halaman" 
    """);
    print(res);

    List<Productprice> list =
        res.isNotEmpty ? res.map((c) => Productprice.fromJson(c)).toList() : [];  
    return list;
  }
  
   //create Product Img
   createProductImg(Productimg newProductimg) async {
    await deleteAllProductPrice();
    final db = await database;
    final res = await db.insert('ProductImg', newProductimg.toJson());
    print("createProductImg $res");    
    return res;
  }

  // Delete all Product Img
  Future<int> deleteAllProductImg() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM ProductImg');
    return res;
  }

  //get all Product Img
  Future<List<Productimg>> getAllProductImg() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM ProductImg");
    print(res);

    List<Productimg> list =
        res.isNotEmpty ? res.map((c) => Productimg.fromJson(c)).toList() : [];  
    return list;
  }

  // Sql untuk menampilkan produk   
  Future<Product> getProduct(int idproduct) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM PRODUCT WHERE id=$idproduct");
    print("get produk $res");

    Product data = res.isNotEmpty ?  Product.fromJson(res[0]) : [];     
    return data;
  }

  // Sql untuk menampilkan konfigurasi  
   Future<List<Configurationproduct>> getConfiguration(int idproduct) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM ProductGoods pg left join ProductPrice p on pg.idgoods=p.id where pg.status=1 and pg.idproduct=$idproduct");

    print("get konfig $res");

    // List<Configurationproduct> data = res.isNotEmpty ? 
    //  Configurationproduct.fromJson(res) : [];     
     List<Configurationproduct> data = res.isNotEmpty ? 
     res.map((m) => new Configurationproduct.fromJson(m)).toList() : [];  
     
     return data;
  }

  // SELECT * FROM featureproduct fp left join feature f on fp.idfeature=f.id where fp.idproduct=$id and f.status=1 and fp.status=1;
  // Sql untuk menampilkan feature
    Future<List<Feature>> getFeature(int idproduct) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM FeatureProduct fp left join Feature f on fp.idfeature=f.id where fp.idproduct=$idproduct and f.status=1 and fp.status=1");

    print("get feature $res");

    // List<Configurationproduct> data = res.isNotEmpty ? 
    //  Configurationproduct.fromJson(res) : [];     
     List<Feature> data = res.isNotEmpty ? 
     res.map((m) => new Feature.fromJson(m)).toList() : [];  
     
     return data;
  }

   //create Feature 
   createFeature(Feature newFeature) async {
    await deleteAllFeature();
    final db = await database;
    final res = await db.insert('Feature', newFeature.toJson());
    print("createFeature $res");   
    return res;
  }

  // Delete all Feature
  Future<int> deleteAllFeature() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Feature');
    return res;
  }

  //get all Feature 
  Future<List<Feature>> getAllFeature() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM Feature");
    print(res);

    List<Feature> list =
        res.isNotEmpty ? res.map((c) => Feature.fromJson(c)).toList() : [];  
    return list;
  }

   // Sql untuk menampilkan gambar   
  Future<List<Productimg>> getImage(int idproduct) async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM ProductImg WHERE idproduct=$idproduct");
    print("get image $res");

    List<Productimg> list =
        res.isNotEmpty ? res.map((c) => Productimg.fromJson(c)).toList() : [];  
    return list;
  }

  //create Kuisoner   
   createKuisoner(Kuisoner newKuisoner) async {
    await deleteAllKuisoner();
    final db = await database;
    final res = await db.insert('Kuisoner', newKuisoner.toJson());
    print("createKuisoner $res");    
    return res;
  }

  // Delete all Kuisoner
  Future<int> deleteAllKuisoner() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Kuisoner');
    return res;
  }

  //get all Kuisoner
  Future<List<Kuisoner>> getAllKuisoner(String filterCriteria, int panjang, int halaman) async {
    final db = await database;
    // final res = await db.rawQuery("SELECT * FROM Kuisoner");
    var res = await db.rawQuery('''SELECT * FROM Kuisoner 
    WHERE 
    nama LIKE "%$filterCriteria%" or 
    created_at LIKE "%$filterCriteria%"    
    limit "$panjang" 
    offset "$halaman" 
    ''');
    print(res);

    List<Kuisoner> list =
        res.isNotEmpty ? res.map((c) => Kuisoner.fromJson(c)).toList() : [];  
    return list;
  }
  
  //create Folder   
   createFolder(Folder newKuisoner) async {
    await deleteAllKuisoner();
    final db = await database;
    final res = await db.insert('Folder', newKuisoner.toJson());
    print("createFolder $res");    
    return res;
  }

  // Delete all Folder
  Future<int> deleteAllFolder() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Folder');
    return res;
  }

  //get all Folder
  Future<List<Folder>> getAllFolder() async {
    final db = await database;
    // final res = await db.rawQuery("SELECT * FROM Kuisoner");
    var res = await db.rawQuery('SELECT * FROM Folder');
    print(res);

    List<Folder> list =
        res.isNotEmpty ? res.map((c) => Folder.fromJson(c)).toList() : [];  
    return list;
  }

   //create Document   
   createDocument(Dokumen newDocument) async {
    await deleteAllKuisoner();
    final db = await database;
    final res = await db.insert('Document', newDocument.toJson());
    print("createDocument $res");    
    return res;
  }
 
  // Delete all Document
  Future<int> deleteAllDocument() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Document');
    return res;
  }

   Future<int> deleteDocument(int id) async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Document WHERE id=$id');
    return res;
  }

  //get all Document
  Future<List<Dokumen>> getAllDocument() async {
    final db = await database;
    // final res = await db.rawQuery("SELECT * FROM Kuisoner");
    var res = await db.rawQuery('SELECT * FROM Document');
    print(res);

    List<Dokumen> list =
        res.isNotEmpty ? res.map((c) => Dokumen.fromJson(c)).toList() : [];  
    return list;
  }


  //get all Document 
  Future<List<Dokumen>> getAllBeginningDocument(int folderid, int categoryid) async {
    print("categoryid $categoryid");
    final db = await database;    
    // var res = await db.rawQuery('SELECT * FROM Document ');
    // var res = await db.rawQuery('SELECT * FROM Document WHERE idcategory=$categoryid AND id=$folderid');
    var res = await db.rawQuery('SELECT * FROM Document WHERE idcategory=$categoryid AND folderid=$folderid');
    print(res);

    List<Dokumen> list =
        res.isNotEmpty ? res.map((c) => Dokumen.fromJson(c)).toList() : [];  
    return list;
  }

   //get ALL Folder content
  Future<List<Folder>> getFolderContent(int folderid, int categoryid) async {
    final db = await database;
    // final res = await db.rawQuery("SELECT * FROM Kuisoner");
    var res = await db.rawQuery('SELECT * FROM Folder WHERE categoryid=$categoryid');
    print("FOLDERCONTENT $res");

    List<Folder> list =
        res.isNotEmpty ? res.map((c) => Folder.fromJson(c)).toList() : [];  
    return list;
  }

  
}