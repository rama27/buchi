class PriceListResponse {
  int total;
  int pages;
  List<PriceList> data;

  PriceListResponse({this.total, this.pages, this.data});

  PriceListResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    pages = json['pages'];
    if (json['data'] != null) {
      data = new List<PriceList>();
      json['data'].forEach((v) {
        data.add(new PriceList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['pages'] = this.pages;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PriceList {
  int id;
  String code;
  String name;
  int price;
  int status;
  String createdAt;
  String updatedAt;

  PriceList(
      {this.id,
      this.code,
      this.name,
      this.price,
      this.status,
      this.createdAt,
      this.updatedAt});

  PriceList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    price = json['price'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['name'] = this.name;
    data['price'] = this.price;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}