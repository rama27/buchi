import 'dart:io';

import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class FormEdit extends StatefulWidget {
  @override
  _FormEditState createState() => _FormEditState();
}

class _FormEditState extends State<FormEdit> {
  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final createdAtController = TextEditingController();
  final lastLoginController = TextEditingController();
  final passwdController = TextEditingController();
  TextFormField name;
  TextFormField email;
  TextFormField createdAt;
  TextFormField lastLogin;
  bool _isHidePassword = true; 
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
      name  = TextFormField(  
      keyboardType: TextInputType.emailAddress,
      controller: usernameController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter name ';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        labelText: 'Name',
        hintText: 'Name',
        contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      );

      email  = TextFormField(  
      keyboardType: TextInputType.text,
      controller: emailController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter email';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'Email',
        contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      );

      createdAt  = TextFormField(  
      keyboardType: TextInputType.text,
      controller: createdAtController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter createdAt';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        labelText: 'Created At',
        hintText: 'Created At',
        contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      );

      createdAt  = TextFormField(  
      keyboardType: TextInputType.text,
      controller: createdAtController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter createdAt';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        labelText: 'Created At',
        hintText: 'Created At',
        contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      );

      lastLogin = TextFormField(  
      keyboardType: TextInputType.text,
      controller: createdAtController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter lastLogin';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        labelText: 'Last Login',
        hintText: 'Las Login',
        contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      );

  }

   void _togglePasswordVisibility(){
   setState(() {
     _isHidePassword = !_isHidePassword;
   });
 }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey();
    return  Card(
            child :  Container(  
                          height: Utils().screenHeight(context, 120),                   
                          padding: EdgeInsets.only(right: 30.0, left : 30.0, top: 0.0, bottom: 0.0), 
                              child: Form(
                              key: _formKey, 
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,                              
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[       

                                Padding(
                                  padding: const EdgeInsets.only(left : 8.0, bottom: 30.0),
                                  child: Text("Name", 
                                   style: TextStyle(
                                     fontSize: Utils().heightDivided(context, 45)
                                   ),),
                                ),
                                name,
                               
                                Padding(
                                  padding: const EdgeInsets.only(left : 8.0, bottom: 30.0,top : 70.0),
                                  child: Text("Email", 
                                   style: TextStyle(
                                     fontSize: Utils().heightDivided(context, 45)
                                   ),),
                                ),
                                email,                                      

                               Padding(
                                  padding: const EdgeInsets.only(left : 8.0, bottom: 30.0,top : 70.0),
                                  child: Text("Created At", 
                                   style: TextStyle(
                                     fontSize: Utils().heightDivided(context, 45)
                                   ),),
                                ),
                                createdAt,  

                               Padding(
                                  padding: const EdgeInsets.only(left : 8.0, bottom: 30.0,top : 70.0),
                                  child: Text("Last Login", 
                                   style: TextStyle(
                                     fontSize: Utils().heightDivided(context, 45)
                                   ),),
                                ),
                                lastLogin,            
                               
                               //text passwd
                                Row(
                                  children: <Widget>[
                                   Padding(
                                      padding: const EdgeInsets.only(left : 8.0, bottom: 30.0,top: 70.0),
                                      child: Text("New Password", 
                                        style: TextStyle(
                                          fontSize: Utils().heightDivided(context, 45)
                                        ),),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left : 30.0, bottom: 30.0,top: 70.0),
                                      child: Text("Fill to change password", 
                                        style: TextStyle(
                                          fontSize: Utils().screenHeight(context, 1.8),
                                          color: Colors.red
                                        ),),
                                    ),
                                  ],
                                ),
                              
                                TextFormField(   
                                  style : new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),                           
                                  decoration: InputDecoration(
                                              labelText: 'New Password',
                                              hintText: 'New Password',
                                              suffixIcon: GestureDetector(
                                                onTap: () {
                                                  _togglePasswordVisibility();
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.only(right : 20.0),
                                                  child: Transform.scale(
                                                    scale: 2.0,                                                  
                                                    child: Icon(
                                                      _isHidePassword ? Icons.visibility_off : Icons.visibility,
                                                      color: _isHidePassword ? Colors.grey : Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              contentPadding: new EdgeInsets.symmetric(vertical: 40.0, horizontal: 50.0),
                                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                              isDense: true,
                                  ),                                  
                                  validator: (value) {
                                  if (value.isEmpty) {
                                      return 'Please enter new password';
                                    }
                                    return null;
                                  },
                                  controller: passwdController,
                                  keyboardType: TextInputType.text,
                                  //onSubmitted: (_) => submitData(),
                                  //  onChanged: (val) => amountInput = val
                                  obscureText: _isHidePassword,                              
                                ),
                                 
                              //box photo 
                              Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top : 30.0),
                                    child: Center(
                                      child: Container(
                                      height: Utils().screenHeight(context, 35),
                                      width: Utils().screenWidth(context, 60),  
                                      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          top: BorderSide(width: 1.0, color: Colors.black),
                                          left: BorderSide(width: 1.0, color: Colors.black),
                                          right: BorderSide(width: 1.0, color: Colors.black),
                                          bottom: BorderSide(width: 1.0, color: Colors.black),
                                        ),                                
                                      ),
                                      ),
                                    ),
                                  ),  

                              Padding(
                                padding: EdgeInsets.only(top: Utils().screenHeight(context, 23.0),
                                                        left : Utils().screenWidth(context, 16.0),
                                                        right : Utils().screenWidth(context, 16.0),
                                                        bottom: Utils().screenHeight(context, 3.0)),   
                                child : new SizedBox(      
                                width : Utils().width(context),   
                                height: Utils().screenHeight(context, 5),
                                child : RaisedButton(
                                  shape : new RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(0.0),
                                      side: BorderSide(color: Colors.white)), 
                                  child : Padding( 
                                      child: Text('Change Photo',style: TextStyle(
                                                                  fontWeight: FontWeight.normal,
                                                                  fontSize: 35.0,
                                                                  color: Colors.white
                                                                ),
                                                                ),
                                    padding: EdgeInsets.all(0.0),
                                  ),
                                  color: Color.fromRGBO(95, 98, 101, 2.0),
                                  textColor:  Colors.white, 
                                  onPressed: () {
                                     ImagePicker.pickImage(source: ImageSource.gallery);
                                  }                            
                                  ),
                                  )
                              ),   
                                ],
                              ), 
                          ],
                        ),
                      ),
                    ),    
           );
  }
}