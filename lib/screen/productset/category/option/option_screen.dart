import 'dart:convert';

import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/screen/productset/category/option/question/question_screen.dart';
import 'package:buchi/screen/productset/category/shared/shared_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/card/category_item.dart';
import 'package:buchi/widget/card/option_item.dart';
import 'package:buchi/widget/drawer/side_drawer.dart';
import 'package:flutter/material.dart';

class OptionScreen extends StatefulWidget {
  final int idCategory;
  OptionScreen({this.idCategory});

  @override
  _OptionScreenState createState() => _OptionScreenState();
}

class _OptionScreenState extends State<OptionScreen> {
  String idQuestion;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    DBProvider.db.getFirstQuestion(widget.idCategory).then((val){
      // print("First Question $val");
      // print("idquestion ${val[0]["idquestion"]}");

      setState(() {
        int idquestion = val[0]["idquestion"]; 
        val != null ? idQuestion= idquestion.toString() :  idQuestion = "kosong";
      });         
    });


    super.didChangeDependencies();
  }


  @override
  Widget build(BuildContext context) {
  //  print("idCategoryOption ${widget.idCategory}"); 
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  
  void navigationPage() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => QuestionScreen(idCat: widget.idCategory,idQuestion: int.parse(idQuestion),)
    ));     
  }

  void navigationPage2() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => SharedScreen(idCategory: widget.idCategory)
    ));     
  }
 
 
  var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),
                                          
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 55)),
                  child: IconButton(
                    padding: EdgeInsets.all(5.0),
                    icon: Image.asset('assets/images/refresh.png'),
                    onPressed: () {
                      // Implement navigation to shopping cart page here...
                      print('Click Menu');
                    },
                  ),
                ),                        
              ],
          ),                                                                                                                                                 
      ),   
    );

    
    var size = MediaQuery.of(context).size;  
    var appBarHeight = appBar.preferredSize.height;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - appBarHeight - 24) / 3.5;
    final double itemWidth = size.width / 2;

    return Scaffold(
      key : _scaffoldKey,
      appBar : appBar,
      drawer : SideDrawer(),  
      body: Stack(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),
          
          Padding(
            padding:  EdgeInsets.fromLTRB(16.0,Utils().screenHeight(context, 25),16.0,0.0),
            child: Container(
               child: GridView.count(
               crossAxisCount: 2,
               childAspectRatio: (itemWidth / itemHeight),
               children: <Widget>[

                 GestureDetector(
                   child: OptionItem(path: 'assets/images/decisiontree.png', title:"Decision Tree",),
                   onTap: () {navigationPage();},
                 ),

                 GestureDetector(
                   child: OptionItem(path: 'assets/images/shared.png', title:"Shared Folder",),
                   onTap: () {
                     navigationPage2();
                   },
                 ),                              
                ],
                ),
            ),
          ),
        ],
      ),
    );
  }
}