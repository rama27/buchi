import 'package:buchi/screen/productset/category/option/option_screen.dart';
import 'package:buchi/widget/card/category_item.dart';
import 'package:flutter/material.dart';

class CategoryGrid extends StatelessWidget {

  final double appBarHeigth;

  CategoryGrid({@required this.appBarHeigth});

  @override
  Widget build(BuildContext context) {
   void navigationPage() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => OptionScreen()
    ));    
  //  Navigator.of(context).pushReplacementNamed('/category'); 
  }
  
  var size = MediaQuery.of(context).size;  
  // var appBarHeight = appBar.preferredSize.height;
   /*24 is for notification bar on Android*/
  final double itemHeight = (size.height  - 24) / 4;
  final double itemWidth = size.width / 2;
  
    return Container(
             child: GridView.count(
             crossAxisCount: 2,
             childAspectRatio: (itemWidth / itemHeight),
             children: <Widget>[

               GestureDetector(
                 child: CategoryItem(path: 'assets/images/r300.png', title:"Laboratory Evaporation",),
                 onTap: () {navigationPage();},
               ),

               CategoryItem(path: 'assets/images/IndustrialEvaporation.png', title:"Industrial Evaporation",), 
               CategoryItem(path: 'assets/images/spray.png', title: "Spray Drying & Encapsulation",),  
               CategoryItem(path: 'assets/images/meltingpoint.png', title:"Melting Point",),
               CategoryItem(path: 'assets/images/multivapor.jpg', title: "Parallel Evaporation",),
               CategoryItem(path: 'assets/images/freezedrying.png', title:"Freeze Dryer",),
               CategoryItem(path: 'assets/images/PURE.jpg', title:"Preparative Chromatography",), 
               CategoryItem(path: 'assets/images/kjeldahl.jpg', title:"Kjeldahl",),  
               CategoryItem(path: 'assets/images/extraction.jpg', title:"Extraction",),
               CategoryItem(path: 'assets/images/nirsolutions.png', title:"NIR Solutions",),
               CategoryItem(path: 'assets/images/nironline.png', title:"NIR-ONLINE",),        

              ],
              ),
          );
  }
}