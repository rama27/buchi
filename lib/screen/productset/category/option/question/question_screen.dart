import 'dart:convert';

import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/first_question_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/providers/question_api_provider.dart';
import 'package:buchi/screen/productset/category/option/option_screen.dart';
import 'package:buchi/screen/productset/category/option/question/result/result_screen.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/dialog/content_question_dialog.dart';
import 'package:flutter/material.dart';

class QuestionScreen extends StatefulWidget {
  
  int idCat, idQuestion;
  QuestionScreen({this.idCat,this.idQuestion});

  @override
  _QuestionScreenState createState() => _QuestionScreenState();
}

 

class _QuestionScreenState extends State<QuestionScreen> {
  bool isLoading = false;
  String question;
  String yesnote,nonote;   
  int yes,yesend;
  int no,nosend;
  String answer,img;
  int idQuestion,idQuestionNo;
  int yesEnd;
  int price;
  String product,keterangan;  
  List<Feature> featureList;
  List<Productimg> imageList;

  Future<Null> getInformationDialog(int idcategory, int idquestion) async{
    String returnVal =  await  showGeneralDialog(
      barrierLabel: "Label",
      barrierDismissible: true,           
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return ContentQuestionDialog(answer: answer, img: img, idcat: idcategory, idquestion: idquestion,);
      },
            transitionBuilder: (context, anim1, anim2, child) {
              return SlideTransition(
                position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                child: child,
              );
            },
    ); 

    if (returnVal == 'success') {      
      DBProvider.db.getPertanyaan(widget.idCat, idquestion).then((val) {
      // print("xxy ${val[0]['question']}");  
      setState(() {
        question = val[0]['question'] != null ? val[0]['question'] : ""; 
        yesnote = val[0]['yesnote'];
        nonote = val[0]['nonote'];
        yes = val[0]['yes'] != null ? val[0]['yes'] : val[0]['yesend'];
        answer = val[0]['answer'];
        img = val[0]['img'];
      });
      if(answer != null || img !=null){
        if(yes != null){
           getInformationDialog(widget.idCat, yes);
        }else{            
           getInformationDialog(widget.idCat, idquestion);
        }       
      }                         
     });
    }
    // return returnVal;   
  }

   @override
  void initState() {   
    featureList = new List();
    imageList= new List();
    super.initState();    
  }

  
  @override
  void didChangeDependencies() async {             
    
      DBProvider.db.getPertanyaan(widget.idCat, widget.idQuestion).then((val) {
      // print("PERTANYAAN ${val[0]['question']}");   
      setState(() {
         question = val[0]['question']; 
         img = val[0]['img']; 
      });       
      yesnote = val[0]['yesnote'];
      nonote = val[0]['nonote'];
      yes = val[0]['yes'];
      idQuestion = yes;
      no =  val[0]['no'];
      nosend = val[0]['noend']; 
      idQuestionNo = no;
      print("yesnote $yesnote"); 
      print("no $no nosend $nosend");               
 
      if(img != null){
        getInformationDialog(widget.idCat,yes);
      }

     });     
    super.didChangeDependencies();
  }

  
  

  _loadFromApi() async {
    setState(() {
      isLoading = true;
    });
    
    var apiProvider2 = QuestionApiProvider();
    await apiProvider2.getAllQuestion().then((val){
      print("questions from API $val");
            
    });

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
    });
  }

 

  @override
  Widget build(BuildContext context) {
  // print("Rebuild");  
  // print("idCatQuestion ${widget.idCat} ${widget.idQuestion}");
  // print("rebuildno $no $nosend");
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  
  void navigationPage() {
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => OptionScreen()
    ));     
  } 
 
  var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),
                                          
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 45)),
                  child: IconButton(
                    padding: EdgeInsets.all(5.0),
                    icon: Image.asset('assets/images/refresh.png'),
                    onPressed: () {
                      // Implement navigation to shopping cart page here...
                      _loadFromApi();
                      print('Click Menu Question');
                    },
                  ),
                ),                        
              ],
          ),                                                                                                                                                 
      ),   
    );

  
    var size = MediaQuery.of(context).size;  
    var appBarHeight = appBar.preferredSize.height;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - appBarHeight - 24) / 4;
    final double itemWidth = size.width / 2;
    String title  = "Laboratory Evaporation";
    // String question = "Apakah selama proses evaporasi berjalan, Bapak/Ibu pernah menemui kendala dalam menentukan titik didih dari suatu campuran solvent/extract ?";
    

    return Scaffold(
      appBar: appBar,
      body: Stack(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),
          Column(
            children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top : Utils().screenHeight(context, 3),
                                           bottom: Utils().screenHeight(context, 1)),
                  child: Container(
                    height: Utils().screenHeight(context, 10),                      
                        child: Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Column(
                              children: <Widget>[                              
                                  Expanded(
                                   child: Padding(
                                     padding: EdgeInsets.only(top : Utils().screenHeight(context, 2),
                                                              bottom : Utils().screenHeight(context, 2),
                                                              left: Utils().screenHeight(context, 4),
                                                              right: Utils().screenHeight(context, 4),
                                                              ),
                                     child: Text(
                                        title,
                                        style: TextStyle(
                                                fontSize: 60.0,
                                                fontWeight: FontWeight.bold,
                                                color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                      ),
                                   ),
                                  ),          
                              ],
                            ),    
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            elevation: 5,
                            margin: EdgeInsets.all(10),
                          ),                                    
          ),
         ),

          Container(
                  height: Utils().screenHeight(context, 55),                      
                      child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Column(
                            children: <Widget>[                              
                                Expanded(
                                 child: Padding(
                                   padding: EdgeInsets.fromLTRB(Utils().screenHeight(context, 3),
                                                                Utils().screenHeight(context, 3),
                                                                Utils().screenHeight(context, 5),
                                                                Utils().screenHeight(context, 3)),
                                   child: 
                                   question != null?
                                   Text(                                     
                                      question,
                                      style: TextStyle(
                                              fontSize: 40.0,
                                              fontWeight: FontWeight.bold,
                                              color : Color.fromRGBO(108,117, 125, 1.0)
                                      ),
                                    ) 
                                    :
                                    Center(child: CircularProgressIndicator()),                                
                                 ),
                                ), 

                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                  Utils().screenWidth(context, 3),
                                  Utils().screenWidth(context, 3),
                                  Utils().screenWidth(context, 3),
                                  Utils().screenWidth(context, 3)
                                  ),
                                child: Row(
                                  children: <Widget>[                        
                                      Padding(
                                        padding: EdgeInsets.only(left : Utils().screenWidth(context, 2)),
                                        child: RaisedButton(
                                          onPressed: () {                                           
                                            // showDialog();
                                            DataConfig().getDataInt("yesend").then((val) {
                                              yesEnd= val;   
                                              print("yesEnd from dataConf $yesEnd");       
                                              if(yesEnd == null || yesEnd == 0){
                                               print("not navigate");     
                                              }else{                                                
                                                 print("navigate");          
                                                  var prices;                                
                                                   DBProvider.db.getProduct(yesEnd).then((val) {                                                  
                                                      print("getPloduk ${val.name}");
                                                      setState(() {        
                                                        prices  = val.price;
                                                        product = val.name;
                                                        keterangan = val.keterangan;                                                                                                           
                                                      });
                                                                                                         
                                                      print("pricenow $prices");
                                                               
                                                       DBProvider.db.getFeature(yesEnd).then((val){
                                                          print("getFeatures ${val}");
                                                            setState(() {
                                                              featureList.addAll(val);                                                               
                                                            });         
                                                            DBProvider.db.getImage(yesEnd).then((val){
                                                               
                                                                setState(() {
                                                                   imageList.addAll(val);
                                                                });      
                                                                 print("getImageQuestion $imageList");
                                                                Navigator.push(
                                                                context,
                                                                MaterialPageRoute(builder: (context) => ResultScreen(idproduct: yesEnd,
                                                                price: prices, product: product, 
                                                                featureList: featureList, keterangan: keterangan, 
                                                                imageList: imageList,
                                                                ))
                                                                );                 
                                                             });                                                       
                                                          //  Navigator.of(context).pushReplacement( MaterialPageRoute( builder: (context) =>  ResultScreen(idproduct: yesEnd, price: prices, product: product, featureList: featureList,)));   
                                                          //  return  ResultScreen(idproduct: yesEnd, price: prices, product: product, featureList: featureList);                                                                                                       
                                                         
                                                      });
                                                                                                    
                                                   });
                                                                                                                                              
                                              }                                                                                                                                                                               
                                            }); 
                                           
                                            setState(() {                                           
                                              widget.idQuestion = idQuestion;
                                            });

                                            DBProvider.db.getPertanyaan(
                                              widget.idCat, widget.idQuestion).then(
                                                (val) {
                                                  setState(() {
                                                      question = val[0]['question'];
                                                      answer = val[0]['answer'];
                                                      img = val[0]['img'];                                                    
                                                      yes = val[0]['yes']; 
                                                      yesnote = val[0]['yesnote']; 
                                                      nonote = val[0]['nonote'];
                                                      yesend = val[0]['yesend'];                                                                                                          
                                                      print("yes $yes yesend $yesend");
                                                      print("answer $answer");

                                                      if(yesend == 0){
                                                        print("yesend0");
                                                        idQuestion = yes;
                                                        DataConfig().setDataInt("yesend", 0);                                                     
                                                      }else{
                                                        print("yesendnot0");
                                                        print("yesend $yesend");
                                                        idQuestion = yesend;
                                                        DataConfig().setDataInt("yesend", yesend);                                                                                                               
                                                      }

                                                      if(answer != null || img != null){                                                         
                                                            getInformationDialog(widget.idCat,idQuestion).then((val){
                                                              // print("dialog $val");
                                                            });                                                                                                                                                                         
                                                      }                                                                                                             
                                                  });                                                                                                
                                            });
                                          },
                                          textColor: Colors.white,
                                          padding: const EdgeInsets.all(0.0),
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              color: Colors.green,                                 
                                            ),
                                            padding: EdgeInsets.fromLTRB(Utils().screenWidth(context, 2),
                                                                        Utils().screenHeight(context, 1.5),
                                                                        Utils().screenWidth(context, 2),
                                                                        Utils().screenHeight(context, 1.5),
                                                                        ),
                                            child: const Text(
                                              'Ya',
                                              style: TextStyle(fontSize: 35.0)
                                            ),
                                          ),                            
                                        ),
                                      ),

                                      Expanded(
                                      child: Padding(
                                          padding: EdgeInsets.only(left : Utils().screenWidth(context, 2)),
                                          child: 
                                          yesnote != null ?
                                          Text(
                                           yesnote,
                                            style: TextStyle(
                                                    fontSize: 30.0,
                                                    fontWeight: FontWeight.bold,
                                                    color : Color.fromRGBO(108,117, 125, 1.0)
                                            ),
                                          ):
                                          Text(
                                            "",
                                            style: TextStyle(
                                                    fontSize: 30.0,
                                                    fontWeight: FontWeight.bold,
                                                    color : Color.fromRGBO(108,117, 125, 1.0)
                                            ),
                                          ),

                                        ),
                                      ),                      
                                  ],
                                ),                                
                              ),                              
                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                  Utils().screenWidth(context, 3),
                                  Utils().screenWidth(context, 3),
                                  Utils().screenWidth(context, 3),
                                  Utils().screenWidth(context, 3)
                                  ),
                                child: Row(
                                  children: <Widget>[
                                    no != 0 || nosend != 0 ?
                                      RaisedButton(
                                        onPressed: () {

                                            setState(() {                                          
                                              widget.idQuestion = idQuestionNo;
                                              print("quest ${widget.idQuestion} $idQuestion");
                                            });  

                                           DBProvider.db.getPertanyaan(
                                              widget.idCat, widget.idQuestion).then(
                                                (val) {
                                                  setState(() {
                                                      question = val[0]['question'] != null ? val[0]['question'] : "" ;
                                                      answer = val[0]['answer'];
                                                      img = val[0]['img'];                                                    
                                                      no = val[0]['no']; 
                                                      yesnote = val[0]['yesnote']; 
                                                      nonote = val[0]['nonote']; 
                                                      nosend = val[0]['noend'];                                                                                                          
                                                      print("no $no $nosend");
                                                      print("answer $answer");

                                                      if(nosend == 0){
                                                        setState(() {
                                                          idQuestionNo = no;
                                                          print("not send == 0 with idquestion $idQuestion");
                                                        });                                                        
                                                      }else{                                                        
                                                        idQuestion = nosend;
                                                        print("not send != 0 $idQuestion");
                                                      }

                                                      if(answer != null || img != null){                                                         
                                                            getInformationDialog(widget.idCat,idQuestion).then((val){
                                                              print("dialog $val");
                                                            });                                                                                                                                                                         
                                                      }                                                                                                                                                                                                               
                                                  });                                                                                                
                                            });

                                        },
                                        textColor: Colors.white,
                                        padding: const EdgeInsets.all(0.0),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Colors.red,                                 
                                          ),
                                          padding: EdgeInsets.fromLTRB(Utils().screenWidth(context, 2),
                                                                      Utils().screenHeight(context, 1.5),
                                                                      Utils().screenWidth(context, 2),
                                                                      Utils().screenHeight(context, 1.5),
                                                                      ),
                                          child: 
                                          const Text(
                                            'Tidak',
                                            style: TextStyle(fontSize: 35.0)
                                          ),
                                        ),                            
                                      ) 
                                      :
                                      Container(),                                   

                                      Expanded(
                                      child: Padding(
                                          padding: EdgeInsets.only(left : Utils().screenWidth(context, 2)),
                                          child: 
                                          nonote != null ?
                                          Text(
                                            nonote,
                                            style: TextStyle(
                                                    fontSize: 30.0,
                                                    fontWeight: FontWeight.bold,
                                                    color : Color.fromRGBO(108,117, 125, 1.0)
                                            ),
                                          ):
                                          Text(
                                            "",
                                            style: TextStyle(
                                                    fontSize: 30.0,
                                                    fontWeight: FontWeight.bold,
                                                    color : Color.fromRGBO(108,117, 125, 1.0)
                                            ),
                                          ),

                                        ),
                                      ),                      
                                  ],
                                ),                                
                              ),                                                                                                                  
                            ],
                          ),    
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),                                    
          ),
          ],
        )      
        ]
      ),  
    );
  }
}