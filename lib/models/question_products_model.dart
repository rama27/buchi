import 'dart:convert';

QuestionProducts welcomeFromJson(String str) => QuestionProducts.fromJson(json.decode(str));

String welcomeToJson(QuestionProducts data) => json.encode(data.toJson());

class QuestionProducts {
    List<Questionproduct> questionproduct;

    QuestionProducts({
        this.questionproduct,
    });

    factory QuestionProducts.fromJson(Map<String, dynamic> json) => QuestionProducts(
        questionproduct: List<Questionproduct>.from(json["questionproduct"].map((x) => Questionproduct.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "questionproduct": List<dynamic>.from(questionproduct.map((x) => x.toJson())),
    };
}

class Questionproduct {
    int id;
    int idcategory;
    int idquestion;
    int yes;
    String nonote;
    String yesnote;
    int no;
    int start;
    int noend;
    int yesend;
    int status;
    DateTime createdAt;
    dynamic updatedAt;

    Questionproduct({
        this.id,
        this.idcategory,
        this.idquestion,
        this.yes,
        this.nonote,
        this.yesnote,
        this.no,
        this.start,
        this.noend,
        this.yesend,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Questionproduct.fromJson(Map<String, dynamic> json) => Questionproduct(
        id: json["id"],
        idcategory: json["idcategory"],
        idquestion: json["idquestion"],
        yes: json["yes"],
        nonote: json["nonote"] == null ? null : json["nonote"],
        yesnote: json["yesnote"] == null ? null : json["yesnote"],
        no: json["no"],
        start: json["start"],
        noend: json["noend"],
        yesend: json["yesend"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idcategory": idcategory,
        "idquestion": idquestion,
        "yes": yes,
        "nonote": nonote == null ? null : nonote,
        "yesnote": yesnote == null ? null : yesnote,
        "no": no,
        "start": start,
        "noend": noend,
        "yesend": yesend,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}

enum UpdatedAtEnum { THE_00011130000000 }

final updatedAtEnumValues = EnumValues({
    "-0001-11-30 00:00:00": UpdatedAtEnum.THE_00011130000000
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
