
import 'dart:convert';

FeatureProducts featureProductFromJson(String str) => FeatureProducts.fromJson(json.decode(str));

String featureProductToJson(Featureproduct data) => json.encode(data.toJson());

class FeatureProducts {
    List<Featureproduct> featureproduct;

    FeatureProducts({
        this.featureproduct,
    });

    factory FeatureProducts.fromJson(Map<String, dynamic> json) => FeatureProducts(
        featureproduct: List<Featureproduct>.from(json["featureproduct"].map((x) => Featureproduct.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "featureproduct": List<dynamic>.from(featureproduct.map((x) => x.toJson())),
    };
}

class Featureproduct {
    int id;
    int idproduct;
    int idfeature;
    int status;
    DateTime createdAt;
    DateTime updatedAt;

    Featureproduct({
        this.id,
        this.idproduct,
        this.idfeature,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Featureproduct.fromJson(Map<String, dynamic> json) => Featureproduct(
        id: json["id"],
        idproduct: json["idproduct"],
        idfeature: json["idfeature"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idproduct": idproduct,
        "idfeature": idfeature,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
