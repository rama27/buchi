class Configurationproduct {
    int id;
    int idproduct;
    int idgoods;
    int status;
    DateTime createdAt;
    DateTime updatedAt;
    String code;
    String name;
    int price;

    Configurationproduct({
        this.id,
        this.idproduct,
        this.idgoods,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.code,
        this.name,
        this.price
    });
  
    factory Configurationproduct.fromJson(Map<String, dynamic> json) => Configurationproduct(
        id: json["id"],
        idproduct: json["idproduct"],
        idgoods: json["idgoods"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        code : json["code"],
        name : json["name"],
        price : json["price"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idproduct": idproduct,
        "idgoods": idgoods,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "code": code,
        "name": name,
        "price": price
    };
}
