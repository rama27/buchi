import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';

import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/screen/productset/category/option/question/result/result_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:file_picker/file_picker.dart';
import 'package:mime/mime.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:toast/toast.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';

import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:open_file/open_file.dart';

class ContentDialogUploadFile extends StatefulWidget {
  @override
  _ContentDialogUploadFileState createState() =>
      _ContentDialogUploadFileState();
}

class _ContentDialogUploadFileState extends State<ContentDialogUploadFile> {
  File fileUpload;
  List listFileUpload;
  bool _pickFileinProgress = false;
  // To store the file provided by the image_picker
  File _imageFile;

  String _path;
  List<String> listPath;
  String _fileName;
  List<String> listFileName;
  
  bool _isHideBtnUpload = false;  
 // To track the file uploading state
  bool _isUploading = false;
  String _openResult = 'Unknown';

  @override
  void initState() {
    listFileUpload = new List();
    listFileUpload.clear();
    listFileName = new List();
    listFileName.clear();
    listPath = new List();
    listPath.clear();
    print("initState");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final String phpEndPoint = 'http://buchi-indonesia.co.id/document/stores';
    void navigationPage() {
      // Navigator.of(context).pushReplacementNamed('/home');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ResultScreen(
                    idproduct: null,
                    featureList: <Feature>[],
                    price: null,
                    product: null,
                    imageList: <Productimg>[],
                    keterangan: null,
                  )));
    }

    // void _openFileExplorer() async {
    //   setState(() => _pickFileinProgress = true);
    //   try {
    //     fileUpload = await FilePicker.getFile();
    //     _path = fileUpload.path;
    //     listPath.add(_path);
    //     _imageFile = fileUpload;
    //     listFileUpload.add(fileUpload);
    //     print("length ${listFileUpload.length}");
    //   } on PlatformException catch (e) {
    //     print("Unsupported operation" + e.toString());
    //   }
    //   if (!mounted) return;
    //   setState(() {
    //     _pickFileinProgress = false;
    //     _fileName = _path != null ? _path.split('/').last : '...';
    //     listFileName.add(_fileName);
    //   });
    // }

    _pickDocument() async {
      String result;

      try {
        setState(() {
          _pickFileinProgress = true;
          _path = '-';
        });

        result = await FlutterDocumentPicker.openDocument();
        _isHideBtnUpload = true;
      } catch (e) {
        print(e);
        result = 'Error: $e';
      } finally {
         print("finally");
        setState(() {
          _pickFileinProgress = false;
          _fileName = result != null ? result.split('/').last : '...';         
          listFileName.add(_fileName);
        });
      }

      //path  yang akan upload masukan var fileUpload
      setState(() {
        _path = result;
        listPath.add(_path);
        fileUpload = File(_path);
        listFileUpload.add(fileUpload);
        print("path $_path");
      });   

      for(var file in listFileName){
          print("filexx $file");
      }       
    }

    // void _upload(List<int> asset, String name) {
    //   if (fileUpload == null) return;
    //   String base64Image = base64Encode(asset);
    //   String fileName = fileUpload.path.split("/").last;

    //   http.post(phpEndPoint, body: {
    //     "filename[]": base64Image,
    //     "parameterid ": 0,
    //     "id": 1,
    //     "folderid  ": 0,
    //   }).then((res) {
    //     print(res.statusCode);
    //     Toast.show("${res.statusCode}", context,
    //         duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //   }).catchError((err) {
    //     print(err);
    //     Toast.show("$err", context,
    //         duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //   });
    // }

    // Future getPdfAndUpload() async {
    //   var rng = new Random();
    //   String randomName = "";
    //   for (var i = 0; i < 20; i++) {
    //     print(rng.nextInt(100));
    //     randomName += rng.nextInt(100).toString();
    //   }
    //   File file =
    //       await FilePicker.getFile(type: FileType.CUSTOM, fileExtension: 'pdf');
    //   String fileName = '${randomName}.pdf';
    //   print(fileName);
    //   print('${file.readAsBytesSync()}');
    //   _upload(file.readAsBytesSync(), fileName);
    // }

    // Future<void> openFile() async {
    //   final filePath = '/storage/emulated/0/update.apk';
    //   final result = await OpenFile.open(filePath);

    //   setState(() {
    //     _openResult = "type=${result.type}  message=${result.message}";
    //   });
    // }

    // void _resetState() {
    //   setState(() {
    //     _isUploading = false;
    //     _imageFile = null;
    //   });
    // }

    // Future<Map<String, dynamic>> _uploadImage(String path) async {
    //   // Toast.show("Image $path", context,
    //   //       duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //   setState(() {
    //     _isUploading = true;
    //   });
    //   // Find the mime type of the selected file by looking at the header bytes of the file
    //   final mimeTypeData =
    //       lookupMimeType(path, headerBytes: [0xFF, 0xD8]).split('/');
    //   // Intilize the multipart request
    //   final imageUploadRequest =
    //       http.MultipartRequest('POST', Uri.parse(phpEndPoint));
    //   // Attach the file in the request
    //   final file = await http.MultipartFile.fromPath('filename[]', path,
    //       contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    //   // Explicitly pass the extension of the image with request body
    //   // Since image_picker has some bugs due which it mixes up
    //   // image extension with file name like this filenamejpge
    //   // Which creates some problem at the server side to manage
    //   // or verify the file extension
    //   // imageUploadRequest.fields['ext'] = mimeTypeData[1];
    //   imageUploadRequest.fields['parameterid'] = '0';
    //   imageUploadRequest.fields['id'] = '1';
    //   imageUploadRequest.fields['folderid'] = '0';
    //   imageUploadRequest.files.add(file);
    //   try {
    //     final streamedResponse = await imageUploadRequest.send();
    //     final response = await http.Response.fromStream(streamedResponse);
    //     if (response.statusCode != 200) {
    //       return null;
    //     }
    //     final Map<String, dynamic> responseData = json.decode(response.body);
    //     _resetState();
    //     return responseData;
    //   } catch (e) {
    //     print(e);
    //     return null;
    //   }
    // }

    //upload file to server
    Upload(File imageFile) async {

      setState(() {
        _isUploading = true;  
      });
      
      print("path will upload ${imageFile.path}");
      
      
      listFileUpload.forEach((f) async {
          print("file path ${f.path}");
          var stream = new http.ByteStream(DelegatingStream.typed(f.openRead()));
          var length = await f.length();
          var uri = Uri.parse(phpEndPoint);
          var request = new http.MultipartRequest("POST", uri);
          var multipartFile = new http.MultipartFile('filename[]', stream, length,
                filename: basename(f.path));
              //contentType: new MediaType('image', 'png'));
          request.fields['parameterid'] = '0';
          request.fields['id'] = '1';
          request.fields['folderid'] = '0';
          request.files.add(multipartFile);
          var response = await request.send();
          print(response.statusCode);
          response.stream.transform(utf8.decoder).listen((value) {
            print(value);
            setState(() {
                _isUploading = false;
            });          
            Toast.show("Berhasil Upload", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          });
      });
    }

    // void _startUploading() async {
    //   final Map<String, dynamic> response = await _uploadImage(_path);
    //   print(response);
    //   // Check if any error occured
    //   if (response == null || response.containsKey("error")) {
    //     Toast.show("Image Upload Failed!!!", context,
    //         duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //   } else {
    //     Toast.show("Image Uploaded Successfully!!!", context,
    //         duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //   }
    // }

    double c_width = MediaQuery.of(context).size.width * 0.90;
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: c_width,
        height: Utils().screenHeight(context, 60.0), //seting heigth card
        child: Card(
          elevation: 10.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0, left: 60.0),
                    child: Text(
                      "Upload New Files",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 40),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(40.0, 60.0, 50.0, 0.0),
                      child: Image.asset('assets/images/close24.png'),
                    ),
                  )
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 50.0, left: 60.0, right: 40.0),
                child: Text(
                  "Upload File:",
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 35),
                ),
              ),

              //button choose file
              Padding(
                padding:
                    const EdgeInsets.only(left: 60.0, right: 60.0, top: 20.0),
                child: Container(
                  height: 100,
                  width: c_width,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 2.0),
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 1.0, color: Colors.black),
                      left: BorderSide(width: 1.0, color: Colors.black),
                      right: BorderSide(width: 1.0, color: Colors.black),
                      bottom: BorderSide(width: 1.0, color: Colors.black),
                    ),
                    color: Colors.white,
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 0.0),
                      child: Center(
                        child: RaisedButton(
                          // onPressed: ()  {
                          //   // _openFileExplorer();
                          //   // getPdfAndUpload();
                          //   //  await FlutterDocumentPicker.openDocument();
                          //   openFile();
                          // },
                          onPressed: _pickDocument,
                          child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20.0, horizontal: 120.0),
                              child: Text('Choose File',
                                  style: TextStyle(fontSize: 30))),
                        ),
                      ),
                    ),                   
                  ),
                ),
              ),

              //list file name yang akan tampil
              _isUploading ?
              Padding(
                padding: const EdgeInsets.all(38.0),
                child: Center(
                child: SizedBox(
                      child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Color.fromRGBO(53, 216, 128, 1)),
                      ),                
                      height: 150,
                      width: 150,
                  ),
                ),
              )
              :
              new Builder(
                builder: (BuildContext context) => _pickFileinProgress
                    ? Padding(
                        padding:
                            const EdgeInsets.only(bottom: 10.0, left: 20.0),
                        child: Center(child: const CircularProgressIndicator()))
                    : _path != null
                        ? Container(
                            padding:
                                const EdgeInsets.only(bottom: 30.0, left: 0.0),
                            height: MediaQuery.of(context).size.height * 0.25,
                            child: Scrollbar(
                                child: ListView.separated(
                              itemCount: listFileName.length,
                              itemBuilder: (BuildContext context, int index) {
                                return ListTile(
                                  title: Text(
                                    listFileName[index],
                                  ),
                                  subtitle: Text(listPath[index]),
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      Divider(),
                            )),
                          )
                        : new Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                        padding: EdgeInsets.only(
                            top: 50.0, right: 50.0, bottom: 40.0),
                        child: new SizedBox(
                          width: Utils().screenWidth(context, 15),
                          height: Utils().screenHeight(context, 6),
                          child: RaisedButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  side: BorderSide(color: Colors.white)),
                              child: Padding(
                                child: Text(
                                  'Close',
                                  style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 30.0,
                                      color: Colors.white),
                                ),
                                padding: EdgeInsets.all(0),
                              ),
                              color: Colors.grey,
                              textColor: Colors.white,
                              onPressed: () {
                                Navigator.of(context).pop();
                              }),
                        )),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: 
                    _isHideBtnUpload ?
                          Padding(
                        padding: EdgeInsets.only(
                            top: 50.0, right: 50.0, bottom: 40.0),
                        child: new SizedBox(
                          width: Utils().screenWidth(context, 17),
                          height: Utils().screenHeight(context, 6),
                          child: RaisedButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  side: BorderSide(color: Colors.white)),
                              child: Padding(
                                child: 
                                      Text(
                                        'Upload',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 30.0,
                                            color: Colors.white),
                                      ),
                                padding: EdgeInsets.all(0),
                              ),
                              color: Colors.blue,
                              textColor: Colors.white,
                              onPressed: () {
                                // _upload();
                                // _startUploading();
                                // navigationPage();
                                Upload(fileUpload);
                              }),
                        )
                    )    
                    : 
                     Container()      
                    //     Center(
                    //     child: SizedBox(
                    //       child: CircularProgressIndicator(
                    //         valueColor: new AlwaysStoppedAnimation<Color>(Color.fromRGBO(53, 216, 128, 1)),
                    //       ),                
                    //       height: 50.0,
                    //       width: 50.0,
                    //     ),
                    //  )         
                                  
                  ),
                ],
              ),
              //Button
            ],
          ),
        ),
        margin: EdgeInsets.only(bottom: 300, left: 5.0, right: 5.0),
      ),
    );
  }
}
