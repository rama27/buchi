import 'dart:io';

import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/screen/productset/category/option/question/result/result_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';

class ContentNewFolder extends StatelessWidget {  
  var _controller = TextEditingController();      
  @override
  Widget build(BuildContext context) {
    Future<File> getFile() async {
        File file = await FilePicker.getFile();
        return file;
    }
  
    void navigationPage() { 
      // Navigator.of(context).pushReplacementNamed('/home');
      Navigator.push(context,
      MaterialPageRoute(builder: (context) => ResultScreen(idproduct: null, featureList: <Feature>[], price: null, product: null, imageList: <Productimg>[], keterangan: null,)
      ));
    }

    double c_width = MediaQuery.of(context).size.width*0.90;  
    return  Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: c_width,
                height: Utils().screenHeight(context, 35.0),  //seting heigth card                
                child: Card(
                    elevation: 10.0,                
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top : 50.0,  left : 60.0),
                                child: Text("Create New Folder",
                                      style: TextStyle(fontWeight: FontWeight.normal, fontSize: 40),),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.of(context).pop();
                                },
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(40.0,60.0,50.0,0.0),
                                  child: Image.asset('assets/images/close24.png'),
                                ),                                
                              )
                            ],
                          ),

                        Padding(
                              padding: const EdgeInsets.only(top : 50.0,  left : 60.0, right: 40.0),
                              child: Text("Folder name:",
                              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 35),),
                        ),

                         Padding(
                           padding: const EdgeInsets.only(left : 60.0,right: 60.0, top : 40.0),
                           child: TextFormField(  
                            keyboardType: TextInputType.emailAddress,
                            controller: _controller,
                            autofocus: false,
                            validator: (value) {
                            if (value.isEmpty) {
                                return 'Please enter email address';
                              }
                              return null;
                            },  
                            style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
                            decoration: InputDecoration(
                              labelText: '',
                              hintText: '',
                              contentPadding: new EdgeInsets.symmetric(vertical: 25.0, horizontal: 50.0),
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                              ),    
                        ),
                         ),                    
                        
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.bottomRight,
                                child: Padding(
                                padding: EdgeInsets.only(top: 50.0, right: 50.0, bottom: 40.0),   
                                child : new SizedBox(      
                                width : Utils().screenWidth(context, 12),   
                                height: Utils().screenHeight(context, 6),
                                child : RaisedButton(
                                  shape : new RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(10.0),
                                      side: BorderSide(color: Colors.white)), 
                                  child : Padding( 
                                      child: Text('Close',style: TextStyle(
                                                                  fontWeight: FontWeight.normal,
                                                                  fontSize: 30.0,
                                                                  color: Colors.white
                                                                ),
                                                                ),
                                    padding: EdgeInsets.all(0),
                                  ),
                                  color: Colors.grey,
                                  textColor:  Colors.white, 
                                  onPressed: ()  {
                                    navigationPage();                                                               
                                  }
                                  ),
                                  )
                                ),
                              ),
                           Align(
                                alignment: Alignment.bottomRight,
                                child: Padding(
                                padding: EdgeInsets.only(top: 50.0, right: 50.0, bottom: 40.0),   
                                child : new SizedBox(      
                                width : Utils().screenWidth(context, 12),   
                                height: Utils().screenHeight(context, 6),
                                child : RaisedButton(
                                  shape : new RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(10.0),
                                      side: BorderSide(color: Colors.white)), 
                                  child : Padding( 
                                      child: Text('Save',style: TextStyle(
                                                                  fontWeight: FontWeight.normal,
                                                                  fontSize: 30.0,
                                                                  color: Colors.white
                                                                ),
                                                                ),
                                    padding: EdgeInsets.all(0),
                                  ),
                                  color: Colors.blue,
                                  textColor:  Colors.white, 
                                  onPressed: ()  {
                                    navigationPage();                                                               
                                  }
                                  ),
                                  )
                                ),
                              ),   
                          ],
                        )                                           
                        //Button  
                             
                      ],
                    ),
                  ),                                      
                  margin: EdgeInsets.only(bottom: 300, left: 5.0, right: 5.0),               
                  ),
                );                  
              }
}