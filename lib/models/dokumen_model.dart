class DokumenModel {
    List<Dokumen> document;

    DokumenModel({
        this.document,
    });

    factory DokumenModel.fromJson(Map<String, dynamic> json) => DokumenModel(
        document: List<Dokumen>.from(json["document"].map((x) => Dokumen.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "document": List<dynamic>.from(document.map((x) => x.toJson())),
    };
}

class Dokumen {
    int id;
    int idcategory;
    String name;
    String type;
    int status;
    int folderid;
    int userid;
    DateTime createdAt;
    String updatedAt;

    Dokumen({
        this.id,
        this.idcategory,
        this.name,
        this.type,
        this.status,
        this.folderid,
        this.userid,
        this.createdAt,
        this.updatedAt,
    });

    factory Dokumen.fromJson(Map<String, dynamic> json) => Dokumen(
        id: json["id"],
        idcategory: json["idcategory"],
        name: json["name"],
        type: json["type"],
        status: json["status"],
        folderid: json["folderid"],
        userid: json["userid"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idcategory": idcategory,
        "name": name,
        "type": type,
        "status": status,
        "folderid": folderid,
        "userid": userid,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}
