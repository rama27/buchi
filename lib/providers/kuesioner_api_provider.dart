import 'package:buchi/models/kuisoner_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

class KuisonerApiProvider {
  
  Future <List<Kuisoner>> getAllKuesioner() async {
    var url = Consts.base_url+"kuisoner";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    // print("RESP QUESTIONS ${response.data}");
    responseData = json.decode(response.data);//response JSON data didecode agar bisa di parse    
    print("nodecode $responseData");
    int total = responseData["total"];
    int pages = responseData["pages"];   
    DataConfig().setDataInt("total_kuisoner", total);
    DataConfig().setDataInt("pages_kuisoner", pages);
    List responseJson =responseData["kuisoner"]; //ambil data json question masukan ke variabel tipe data list              
    print("responseJsonKuisoner $responseJson");
    //fungsi map disini persis looping for 
    //yaitu mengeluarkan json object satu persatu data dari json array 
    return (responseJson).map((val) {           
      print('Inserting $val');            
      DBProvider.db.createKuisoner(Kuisoner.fromJson(val));
    }).toList();

  }
}