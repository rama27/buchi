import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:json_table/json_table.dart';

class Table_Price extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Decode your json string
final String jsonSample='[{"id":1},{"id":2}]';
var json = jsonDecode(jsonSample);

//Simply pass this json to JsonTable
  return JsonTable(
   json,
   tableHeaderBuilder: (String header) {
     return Container(
       padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
       decoration: BoxDecoration(border: Border.all(width: 0.5),color: Colors.grey[300]),
       child: Text(
         header,
         textAlign: TextAlign.center,
         style: Theme.of(context).textTheme.display1.copyWith(fontWeight: FontWeight.w700, fontSize: 14.0,color: Colors.black87),
       ),
     );
   },
   tableCellBuilder: (value) {
     return Container(
       padding: EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
       decoration: BoxDecoration(border: Border.all(width: 0.5, color: Colors.grey.withOpacity(0.5))),
       child: Text(
         value,
         textAlign: TextAlign.center,
         style: Theme.of(context).textTheme.display1.copyWith(fontSize: 14.0, color: Colors.grey[900]),
       ),
     );
   },
 );
  }
}