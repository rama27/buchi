import 'dart:convert';

import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:buchi/models/product_model.dart';
import 'package:dio/dio.dart';

class ProductApiProvider {
  
  Future <List<Product>> getAllProduct() async {
    var url = Consts.base_url+"products";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    // print("RESP QUESTIONS ${response.data}");
    responseData = json.decode(response.data);//response JSON data didecode agar bisa di parse    
    // print("nodecode ${response.data["question"]}");   
    List responseJson =responseData["product"]; //ambil data json question masukan ke variabel tipe data list              

    return (responseJson).map((product) {     
      // print('Inserting $question');      
      DBProvider.db.createProduct(Product.fromJson(product));
    }).toList();

  }
}