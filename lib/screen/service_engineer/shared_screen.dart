
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/dialog/content_dialog_upload_file.dart';
import 'package:buchi/widget/dialog/content_new_folder.dart';
import 'package:buchi/widget/shared/shared_item.dart';
import 'package:flutter/material.dart';

class SharedScreen extends StatefulWidget {
  
  @override
  _SharedScreenState createState() => _SharedScreenState();
}

class _SharedScreenState extends State<SharedScreen> {

  @override
  void initState() {
    print("initState");
    DBProvider.db.getAllBeginningDocument(0, 0).then((val){
      print("service engineer $val");
      for(var v in val ){
        if(v.status != 0){
          print("name ${v.name}");  
        }     
      }
      DBProvider.db.getFolderContent(0,0).then((val){
        for(var v in val ){
          if(v.status != 0){
           if(v.parent == 0){
              print("name folder ${v.name}");  
            }     
          }   
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();  
 
  var myFabButton = Container(
      width: 400.0,
      height: 400.0,
      child: new RawMaterialButton(
        shape: new CircleBorder(),
        elevation: 0.0,
        child: Icon(
          Icons.favorite,
          color: Colors.blue,
         ),
      onPressed: (){},
  )
  );

  var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
               Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),      
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                // Padding(
                //   padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 55)),
                //   child: IconButton(
                //     padding: EdgeInsets.all(5.0),
                //     icon: Image.asset('assets/images/refresh.png'),
                //     onPressed: () {
                //       // Implement navigation to shopping cart page here...
                //       print('Click Menu');
                //     },
                //   ),
                // ),                        
              ],
          ),                                                                                                                                                 
      ),   
    );
 
    var size = MediaQuery.of(context).size;  
    var appBarHeight = appBar.preferredSize.height;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - appBarHeight - 24) / 3.2;
    final double itemWidth = size.width / 2;
    
    void showDialog(){
        showGeneralDialog(
        barrierLabel: "Label",
        barrierDismissible: true,           
        transitionDuration: Duration(milliseconds: 700),
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return ContentDialogUploadFile();
        },
              transitionBuilder: (context, anim1, anim2, child) {
                return SlideTransition(
                  position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                  child: child,
                );
              },
      );    
    }

  void showDialog2(){
        showGeneralDialog(
        barrierLabel: "Label",
        barrierDismissible: true,           
        transitionDuration: Duration(milliseconds: 700),
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return ContentNewFolder();
        },
              transitionBuilder: (context, anim1, anim2, child) {
                return SlideTransition(
                  position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                  child: child,
                );
              },
      );    
    }
    return Scaffold(
      appBar: appBar,
      body: Stack(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),
          Container(
          height: Utils().screenHeight(context, 90),
          child: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: (itemWidth / itemHeight),
          children: <Widget>[
            GestureDetector(
              child: SharedItem(path: 'assets/images/folder.png', title:"Unesa Surabaya",),
              onTap: () {               
              },
            ),
            // GestureDetector(
            //   child: SharedItem(path: 'assets/images/pdf.png', title:"AN_Distilled_solvent_R100_final.pdf",),
            //   onTap: () {               
            //   },
            // ),
            // GestureDetector(
            //   child: SharedItem(path: 'assets/images/file.png', title:"Feedback Form Trial_rev2.docx Request - Form Trial_rev2.docx",),
            //   onTap: () {               
            //   },
            // ),
            // GestureDetector(
            //   child: SharedItem(path: 'assets/images/video.png', title:"BUCHI Products - General Overview.mp4",),
            //   onTap: () {               
            //   },
            // ),
          ],
        ),
        ),
        
        Positioned(
         bottom: 170.0,
         right: 30.0, 
         child: Align(
            alignment: Alignment.bottomRight,
            child: Container(
            height: 100.0,
            width: 100.0,
            child: FittedBox(
                child: FloatingActionButton(onPressed: () {
                                                             showDialog();
                                                          },
                                            backgroundColor: Colors.deepOrangeAccent,
                                            child: Icon(Icons.note_add),
                                            heroTag: "btn1",),
            ),
           ),
          ),
        ), 

        Positioned(
          bottom: 50.0,
          right: 30.0, 
          child: Align(
            alignment: Alignment.bottomRight,
            child: Container(
            height: 100.0,
            width: 100.0,
            child: FittedBox(
                child: FloatingActionButton(onPressed: () {showDialog2();},
                                            backgroundColor: Colors.orange,
                                            child: Icon(Icons.create_new_folder),
                                            heroTag: "btn2",),
            ),
           ),
          ),
        ),       
      
        ],        
      ),     
    );
  }
}