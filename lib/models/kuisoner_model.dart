class KuisonerModel {
    List<Kuisoner> kuisoner;

    KuisonerModel({
        this.kuisoner,
    });

    factory KuisonerModel.fromJson(Map<String, dynamic> json) => KuisonerModel(
        kuisoner: json["kuisoner"] != null ? List<Kuisoner>.from(json["kuisoner"].map((x) => Kuisoner.fromJson(x))) : List<Kuisoner>(),
    );

    Map<String, dynamic> toJson() => {
        "kuisoner": List<dynamic>.from(kuisoner.map((x) => x.toJson())),
    };
}

class Kuisoner{
    int id;
    String nama;
    DateTime createdAt;
    DateTime updatedAt;
    int status;
    String keterangan;

    Kuisoner({
        this.id,
        this.nama,
        this.createdAt,
        this.updatedAt,
        this.status,
        this.keterangan,
    });

    factory Kuisoner.fromJson(Map<String, dynamic> json) => Kuisoner(
        id: json["id"],
        nama: json["nama"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        status: json["status"],
        keterangan: json["keterangan"] == null ? null : json["keterangan"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "status": status,
        "keterangan": keterangan == null ? null : keterangan,
    };
}
