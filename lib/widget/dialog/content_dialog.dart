import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';

class ContentDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     double c_width = MediaQuery.of(context).size.width*0.80;
    return  Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: c_width,
                height: Utils().heightDivided(context, 4.5),  //seting heigth card                
                child: Card(
                    elevation: 10.0,                
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top : 50.0,  left : 60.0),
                            child: Text("Gagal Login",
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50),),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top : 30.0, left : 60.0),
                            child: Text("Silahkan Login Kembali",
                                 style: TextStyle(fontWeight: FontWeight.normal, fontSize: 35)),
                          ),
                             //Button  
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Padding(
                                    padding: const EdgeInsets.only(top: 20.0, right: 80.0),   
                                    child : new SizedBox(      
                                    width : Utils().screenWidth(context, 20),   
                                    height: Utils().screenHeight(context, 7),
                                    child : RaisedButton(
                                      shape : new RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.circular(10.0),
                                          side: BorderSide(color: Colors.white)), 
                                      child : Padding( 
                                          child: Text('Okay',style: TextStyle(
                                                                      fontWeight: FontWeight.normal,
                                                                      fontSize: 45.0,
                                                                      color: Colors.white
                                                                    ),
                                                                    ),
                                        padding: EdgeInsets.all(14),
                                      ),
                                      color: Color.fromRGBO(40, 167, 69, 100),
                                      textColor:  Colors.white, 
                                      onPressed: ()  {                                                                
                                        Navigator.pop(context);                                  
                                      }
                                      ),
                                      )
                                    ),
                                  ),
                      ],
                    ),
                  ),                                      
                  margin: EdgeInsets.only(bottom: 500, left: 5.0, right: 5.0),               
                  ),
                );                  
              }
}