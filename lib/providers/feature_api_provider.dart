import 'dart:convert';
import 'package:buchi/models/feature_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:buchi/models/feature_product_model.dart';
import 'package:dio/dio.dart';

class FeatureApiProvider {
  
  Future <List<Feature>> getAllFeature() async {
    var url = Consts.base_url+"feature";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    // print("RESP QUESTIONS ${response.data}");
    responseData = json.decode(response.data);//response JSON data didecode agar bisa di parse    
    // print("nodecode ${response.data["question"]}");   
    List responseJson =responseData["feature"]; //ambil data json question masukan ke variabel tipe data list              
    print("responseJsonFeature $responseJson");
    return (responseJson).map((featureproduct) {     
      // print('Inserting $question');      
      DBProvider.db.createFeature(Feature.fromJson(featureproduct));
    }).toList();

  }
}