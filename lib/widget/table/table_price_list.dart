import 'package:flutter/material.dart';
import 'package:json_table/json_table.dart';

class TablePriceList extends StatefulWidget {

  String cellLeft;
  String cellRight;
  String cellMiddle;

  TablePriceList({@required this.cellLeft,
                  @required this.cellMiddle,
                  @required this.cellRight});

  @override
  _TablePriceListState createState() => _TablePriceListState();
}


class _TablePriceListState extends State<TablePriceList> {

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }


  @override
  Widget build(BuildContext context) {
    return  Table(
          border: TableBorder.all(color: Colors.black),
          children: [
            TableRow(children: [
              Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(widget.cellLeft,style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        color : Color.fromRGBO(108,117, 125, 1.0)
                        ),
                ),
              ), 

              Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(widget.cellMiddle,style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        color : Color.fromRGBO(108,117, 125, 1.0)
                     ),
                ),
              ), 
           
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(widget.cellRight,style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                    ),
              ),                               
            ]),                                 
        
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("000394",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Y-piece D10mms',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('400,000.00',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                                                       
              ]
            ),

           TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("000398",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Gasket SVL30x18 PTFE',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('1,100,000.00',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                                                       
              ]
            ),

           TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("000420",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Drying flask 24/40 1000ml',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('4,000,000.00',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                                                       
              ]
            ),

            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("000421",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Receiving flask S35 50ml',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('3,600,000.00',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                                                       
              ]
            ),

            
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("000422",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('	Receiving flask S35 100ml',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('2,800,000.00',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                                                       
              ]
            ),


           ],
        );

  }
}