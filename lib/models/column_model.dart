class Columns {
    String name;
    String code;
   

    Columns({
        this.name,
        this.code,       
    });

    factory Columns.fromMap(Map<String, String> map) => Columns(
        code: map["code"],
        name: map["name"],       
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,       
    };
}
