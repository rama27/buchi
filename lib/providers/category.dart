import 'dart:convert';
import 'dart:io';

import 'package:buchi/models/category_response.dart';
import 'package:buchi/utils/consts.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class CategoryProvider with ChangeNotifier{
   List<CategoryResp> _items = [];

   List<CategoryResp> get items{
      return _items;
   }

   void setItem(List<CategoryResp> items){
      _items = items;
   }

  Future<List<CategoryResp>> getCategory(
    String start , String perpage, String search) async{
    print("getCategory");  
    Map<String,dynamic> responseData;    
    List responseJson;      
    var baseurl = Consts.base_url;
    var url = baseurl + "category"; 
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');   
    // Map<String, String> headers = {"access-token": "TmU4Z0VDTkZFT2JjVzZ5QVM4aHFxdDY0dGhxUnczRnlDVXFGWDlUdU5CVE1iczQwU3o0U2tlbW1VR05i5e31a75c23646"};    
    Map<String, String> headers = 
    {'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'bmQ2cFVIM0ljS2M3TTliTlpkS3Y5SGlUbHEzd1FhQ0RJTGNqeG9nOUZqa1k4eUx0R0RGclluNjI5ang05e31bba4eeecb',};    
    // Map<String, String> headers = {HttpHeaders.authorizationHeader: "bmQ2cFVIM0ljS2M3TTliTlpkS3Y5SGlUbHEzd1FhQ0RJTGNqeG9nOUZqa1k4eUx0R0RGclluNjI5ang05e31bba4eeecb"};
    print("CATEGORY $url $headers ");
 
    try{
        final response = await http.get(
        url, 
        headers:headers
        );               
        notifyListeners();
        print("${response.body}");
        responseData = json.decode(response.body);  
        // print("$responseData");
        responseJson = responseData["category"];
        _items =  responseJson.map((m) => new CategoryResp.fromJson(m)).toList();   
        print(responseJson);
        return responseJson.map((m) => new CategoryResp.fromJson(m)).toList();  
                               

    } catch(error)  {
          // print("auth "+error);
          //throw HttpException;
          
    }
    //  return responseJson.map((m) => new Lokasi.fromJson(m)).toList();  
  }  

  void printWrapped(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
} 
  


}