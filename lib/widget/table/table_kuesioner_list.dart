import 'package:flutter/material.dart';

class TableKuesinonerList extends StatelessWidget {

  String cellLeft;
  String cellRight;
  String cellMiddle;

  TableKuesinonerList(
                      {
                         @required this.cellLeft,
                        @required this.cellMiddle,
                        @required this.cellRight
                      }
                      );
  @override
  Widget build(BuildContext context) {
    return  Table(
          border: TableBorder.all(color: Colors.black),
          children: [
            TableRow(children: [
              Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(cellLeft,style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        color : Color.fromRGBO(108,117, 125, 1.0)
                        ),
                ),
              ), 

              Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(cellMiddle,style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        color : Color.fromRGBO(108,117, 125, 1.0)
                        ),
                ),
              ), 
           
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(cellRight,style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                    ),
              ),                               
            ]),                                 
        
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Feedback Form Trial / Demo Unit",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('18 December 2019 13:48',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Open",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Colors.blue
                            ),),
                ),                                                       
              ]
            ),

           TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Kuisoner 2019",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('11 December 2019 22:25',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Open",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Colors.blue
                            ),),
                ),                                                    
              ]
            ),

           TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Survey Kepuasan Pelanggan",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('18 December 2019 13:35',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),                 
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Open",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Colors.blue
                            ),),
                ),                                                           
              ]
            ),
      
           ],
        );

  }
}