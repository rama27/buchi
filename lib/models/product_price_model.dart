import 'dart:convert';

Productprices productPriceFromJson(String str) => Productprices.fromJson(json.decode(str));

String productPriceToJson(Productprices data) => json.encode(data.toJson());

class Productprices {
    List<Productprice> productprice;

    Productprices({
        this.productprice,
    });

    factory Productprices.fromJson(Map<String, dynamic> json) => Productprices(
        productprice: List<Productprice>.from(json["productprice"].map((x) => Productprice.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "productprice": List<dynamic>.from(productprice.map((x) => x.toJson())),
    };
}

class Productprice {
    int id;
    String code;
    String name;
    int price;
    int status;
    DateTime createdAt;
    DateTime updatedAt;

    Productprice({
        this.id,
        this.code,
        this.name,
        this.price,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Productprice.fromJson(Map<String, dynamic> json) => Productprice(
        id: json["id"],
        code: json["code"],
        name: json["name"],
        price: json["price"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
        "price": price,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
