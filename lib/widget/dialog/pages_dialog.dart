import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/screen/productset/category/option/question/question_screen.dart';
import 'package:buchi/screen/productset/category/option/question/result/result_screen.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';

class PagesDialog extends StatefulWidget {  
  
  int jumlahpages;
 

  PagesDialog({@required this.jumlahpages});    

  @override
  _PagesDialogState createState() => _PagesDialogState();
}



class _PagesDialogState extends State<PagesDialog> {
   String baseurlimg="http://buchi-indonesia.co.id/img/img/answer/";
   var list;
   List<Widget> widgets;
   int _selectedPages;

    /// the list of positive integers starting from 0
  Iterable<int> get positiveIntegers sync* {
    int i = 0;
    while (true) yield i++;
  }

  @override
  void initState() {
    list = positiveIntegers
      .skip(1)   // don't use 0
      .take(widget.jumlahpages)  // take 10 numbers
      .toList(); // create a list
    // print(list); 
      // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    // for(var l in list){
    //   listWidget(l.toString());
    //   widgets = [Text(list),];
    // }  
    super.initState();
  }

  Widget buildListWidget(String item){ 
    final curScaleFactor = MediaQuery.of(context).textScaleFactor;
    return Center(
      child: Container(
      height : Utils().screenHeight(context, 60),  
      width : Utils().screenHeight(context, 10),
      padding: const EdgeInsets.all(16.0),
      child: 
      new ListView.builder
      (
        itemCount: list.length,
        itemBuilder: (BuildContext ctxt, int index) {
        return InkWell(
            child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: new Text(list[index].toString(), 
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 50 * curScaleFactor ),
            ),
          ),
          onTap: ()
          {
            setState(() {
               _selectedPages=list[index];
            });           
            print("clicked");
          },
        );
        }
      )
      // ListView(
      //    scrollDirection: Axis.vertical,// As you expect multiple lines you need a column not a row
      //    children: <Widget>[
      //        Text(item, style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30 * curScaleFactor ),),   
      //    ]
      // ),
      ),
    );
  }
  

  @override
  Widget build(BuildContext context) {    
    // print("jumlahpages ${widget.jumlahpages}");

    void navigationPage() { 
      // Navigator.of(context).pushReplacementNamed('/home');
      Navigator.push(context,
      MaterialPageRoute(builder: (context) => ResultScreen(idproduct: null, featureList: <Feature>[], price: null, product: null, imageList: <Productimg>[], keterangan: null,)
      ));
    }

    double c_width = MediaQuery.of(context).size.width*0.80;
    // String keterangan = "System evaporasi yang BUCHI miliki, dilengkapi dengan Foaming Sensor. Foaming Sensor ini akan bekerja, ketika sample extract mulai berbuih, maka system akan melakukan  aerasi /menaikkan tekanan vacuum secara otomatis. Sehingga destilat yang sudah murni tidak terkontaminasi sample";
    return  Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: c_width,
                height: Utils().screenHeight(context, 40.0),  //seting heigth card                
                child: Card(
                    elevation: 10.0,                
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top : 50.0,  left : 60.0),
                                child: Text("Pilih Pages",
                                      style: TextStyle(fontWeight: FontWeight.normal, fontSize: 40),),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.of(context).pop();                                                                
                                },
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(40.0,60.0,50.0,0.0),
                                  child: Image.asset('assets/images/close24.png'),                                  
                                ),                                
                              )
                            ],
                          ),
                         Expanded(child: buildListWidget(list.toString())),              
                                                                     
                        //Button  
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Padding(
                                    padding: EdgeInsets.only(top: 50.0, right: 50.0, bottom: 40.0),   
                                    child : new SizedBox(      
                                    width : Utils().screenWidth(context, 17),   
                                    height: Utils().screenHeight(context, 6),
                                    child : RaisedButton(
                                      shape : new RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.circular(10.0),
                                          side: BorderSide(color: Colors.white)), 
                                      child : Padding( 
                                          child: Text('Submit',style: TextStyle(
                                                                      fontWeight: FontWeight.normal,
                                                                      fontSize: 30.0,
                                                                      color: Colors.white
                                                                    ),
                                                                    ),
                                        padding: EdgeInsets.all(0),
                                      ),
                                      color: Colors.blue,
                                      textColor:  Colors.white, 
                                      onPressed: ()  {
                                        // navigationPage();   
                                        DataConfig().setDataInt("selectedpage", _selectedPages);
                                        Navigator.pop(context, 'success');                                                            
                                      }
                                      ),
                                      )
                                    ),
                                  ),
                      ],
                    ),
                  ),                                      
                  margin: EdgeInsets.only(bottom: 300, left: 5.0, right: 5.0),               
                  ),
                );                  
              }
}