import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Color backgroundColor = Colors.red;
  final Text title;
  final AppBar appBar;
  final List<Widget> widgets;

  /// you can add more fields that meet your needs

  const BaseAppBar({Key key, this.title, this.appBar, this.widgets})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    return PreferredSize(
      preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
      child : new AppBar(                       
            backgroundColor: Colors.white,                                 
            flexibleSpace: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,            
              children: <Widget>[
                 Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                    child: Container(child: GestureDetector(
                                            child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                            onTap: () => _scaffoldKey.currentState.openDrawer(),
                                            ),
                                            
                                    ),
                  ), 
                  Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                    child: Image.asset(
                      'assets/images/buchi_logo2.png',                                                    
                      height: Utils().heightDivided(context, 25),
                    ),
                  ), 
                  Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 1.9)),
                    child: IconButton(
                      padding: EdgeInsets.all(5.0),
                      icon: Image.asset('assets/images/refresh.png'),
                      onPressed: () {
                        // Implement navigation to shopping cart page here...
                        print('Click Menu');
                      },
                    ),
                  ),                        
                ],
             ),                                                                                                                                                 
      ),   
    );
  }

 @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
  
}