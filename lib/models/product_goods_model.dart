import 'dart:convert';

ProductGoods productGoodsFromJson(String str) => ProductGoods.fromJson(json.decode(str));

String productGoodsToJson(ProductGoods data) => json.encode(data.toJson());

class ProductGoods {
    List<Productgood> productgoods;

    ProductGoods({
        this.productgoods,
    });

    factory ProductGoods.fromJson(Map<String, dynamic> json) => ProductGoods(
        productgoods: List<Productgood>.from(json["productgoods"].map((x) => Productgood.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "productgoods": List<dynamic>.from(productgoods.map((x) => x.toJson())),
    };
}

class Productgood {
    int id;
    int idproduct;
    int idgoods;
    int status;
    DateTime createdAt;
    DateTime updatedAt;

    Productgood({
        this.id,
        this.idproduct,
        this.idgoods,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Productgood.fromJson(Map<String, dynamic> json) => Productgood(
        id: json["id"],
        idproduct: json["idproduct"],
        idgoods: json["idgoods"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idproduct": idproduct,
        "idgoods": idgoods,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
