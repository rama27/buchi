class UserResponse {
  String name;
  String email;
  Createdat createdat;
  Createdat lastlogin;
  Null img;

  UserResponse(
      {this.name, this.email, this.createdat, this.lastlogin, this.img});

  UserResponse.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    createdat = json['createdat'] != null
        ? new Createdat.fromJson(json['createdat'])
        : null;
    lastlogin = json['lastlogin'] != null
        ? new Createdat.fromJson(json['lastlogin'])
        : null;
    img = json['img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    if (this.createdat != null) {
      data['createdat'] = this.createdat.toJson();
    }
    if (this.lastlogin != null) {
      data['lastlogin'] = this.lastlogin.toJson();
    }
    data['img'] = this.img;
    return data;
  }
}

class Createdat {
  String date;
  int timezoneType;
  String timezone;

  Createdat({this.date, this.timezoneType, this.timezone});

  Createdat.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    timezoneType = json['timezone_type'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['timezone_type'] = this.timezoneType;
    data['timezone'] = this.timezone;
    return data;
  }
}