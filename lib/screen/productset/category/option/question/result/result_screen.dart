import 'dart:async';

import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/draw_circle.dart';
import 'package:buchi/utils/table.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/result/result_image_item.dart';
import 'package:buchi/widget/table/table_row.dart';
import 'package:buchi/widget/table/data_table.dart';
import 'package:buchi/widget/table/table_row_price.dart';
import 'package:flutter/material.dart';

class ResultScreen extends StatefulWidget {

  int idproduct;  
  int price;
  String product;
  String keterangan;
  List<Feature> featureList;
  List<Productimg> imageList;
  ResultScreen({
      @required this.idproduct,
      @required this.price,
      @required this.product,
      @required this.keterangan,
      @required this.featureList,
      @required this.imageList,
  });

  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  
  String namaproduk,keterangan;
  int price;
  List<String> listName,listCode;
  List<Feature> featureList = List();


  @override
  void initState() {   
   super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies    
    super.didChangeDependencies();
  
  
  }



  @override
  Widget build(BuildContext context) {
    print("pliceploduk ${widget.price} ${widget.product}");
    print("idPloduk ${widget.idproduct}");
    print("featureList ${widget.featureList}");
    print("imageList XX${widget.imageList}");    
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();    
    String kesimpulan  = "Produk yang kami rekomendasikan adalah ";
    var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),
                                          
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 55)),
                  child: IconButton(
                    padding: EdgeInsets.all(5.0),
                    icon: Image.asset('assets/images/refresh.png'),
                    onPressed: () {
                      // Implement navigation to shopping cart page here...
                      print('Click Menu');
                    },
                  ),
                ),                        
              ],
          ),                                                                                                                                                 
      ),   
    );

    var size = MediaQuery.of(context).size;  
    var appBarHeight = appBar.preferredSize.height;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - appBarHeight - 24) / 4;
    final double itemWidth = size.width / 2;

    return Scaffold(
      appBar: appBar,
      body: Stack(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),
          ListView(
          children: <Widget>[
          Padding(
                  padding: EdgeInsets.only(top : Utils().screenHeight(context, 3),
                                           bottom: Utils().screenHeight(context, 1)),
                  child: Container(
                    height: Utils().screenHeight(context, 15),                      
                        child: Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Column(
                              children: <Widget>[     
                                Padding(
                                  padding: EdgeInsets.only(bottom: Utils().screenHeight(context, 0.5),
                                                            top : Utils().screenHeight(context, 1.0),
                                  ),
                                  child: Text(
                                          "Kesimpulan",
                                          style: TextStyle(
                                                  fontSize: 40.0,
                                                  fontWeight: FontWeight.bold,
                                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                          ),
                                    ),
                                ),
                                                              
                                  Expanded(
                                   child: Padding(
                                     padding: EdgeInsets.only(
                                                              bottom : Utils().screenHeight(context, 1),
                                                              left: Utils().screenHeight(context, 2.5),
                                                              right: Utils().screenHeight(context, 2.5),
                                                              ),
                                     child: widget.product != null?  
                                     Text(
                                        kesimpulan+widget.product,
                                        style: TextStyle(
                                                fontSize: 40.0,
                                                fontWeight: FontWeight.bold,
                                                color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                      ) :
                                       Text(
                                        kesimpulan+"",
                                        style: TextStyle(
                                                fontSize: 40.0,
                                                fontWeight: FontWeight.bold,
                                                color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                      )
                                   ),
                                  ),                                                                                                                                                                         
                              ],
                            ),    
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            elevation: 5,
                            margin: EdgeInsets.all(10),
                          ),                                    
                ),
              ),

             Padding(
                  padding: EdgeInsets.only(top : Utils().screenHeight(context, 3),
                                           bottom: Utils().screenHeight(context, 1)),
                  child: Container(
                    height: Utils().screenHeight(context, 130),                      
                        child: Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[     
                                Padding(
                                  padding: EdgeInsets.only(bottom: Utils().screenHeight(context, 0.5),
                                                            top : Utils().screenHeight(context, 1.0),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.only(bottom : Utils().screenHeight(context, 1.5)),
                                    child: Center(
                                      child: Text(
                                              "${widget.product}",
                                              style: TextStyle(
                                                      fontSize: 40.0,
                                                      fontWeight: FontWeight.bold,
                                                      color : Color.fromRGBO(108,117, 125, 1.0)
                                              ),
                                        ),
                                 ),
                                  ),
                                ),
                                                              
                               Padding(
                                     padding: EdgeInsets.only( 
                                                              bottom : Utils().screenHeight(context, 0),
                                                              left: Utils().screenHeight(context, 2.5),
                                                              right: Utils().screenHeight(context, 2.5),
                                                              ),
                                     child: Text(
                                        "${widget.keterangan}",
                                        style: TextStyle(
                                                fontSize: 32.0,
                                                fontWeight: FontWeight.normal,
                                                color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                      ),
                               ),                             

                               Padding(
                                     padding: EdgeInsets.only(top : Utils().screenHeight(context, 2),
                                                              bottom : Utils().screenHeight(context, 1),
                                                              left: Utils().screenHeight(context, 2.5),
                                                              right: Utils().screenHeight(context, 2.5),
                                                              ),
                                     child: Text(
                                        "Features :",
                                        style: TextStyle(
                                                fontSize: 32.0,
                                                fontWeight: FontWeight.bold,
                                                color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                      ),
                              ),

                              projectWidget(),
                            

                              Padding(
                                padding: EdgeInsets.only(top :Utils().screenWidth(context, 3.2),
                                                        bottom : Utils().screenHeight(context, 1),
                                                        left: Utils().screenHeight(context, 2.5),
                                                        right: Utils().screenHeight(context, 2.5),
                                                        ),
                                child: Text(
                                  "Konfigurasinya adalah sebagai berikut:",
                                  style: TextStyle(
                                          fontSize: 32.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                      ),
                              ),
                          

                            Padding(
                              padding: EdgeInsets.only(
                                left : Utils().screenHeight(context, 2.5),
                                right: Utils().screenHeight(context, 2.5),
                              ),
                              child: DataTableWidget(idProduct: widget.idproduct,)
                              // TableRowWidget(cellLeft: "Code", cellRight: "Nama",
                              //  idProduct: widget.idproduct,),
                            ),
                 

                            Padding(
                              padding: EdgeInsets.only(top : Utils().screenHeight(context, 4),
                                                      bottom : Utils().screenHeight(context, 0),
                                                      left: Utils().screenHeight(context, 2.5),
                                                      right: Utils().screenHeight(context, 2.5),
                                                      ),
                              child: Text(
                                "PriceList:",
                                style: TextStyle(
                                        fontSize: 32.0,
                                        fontWeight: FontWeight.bold,
                                        color : Color.fromRGBO(108,117, 125, 1.0)
                                      ),
                                    ),
                            ),

                            
                            Padding(
                              padding:  EdgeInsets.only(
                                left : Utils().screenHeight(context, 2.5),
                                right: Utils().screenHeight(context, 2.5),
                                ),
                              child: TableRowPrice(
                                nameProduct: "${widget.product}", 
                                priceProduct: "${widget.price}", 
                                idProduct: widget.idproduct,),
                            ),             
                            imageWidget(),
                            // Padding(
                            //   padding: EdgeInsets.only(
                            //     left : Utils().screenHeight(context, 1.0),
                            //     right: Utils().screenHeight(context, 1.0),
                            //     top: Utils().screenHeight(context, 1.5),
                            //     ),
                            //   child: imageWidget(),                            
                            // ),
                      ],
                    ),    
                          
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                    margin: EdgeInsets.all(10),
                  ),                                    
                ),
              ),                       
            ],                        
          ),
        ],
      ),
    );
  }

      Widget projectWidget() {
      return
         Container(
              height: Utils().heightDivided(context, 5),
              child: ListView.builder(
              itemCount: widget.featureList.length,
              itemBuilder: (context, index) {
                Feature project = widget.featureList[index];
                print("Feature ${project.id}  ${project.name}" );
                return Column(
                  children: <Widget>[
                    // Widget to display the list of project
                     Row(
                      children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left :Utils().screenHeight(context, 3.2),right: Utils().screenWidth(context, 2.0)),
                        child: CustomPaint(painter: DrawCircle()),
                      ),
                      Text(
                              "${project.name}",
                              style: TextStyle(
                                      fontSize: 32.0,
                                      fontWeight: FontWeight.normal,
                                      color : Color.fromRGBO(108,117, 125, 1.0)
                              ),
                      ),
                      ],
                      ),
                  ],
                );
              },
            ),
         );         
    }

    Widget imageWidget() {     
          return 
          Container(
              height: Utils().heightDivided(context, 5),
              child:
          ListView.builder(
             scrollDirection: Axis.horizontal,
            itemCount: widget.imageList.length,
            itemBuilder: (context, index) {
              Productimg project = widget.imageList[index];
              return 
             Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                    ResultImageItem(path: "${project.img}", title:"Industrial Evaporation",),                                 
                    // ResultImageItem(path: 'assets/images/IndustrialEvaporation.png', title:"Industrial Evaporation",),                                 
                    // ResultImageItem(path: 'assets/images/IndustrialEvaporation.png', title:"Industrial Evaporation",),                                 
                ],
             );
            },
          ), 
          );                   
    }

        

}