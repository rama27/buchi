import 'package:flutter/material.dart';
import 'package:buchi/providers/db_provider.dart';

class TableRowWidget extends StatefulWidget {
  String cellLeft;
  String cellRight; 
  int idProduct;

  TableRowWidget({
                  @required this.cellLeft,
                  @required this.cellRight,                
                  @required this.idProduct,
                  });

  @override
  _TableRowWidgetState createState() => _TableRowWidgetState();
}

class _TableRowWidgetState extends State<TableRowWidget> {
  List<String> listCode;
  List<String> listName;
  @override
  void initState() {
     DBProvider.db.getConfiguration(widget.idProduct).then((val){       
      for(var v in val){
        // print("getKonpli ${v.name}");      
        listCode = new List();
        listName = new List();
        setState(() {
          listCode.add(v.code);
          listName.add(v.name);      
        });                     
        for(var code in listCode){
          print("codex $code");
        }
      }  
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {    
   
    return  Table(
          border: TableBorder.all(color: Colors.black),
          children: [
            TableRow(children: [
              Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(widget.cellLeft,style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        color : Color.fromRGBO(108,117, 125, 1.0)
                        ),
                ),
              ), 
           
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(widget.cellRight,style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                    ),
              ),                               
            ]),                                 
          
            TableRow(children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Cell 3',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Cell 4',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ), 
                                                      
              ]
              ),

            //   TableRow(children: [
            //     Padding(
            //       padding: const EdgeInsets.all(16.0),
            //       child: Text('Cell 3',style: TextStyle(
            //                 fontSize: 30.0,
            //                 fontWeight: FontWeight.normal,
            //                 color : Color.fromRGBO(108,117, 125, 1.0)
            //                 ),),
            //     ),
            //     Padding(
            //       padding: const EdgeInsets.all(16.0),
            //       child: Text('Cell 4',style: TextStyle(
            //                 fontSize: 30.0,
            //                 fontWeight: FontWeight.normal,
            //                 color : Color.fromRGBO(108,117, 125, 1.0)
            //                 ),),
            //     ), 
                                                      
            //   ]
            //   ),

            //   TableRow(children: [
            //     Padding(
            //       padding: const EdgeInsets.all(16.0),
            //       child: Text('Cell 5',style: TextStyle(
            //                 fontSize: 30.0,
            //                 fontWeight: FontWeight.normal,
            //                 color : Color.fromRGBO(108,117, 125, 1.0)
            //                 ),),
            //     ),
            //     Padding(
            //       padding: const EdgeInsets.all(16.0),
            //       child: Text('Cell 6',style: TextStyle(
            //                 fontSize: 30.0,
            //                 fontWeight: FontWeight.normal,
            //                 color : Color.fromRGBO(108,117, 125, 1.0)
            //                 ),),
            //     ), 
                                                      
            //   ]
            //   ),

            //   TableRow(children: [
            //         Padding(
            //           padding: const EdgeInsets.all(16.0),
            //           child: Text('Cell 7',style: TextStyle(
            //                     fontSize: 30.0,
            //                     fontWeight: FontWeight.normal,
            //                     color : Color.fromRGBO(108,117, 125, 1.0)
            //                     ),),
            //         ),
            //         Padding(
            //           padding: const EdgeInsets.all(16.0),
            //           child: Text('Cell 8',style: TextStyle(
            //                     fontSize: 30.0,
            //                     fontWeight: FontWeight.normal,
            //                     color : Color.fromRGBO(108,117, 125, 1.0)
            //                     ),),
            //         ), 
                                                          
            // ]
            // ),

            // TableRow(children: [
            //   Padding(
            //     padding: const EdgeInsets.all(16.0),
            //     child: Text('Cell 5',style: TextStyle(
            //               fontSize: 30.0,
            //               fontWeight: FontWeight.normal,
            //               color : Color.fromRGBO(108,117, 125, 1.0)
            //               ),),
            //   ),
            //   Padding(
            //     padding: const EdgeInsets.all(16.0),
            //     child: Text('Cell 6',style: TextStyle(
            //               fontSize: 30.0,
            //               fontWeight: FontWeight.normal,
            //               color : Color.fromRGBO(108,117, 125, 1.0)
            //               ),),
            //   ),                                                             
            // ]
            // ),
           ],
        );

  }
}