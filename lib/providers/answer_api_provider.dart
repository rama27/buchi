import 'dart:convert';
import 'package:buchi/models/answer_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';

class AnswerApiProvider {
  Future<List<Answer>> getAllAnswer() async {
    var url = Consts.base_url+"answer";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    responseData = json.decode(response.data);   //response JSON data didecode agar bisa di parse     
    // print("APIPROVIDER ${responseData["category"]}");
    List responseJson =responseData["answer"];  //ambil data json  category masukan ke variabel tipe data list  

    return (responseJson).map((answer) {     
      // print('Inserting $answer');
       DBProvider.db.createAnswer(Answer.fromJson(answer));
    }).toList();

  }
}