import 'dart:convert';

import 'package:buchi/models/category_response.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/providers/dbprovider_category.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';

class CategoryApiProvider {
  Future<List<CategoryResp>> getAllCategory() async {
    var url = Consts.base_url+"category";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    responseData = json.decode(response.data);   //response JSON data didecode agar bisa di parse     
    // print("APIPROVIDER ${responseData["category"]}");
    List responseJson =responseData["category"];  //ambil data json  category masukan ke variabel tipe data list  

    return (responseJson).map((category) {     
      // print('Inserting $category');
      DBProvider.db.createCategory(CategoryResp.fromJson(category));
    }).toList();

  }
}