import 'dart:convert';

import 'package:buchi/models/delete_response.dart';
import 'package:buchi/models/dokumen_model.dart';
import 'package:buchi/models/folder_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/dialog/content_dialog_upload_file.dart';
import 'package:buchi/widget/dialog/content_new_folder.dart';
import 'package:string_validator/string_validator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SharedScreen extends StatefulWidget {

  final int idCategory;
 
  SharedScreen({this.idCategory});

  @override
  _SharedScreenState createState() => _SharedScreenState();
}

class _SharedScreenState extends State<SharedScreen> {
  List<Dokumen> fileList;
  List<Folder> folderList;
  List<dynamic> sharedList,extensions;
  int userId,folderId,parent,toDeleteId;
  int parentBegin,folderIdBegin=0;
  
  @override
  void initState() {
   print("initSTATE");
   print("folderIdBegin $folderIdBegin");
   fileList = new List();
   folderList = new List();
   sharedList = new List();
   extensions = new List();

   DBProvider.db.getAllBeginningDocument(folderIdBegin, widget.idCategory).then((val){
     print("laboratory $val");
      for(var v in val ){
        if(v.status != 0){
          // print("name ${v.name}");  
          if(v.folderid == 0){setState(() {
             fileList.add(v);
          });  
            setState(() {
             fileList.add(v);
           });  
          }                 
        }     
      }
      DBProvider.db.getFolderContent(folderIdBegin,widget.idCategory).then((val){
        for(var v in val ){
          if(v.status != 0){
            if(v.parent == 0){
              // print("name folder ${v.name}");  
              setState(() {
                 folderList.add(v);
              });             
            }     
          }   
        }    
       setState(() {
         sharedList.addAll(folderList);
         sharedList.addAll(fileList);   
       });        
      });
   });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    print("didChangeDependencies");
     super.didChangeDependencies();
  }
  
  void getDataFromDb(int folderId){
       DBProvider.db.getAllBeginningDocument(folderId, widget.idCategory).then((val){
     print("laboratory $val");
      for(var v in val ){
        if(v.status != 0){
          // print("name ${v.name}");  
          if(v.folderid == 0){setState(() {
             fileList.add(v);
          });  
            setState(() {
             fileList.add(v);
           });  
          }                 
        }     
      }
      DBProvider.db.getFolderContent(folderId,widget.idCategory).then((val){
        for(var v in val ){
          if(v.status != 0){
            if(v.parent == 0){
              // print("name folder ${v.name}");  
              setState(() {
                 folderList.add(v);
              });             
            }     
          }   
        }    
       setState(() {
         sharedList.addAll(folderList);
         sharedList.addAll(fileList);   
       });        
      });


   });
  }


  Future<DeleteResponse> deleteFolder(int toDeleteId,int userId,int folderId, int parent) async {
    Map<String,dynamic> responseData;    
    DeleteResponse responseJson; 
    var url = 'http://buchi-indonesia.co.id/api/v1/deletefolder?delete=$toDeleteId&id=$userId&folderid=$folderId&parent=$parent';    
    final response = await http.get(url);
    responseData = json.decode(response.body); 
    responseJson = responseData["message"];  //mengambil value dari response 
    return responseJson;
  }

  Future<DeleteResponse> deleteFile(int toDeleteId,int userId,int folderId, int parent) async {
    Map<String,dynamic> responseData;    
    DeleteResponse responseJson; 
    var url = 'http://buchi-indonesia.co.id/api/v1/deletefile?delete=$toDeleteId&id=$userId&folderid=$folderId&parent=$parent';    
    final response = await http.get(url);
    responseData = json.decode(response.body); 
    responseJson = responseData["message"];  //mengambil value dari response 
    return responseJson;
  }

   Widget _setIcon(String namafile){   
      if(!namafile.contains(".")){
          return  
            Image.asset(
                  'assets/images/folder.png', 
                  fit: BoxFit.none,
              );   
      }else{
          var extension = namafile.split(".").last;
          extensions.add(extension);
          if(extension.contains('pdf')){        
            return  
            Image.asset(
                  'assets/images/pdf.png', 
                  fit: BoxFit.none,
              );    
            // Container(
            //    height: Utils().screenHeight(context,  1.5),
            //    child : Image.asset('assets/images/pdf.png',fit: BoxFit.cover,), 
            // );
          }else if(extension.contains('xlsx') || extension.contains('xls')
                  || extension.contains('docx') || extension.contains('doc') 
                  || extension.contains('txt') || extension.contains('csv')){
            return Image.asset(
                  'assets/images/file.png', 
                  fit: BoxFit.none,
              );    
            // return  Container(
            //    height: Utils().screenHeight(context,  1.5),
            //    child : Image.asset('assets/images/file.png',fit: BoxFit.cover,), 
            // );
          }else if(extension.contains('mp4') || extension.contains('mkv') || 
          extension.contains('flv') || extension.contains('avi')) {
            return Image.asset(
                  'assets/images/video.png', 
                  fit: BoxFit.none,
            );  
            // return  Container(
            //    height:Utils().screenHeight(context,  1.5),
            //    child : Image.asset('assets/images/video.png',fit: BoxFit.cover,), 
            // ); 
          }else{
            return Image.asset(
                  'assets/images/file.png', 
                  fit: BoxFit.none,
            );  
            // return  Container(
            //    height:Utils().screenHeight(context, 1.5),
            //    child : Image.asset('assets/images/file.png',fit: BoxFit.cover,), 
            // ); 
          } 
      }              
  }
  @override
  Widget build(BuildContext context) {
  print("idcat ${widget.idCategory}");  
  for(var i in fileList){
    print("file ${i.name}");
  }
  for(var i in folderList){
    print("folder ${i}");
  }
  for(var i in sharedList){
    print("shared ${i.name}");
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();  
 
  var myFabButton = Container(
      width: 400.0,
      height: 400.0,
      child: new RawMaterialButton(
        shape: new CircleBorder(),
        elevation: 0.0,
        child: Icon(
          Icons.favorite,
          color: Colors.blue,
         ),
      onPressed: (){},
  )
  );

  var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
               Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),      
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ),                               
              ],
          ),                                                                                                                                                 
      ),   
    );
 
    var size = MediaQuery.of(context).size;  
    var appBarHeight = appBar.preferredSize.height;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - appBarHeight - 24) / 2.9;
    final double itemWidth = size.width / 2;
    
    void showDialog(){
        showGeneralDialog(
        barrierLabel: "Label",
        barrierDismissible: true,           
        transitionDuration: Duration(milliseconds: 700),
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return ContentDialogUploadFile();
        },
              transitionBuilder: (context, anim1, anim2, child) {
                return SlideTransition(
                  position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                  child: child,
                );
              },
      );    
    }

  void showDialog2(){
        showGeneralDialog(
        barrierLabel: "Label",
        barrierDismissible: true,           
        transitionDuration: Duration(milliseconds: 700),
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return ContentNewFolder();
        },
              transitionBuilder: (context, anim1, anim2, child) {
                return SlideTransition(
                  position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                  child: child,
                );
              },
      );    
    }
    return Scaffold(
      appBar: appBar,
      body: Stack(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),
          Container(
          height: Utils().screenHeight(context, 110),
          child: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: (itemWidth / itemHeight),
          children: 
          sharedList.asMap().map((i,element){
            return MapEntry(i, 
             GestureDetector(
              child:  Container(
                  height: Utils().screenHeight(context, 200),
                  width: Utils().screenWidth(context, 100),                      
                      child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[ 
                         
                            Padding(
                                      padding: EdgeInsets.only(left : Utils().screenWidth(context, 0)),
                                      child: RaisedButton(
                                        onPressed: () {
                                          // showDialog();

                                          print("index $i");
                                           setState(() {
                                            if(sharedList[i].name.contain(".")){
                                                deleteFile(sharedList[i].id, sharedList[i].userid, sharedList[i].folderid, sharedList[i].folderid).then((val){
                                                  print("deleteFileStatus $val");
                                                });
                                            }else{
                                                deleteFolder(sharedList[i].id, sharedList[i].userid, sharedList[i].parent , sharedList[i].parent).then((val){
                                                  print("deleteFolderStatus $val");
                                                });
                                            }
                                          
                                             sharedList.removeAt(i);   
                                                 
                                              //  DBProvider.db.deleteDocument(sharedList[i].id).then((val){
                                              //       print("delete $val");
                                              //  });

                                           
                                          });                                                                              
                                        },
                                        textColor: Colors.white,
                                        padding: const EdgeInsets.all(0.0),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Colors.red,                                 
                                          ),
                                          padding: EdgeInsets.fromLTRB(Utils().screenWidth(context, 2),
                                                                      Utils().screenHeight(context, 1.5),
                                                                      Utils().screenWidth(context, 2),
                                                                      Utils().screenHeight(context, 1.5),
                                                                      ),
                                          child:                                           
                                          GestureDetector(
                                            onTap: (){                                            
                                            },
                                             child: Image.asset('assets/images/close_white.png',
                                             height: Utils().screenHeight(context, 1.5),),
                                          )
                                        ),                            
                                      ),
                            ),

                           Expanded(
                             child: Center(                            
                              child: 
                                _setIcon(sharedList[i].name),
                                // Image.asset(
                                //     path, 
                                //     fit: BoxFit.none,
                                // ),                              
                                ),
                           ),
                                                 
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Expanded(
                               child: Center(
                                  child: Text(
                                    sharedList[i].name,
                                    style: TextStyle(
                                            fontSize: 35.0,
                                            fontWeight: FontWeight.bold,
                                            color : Color.fromRGBO(108,117, 125, 1.0)
                                    ),
                                  ),
                                ),
                              ),
                          ),          
                            ],
                          ),    
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),                                    
              ),
              onTap: () {                        
                var isContain = contains(sharedList[i].name, ".");
                if(!isContain){
                  print("folder clicked");
                  setState(() {
                    folderIdBegin = sharedList[i].id;
                    sharedList.clear();
                    getDataFromDb(folderIdBegin);
                  });                  
                }
              },
              )
            );
             
          }).values.toList()

        
          // <Widget>[
          //   GestureDetector(
          //     child: SharedItem(path: 'assets/images/folder.png', title:"Unesa Surabaya",),
          //     onTap: () {               
          //     },
          //   ),
          //   GestureDetector(
          //     child: SharedItem(path: 'assets/images/pdf.png', title:"AN_Distilled_solvent_R100_final.pdf",),
          //     onTap: () {               
          //     },
          //   ),
          //   GestureDetector(
          //     child: SharedItem(path: 'assets/images/file.png', title:"Feedback Form Trial_rev2.docx Request - Form Trial_rev2.docx",),
          //     onTap: () {               
          //     },
          //   ),
          //   GestureDetector(
          //     child: SharedItem(path: 'assets/images/video.png', title:"BUCHI Products - General Overview.mp4",),
          //     onTap: () {               
          //     },
          //   ),
          // ],
        ),
        ),
        
        Positioned(
         bottom: 170.0,
         right: 30.0, 
         child: Align(
            alignment: Alignment.bottomRight,
            child: Container(
            height: 100.0,
            width: 100.0,
            child: FittedBox(
                child: FloatingActionButton(onPressed: () {showDialog();},
                                            backgroundColor: Colors.deepOrangeAccent,
                                            child: Icon(Icons.note_add),
                                            heroTag: "btn1",),
            ),
           ),
          ),
        ), 

        Positioned(
          bottom: 50.0,
          right: 30.0, 
          child: Align(
            alignment: Alignment.bottomRight,
            child: Container(
            height: 100.0,
            width: 100.0,
            child: FittedBox(
                child: FloatingActionButton(onPressed: () {showDialog2();},
                                            backgroundColor: Colors.orange,
                                            child: Icon(Icons.create_new_folder),
                                            heroTag: "btn2",),
            ),
           ),
          ),
        ),       
      
        ],        
      ),     
    );
  }
}