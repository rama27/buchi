import 'package:buchi/screen/profile/profile_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';

class SideDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return new SizedBox(
        width: MediaQuery.of(context).size.width * 0.65,//20.0, 
        child: Drawer(
          child: new ListView(
          children: <Widget>[
            SizedBox(
                height: Utils().height(context) * 0.20,
                child: new DrawerHeader(
                child: new Text("DRAWER HEADER..",
                                  style: TextStyle(
                                  fontSize: 45.0
                                  ),
                                ),
                decoration: new BoxDecoration(color: Color.fromRGBO(148, 205, 12, 100)),
                padding: EdgeInsets.only(top: Utils().height(context)*0.08,
                                         left: Utils().height(context)*0.02),
              ),
            ),
         
            new ListTile(
              title: Padding(
                padding: const EdgeInsets.fromLTRB(16.0,46.0,16.0,16.0),
                child: new Text("Product Set",
                                 style: TextStyle(
                                  fontSize: 45.0,
                                  fontWeight: FontWeight.bold,
                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                ),
                            ),
              ),
              onTap: () {
                // Navigator.pop(context);
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => new HomePage()));
              },
            ),

            new ListTile(
              title: Padding(
                padding: const EdgeInsets.all(16.0),
                child: new Text("Price List",
                                 style: TextStyle(
                                  fontSize: 45.0,
                                  fontWeight: FontWeight.bold,
                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                ),
                            ),
              ),
              onTap: () {
                // Navigator.pop(context);
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => new HomePage()));
              },
            ),
            new ListTile(
              title: Padding(
                padding: const EdgeInsets.all(16.0),
                child: new Text("Mechanic Files",
                                 style: TextStyle(
                                  fontSize: 45.0,
                                  fontWeight: FontWeight.bold,
                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                ),
                            ),
              ),
              onTap: () {
                // Navigator.pop(context);
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => new HomePage()));
              },
            ),
            new ListTile(
              title:  Padding(
                padding: const EdgeInsets.all(16.0),
                child: new Text("Kuesioner",
                                 style: TextStyle(
                                  fontSize: 45.0,
                                  fontWeight: FontWeight.bold,
                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                ),
                            ),
              ),
              onTap: () {
                // Navigator.pop(context);
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => new HomePage()));
              },
            ),
            new ListTile(
              title: Padding(
                padding: const EdgeInsets.all(16.0),
                child: new Text("Profile",
                                 style: TextStyle(
                                  fontSize: 45.0,
                                  fontWeight: FontWeight.bold,
                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                ),
                            ),
              ),
              onTap: () {              
                Navigator.push(context,
                    new MaterialPageRoute(builder: (context) => new ProfileScreen()));
              },
            ),
            new ListTile(
              title: Padding(
                padding: EdgeInsets.only(top : Utils().height(context) * 0.39, left : Utils().height(context) * 0.01),
                child: new Text("Logout",
                                 style: TextStyle(
                                  fontSize: 45.0,
                                  fontWeight: FontWeight.bold,
                                  color : Color.fromRGBO(108,117, 125, 1.0)
                                ),
                            ),
              ),
              onTap: () {
                // Navigator.pop(context);
                // Navigator.push(context,
                //     new MaterialPageRoute(builder: (context) => new HomePage()));
              },
            ),
          ],
        )),
      );
    }
}