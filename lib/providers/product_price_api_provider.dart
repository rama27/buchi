import 'dart:convert';

import 'package:buchi/models/product_goods_model.dart';
import 'package:buchi/models/product_price_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';

class ProductPriceApiProvider {
  
  Future <List<Productprice>> getAllProductPrice() async {
    var url = Consts.base_url+"productprice";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    // print("RESP QUESTIONS ${response.data}");
    responseData = json.decode(response.data);//response JSON data didecode agar bisa di parse    
    // print("nodecode ${response.data["question"]}");   
    List responseJson =responseData["productprice"]; //ambil data json question masukan ke variabel tipe data list              

    return (responseJson).map((productprice) {     
      // print('Inserting $question');      
      DBProvider.db.createProductPrice(Productprice.fromJson(productprice));
    }).toList();

  }
}