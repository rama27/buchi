class Document {
    int iduser;
    String parent;
    dynamic parentfolder;
    String folderid;
    List<Folderlist> folderlist;
    List<Filelist> filelist;

    Document({
        this.iduser,
        this.parent,
        this.parentfolder,
        this.folderid,
        this.folderlist,
        this.filelist,
    });

    factory Document.fromJson(Map<String, dynamic> json) => Document(
        iduser: json["iduser"],
        parent: json["parent"],
        parentfolder: json["parentfolder"],
        folderid: json["folderid"],
        folderlist: List<Folderlist>.from(json["folderlist"].map((x) => Folderlist.fromJson(x))),
        filelist: List<Filelist>.from(json["filelist"].map((x) => Filelist.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "iduser": iduser,
        "parent": parent,
        "parentfolder": parentfolder,
        "folderid": folderid,
        "folderlist": List<dynamic>.from(folderlist.map((x) => x.toJson())),
        "filelist": List<dynamic>.from(filelist.map((x) => x.toJson())),
    };
}

class Filelist {
    int id;
    int idcategory;
    String name;
    String type;
    int status;
    int folderid;
    int userid;
    DateTime createdAt;
    String updatedAt;

    Filelist({
        this.id,
        this.idcategory,
        this.name,
        this.type,
        this.status,
        this.folderid,
        this.userid,
        this.createdAt,
        this.updatedAt,
    });

    factory Filelist.fromJson(Map<String, dynamic> json) => Filelist(
        id: json["id"],
        idcategory: json["idcategory"],
        name: json["name"],
        type: json["type"],
        status: json["status"],
        folderid: json["folderid"],
        userid: json["userid"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idcategory": idcategory,
        "name": name,
        "type": type,
        "status": status,
        "folderid": folderid,
        "userid": userid,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}

class Folderlist {
    int id;
    int categoryid;
    String name;
    int parent;
    int status;
    int userid;
    DateTime createdAt;

    Folderlist({
        this.id,
        this.categoryid,
        this.name,
        this.parent,
        this.status,
        this.userid,
        this.createdAt,
    });

    factory Folderlist.fromJson(Map<String, dynamic> json) => Folderlist(
        id: json["id"],
        categoryid: json["categoryid"],
        name: json["name"],
        parent: json["parent"],
        status: json["status"],
        userid: json["userid"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "categoryid": categoryid,
        "name": name,
        "parent": parent,
        "status": status,
        "userid": userid,
        "created_at": createdAt.toIso8601String(),
    };
}
