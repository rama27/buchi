import 'dart:io';

import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/form/form_edit.dart';
import 'package:flutter/material.dart';


class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
      var appBar = PreferredSize(
      preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
      child : new AppBar(                       
            backgroundColor: Colors.white,                                 
            flexibleSpace: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,            
              children: <Widget>[
                 Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                    child: Container(child: GestureDetector(
                                            child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                            onTap: () => _scaffoldKey.currentState.openDrawer(),
                                            ),
                                            
                                    ),
                  ), 
                  Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                    child: Image.asset(
                      'assets/images/buchi_logo2.png',                                                    
                      height: Utils().heightDivided(context, 25),
                    ),
                  ), 
                  Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 55)),
                    child: IconButton(
                      padding: EdgeInsets.all(5.0),
                      icon: Image.asset('assets/images/refresh.png'),
                      onPressed: () {
                        // Implement navigation to shopping cart page here...
                        print('Click Menu');
                      },
                    ),
                  ),                        
                ],
             ),                                                                                                                                                 
      ),   
    );
    return Scaffold(
      appBar: appBar,
      body:   Stack(
              children: <Widget>[
                Container(
                  color: Color.fromRGBO(148, 205, 12, 100),
                ),
                Container(
                height: Utils().screenHeight(context, 100),
                child: Card(
                  // semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child:
                ListView(
                  //  crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding:  EdgeInsets.only(top : Utils().screenHeight(context, 3.0), 
                                                 left: Utils().screenWidth(context, 4.0),),
                        child: Text('admin', 
                              style: TextStyle(fontWeight: FontWeight.bold,
                                              fontSize: 50.0,)
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top : Utils().screenHeight(context, 3.5), 
                                                 left: Utils().screenWidth(context, 2.0),

                                                 ),
                        child: Text('Sales Buchi Indonesia', 
                              style: TextStyle(fontWeight: FontWeight.normal,
                                              fontSize: 43.0,)
                        ),
                      ),                          
                      ],
                    ),

                    // Form Widget
                      Padding(
                        padding: EdgeInsets.only(top : 50.0),
                        child: FormEdit(),
                      ),

                       //Button                                         
                      Padding(
                          padding: EdgeInsets.only(top: Utils().screenHeight(context, 1.0),
                                                  left : Utils().screenWidth(context, 3.5),
                                                  bottom: Utils().screenHeight(context, 3.0)),   
                          child : new SizedBox(      
                          width : Utils().screenWidth(context, 23),   
                          height: Utils().screenHeight(context, 5),
                          child : RaisedButton(
                            shape : new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.white)), 
                            child : Padding( 
                                child: Text('Edit Profile',style: TextStyle(
                                                            fontWeight: FontWeight.normal,
                                                            fontSize: 35.0,
                                                            color: Colors.white
                                                          ),
                                                          ),
                              padding: EdgeInsets.all(0.0),
                            ),
                            color: Colors.blue,
                            textColor:  Colors.white, 
                            onPressed: ()  {                                                                
                                                               
                            }
                            ),
                            )
                          ),               
                  ],
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(14),
                ),
        ),
              ],           
      ),
    );
  
  }
}