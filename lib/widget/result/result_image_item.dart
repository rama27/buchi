import 'package:flutter/material.dart';

class ResultImageItem extends StatelessWidget {

  final String urlimage="http://buchi-indonesia.co.id/img/img/product/"; 
  String path;
  String title;

  ResultImageItem({@required this.path, @required this.title});
  
  @override
  Widget build(BuildContext context) {
    print("path $path");
    return  Container(
                  width: 350,                
                  height: 400,                      
                      child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Column(
                            children: <Widget>[
                                Expanded(
                                  child: Image.network(
                                  urlimage+path, fit: BoxFit.fill,),
                                ),                             
                            ],
                          ),    
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),                                    
                );
  }
}