import 'package:buchi/models/pricelist_response.dart';
import 'package:buchi/models/product_price_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/providers/dbprovider2.dart';
import 'package:buchi/providers/pricelist.dart';
import 'package:buchi/providers/pricelist_api_provider.dart';
import 'package:buchi/providers/pricelist_example_prov.dart';
import 'package:buchi/providers/product_price_api_provider.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/dialog/pages_dialog.dart';
import 'package:buchi/widget/table/table_price.dart';
import 'package:buchi/widget/table/table_price_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PriceListScreen extends StatefulWidget {
  @override
  _PriceListScreenState createState() => _PriceListScreenState();
}

class _PriceListScreenState extends State<PriceListScreen> {
  String title = "Pricelist Product";
  List _lengths =
    ["10", "25", "50","100"];
  List<String> _page =
  ["1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"];  
  bool _isLastClicked = false;  
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentLength;
  SizedBox search;
  Container searchWidget;
  List<bool> _isClicked = [false,false,false,false,false];
  var searchController = TextEditingController();
  List<TableRow> priceTableRows = [];
  List<TableRow> titleTableRows = [];
  List<PriceList> priceLists = [];  
  int total;
  int _lastPage =0;
  String searchQuery;
  var _isSearching = false;
  var isLoading = false;
  var jumlahpagez;
  var selectedPage=1;
  var indexSelectedPage=0;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentLength = _dropDownMenuItems[0].value;
    searchQuery = "";
            titleTableRows.add(                    
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("Code", style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("Name", style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("Price", style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                      ])
      );    
    super.initState();
  }

   Future<Null> getDialog(int jumlahpage) async{
    String returnVal =  await  showGeneralDialog(
      barrierLabel: "Label",
      barrierDismissible: true,           
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return PagesDialog(jumlahpages: jumlahpagez,);
      },
            transitionBuilder: (context, anim1, anim2, child) {
              return SlideTransition(
                position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                child: child,
              );
            },
    ); 

    if (returnVal == 'success') {      
      DataConfig().getDataInt("selectedpage").then((val){
        setState(() {
          selectedPage = val;
          indexSelectedPage = val -1; //untuk menampilkan angka di button 
           DBProvider.db.getAllProductPrice("",int.parse(_currentLength),val).then((val){
            print("pricelistlengths $val");  
              setState(() {        
                  List<Productprice> pricelists = val;
                  updateTableRows(pricelists); 
              });        
         }); 
        });        
      });
      
    }
    // return returnVal;   
  }

  // here we are creating the list needed for the DropDownButton
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _lengths) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: Container(           
            child: new Text(city, style: TextStyle(fontSize: 35.0),))
      ));
    }
    return items;
  }

  void changedDropDownItem(String selectedLength) async {
    print("Selected per page $selectedLength, we are going to refresh the UI");
    setState(() {
      _currentLength = selectedLength;
      print("currentLength $_currentLength");
    });
      DBProvider.db.getAllProductPrice("",int.parse(_currentLength),selectedPage).then((val){
            print("pricelistlengths $val");  
              setState(() {        
                  List<Productprice> pricelists = val;
                  updateTableRows(pricelists); 
              });        
       }); 
    // var apiProvider = PricelistApiProvider();
    //  await apiProvider.getAllPriceList(_currentLength).then((val){
    //    print("lenght ${val.length}");
    //    if(val.length == int.parse(_currentLength)){            
    //         DBProvider.db.getAllPricelist("",int.parse(_currentLength)).then((val){
    //         print("pricelistlength $val");  
    //           setState(() {        
    //               List<Productprice> pricelists = val;
    //               updateTableRows(pricelists); 
    //           });        
    //         }); 
    //    }                
    // });
  }

  @override
  void didChangeDependencies() {
     search = SizedBox(
      width: Utils().width(context),
      height:  Utils().screenHeight(context, 10),     
      child: TextFormField(  
      onFieldSubmitted: (value) {       
        setState(() {
          if(searchQuery == null){
            searchQuery = "";
          }else{
             print("submited $value");
            _isSearching = true;
            searchQuery = value;
            DBProvider.db.getAllProductPrice(searchQuery,int.parse(_currentLength),selectedPage).then((val){
            // print("getAllPricelist $val");

              setState(() {        
                  List<Productprice> pricelists = val;
                  updateTableRows(pricelists); 
              });        
            });
          }         
        });      
      } ,  
      keyboardType: TextInputType.text,
      controller: searchController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter text search';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        // labelText: 'Search',
        hintText: 'Search',
        contentPadding: new EdgeInsets.symmetric(
          vertical: Utils().screenHeight(context, 2), 
          horizontal: Utils().screenWidth(context, 10)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      ),
    );   

    DataConfig().getDataInt("total").then((val){
      print("totalsss $val");
      setState(() {
          total = val; 
      });     
    });

    DataConfig().getDataInt("pages").then((val){
      print("pagesss $val");
      setState(() {       
         _lastPage = val;
      });
    });

    DBProvider.db.getAllProductPrice(searchQuery,int.parse(_currentLength),selectedPage).then((val){
       print("didChangeDep $val");

       setState(() {        
           List<Productprice> pricelists = val;
           updateTableRows(pricelists); 
       });        
    });

    // DBProvider.db.getAllProductPrice(searchQuery,25).then((val){
    //    print("getAllPricelist $val");

    //    setState(() {        
    //        List<Productprice> pricelists = val;
    //        updateTableRows(pricelists); 
    //    });        
    // });
    
   searchWidget = Container(
              width: Utils().width(context),
              height:  Utils().screenHeight(context, 10),
              padding: EdgeInsets.only(left: 30.0, right: 30.0),
               child:Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Flexible(
                                  child:
                                 TextFormField(  
                                  onFieldSubmitted: (value) {       
                                    setState(() {
                                      if(searchQuery == null){
                                        searchQuery = "";
                                      }else{
                                          print("submited $value");
                                        _isSearching = true;
                                        searchQuery = value;
                                        DBProvider.db.getAllProductPrice(searchQuery,int.parse(_currentLength),selectedPage).then((val){
                                        print("getAllPricelist $val");

                                          setState(() {        
                                              List<Productprice> pricelists = val;
                                              updateTableRows(pricelists); 
                                          });        
                                        });
                                      }         
                                    });      
                                  } ,  
                                  keyboardType: TextInputType.text,
                                  controller: searchController,
                                  autofocus: false,
                                  validator: (value) {
                                  if (value.isEmpty) {
                                      return 'Please enter text search';
                                    }
                                    return null;
                                  },  
                                  style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
                                  decoration: InputDecoration(
                                    // labelText: 'Search',
                                    hintText: 'Search',
                                    contentPadding: new EdgeInsets.symmetric(
                                      vertical: Utils().screenHeight(context, 2), 
                                      horizontal: Utils().screenWidth(context, 10)),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                    ),    
                                  ),    
                              ),   
                              (_isSearching) ?                              
                              GestureDetector(
                               child: Container(                      
                                      padding: new EdgeInsets.all(20.0),                      
                                      child :Image.asset('assets/images/close.png', height: Utils().screenHeight(context, 2),),                                       
                                ),
                                onTap : () {                                                                  
                                  print("Search End");          
                                  setState(() {
                                     _handleSearchEnd();     
                                  });                                                                                                                                                                                                                                                                       
                                },
                              )                                
                            :
                              Container(),                            
                            ],
                          ),                   
              );
  super.didChangeDependencies();
  }

   void _handleSearchEnd() {
    setState(() {    
      _isSearching = false;
       searchController.clear();
       searchQuery = "";
    });
     DBProvider.db.getAllProductPrice("",int.parse(_currentLength),0).then((val){
       print("close $val");

       setState(() {        
           List<Productprice> pricelists = val;
           updateTableRows(pricelists); 
       });        
      });
  }


  void updateTableRows(List<Productprice> pricelists) {
    // print("priceList $pricelists");
    priceTableRows = [];      
    for(Productprice pricelist in pricelists){
        print(pricelist.name);         
        priceTableRows.add(                    
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(pricelist.code, style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(26.0),
                          child: Text(pricelist.name, style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(pricelist.price.toString(), style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                      ])
         );      
      }      
  }

   _loadFromApi() async {
    setState(() {
      isLoading = true;
    });


    var apiProvider = ProductPriceApiProvider();
    await apiProvider.getAllProductPrice().then((val){
      // print(" ioo $val");
      // DBProvider.db.getAllEmployees().then((val){
      //    print("getPriceSQL $val");
      // });
       DBProvider.db.getAllProductPrice(searchQuery,int.parse(_currentLength),selectedPage).then((val){
       print("getAllProductPrice $val");

       setState(() {        
           List<Productprice> pricelists = val;
           updateTableRows(pricelists); 
       });        
      });


    });

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    print("Search $searchQuery");
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();   
    var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),
                                          
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 45)),
                  child: IconButton(
                    padding: EdgeInsets.all(5.0),
                    icon: Image.asset('assets/images/refresh.png'),
                    onPressed: () async {
                      // Implement navigation to shopping cart page here...
                      print('Click Menu');                   
                      await _loadFromApi();              
                      // DBProvider.db.createEmployee(PriceList.fromJson(responseData));        
                    },
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 0)),
                    child: IconButton(
                      padding: EdgeInsets.all(5.0),
                      icon: Icon(Icons.view_agenda),
                      onPressed: () async {
                        // Implement navigation to shopping cart page here...
                        print('Click Menu');                                                
                        DBProvider2.db.getAllEmployees().then((val){
                          print("SELECT DATA $val");
                        });        
                      },
                    ),
                  ),
                ),                                
              ],
          ),                                                                                                                                                 
      ),   
    );

   
    return 
    Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: appBar,
      body: Stack(
    children: <Widget>[
      Container(
            color: Color.fromRGBO(148, 205, 12, 100),
      ),
      Padding(
      padding: EdgeInsets.only( top : Utils().screenHeight(context, 3),
                                bottom: Utils().screenHeight(context, 1)),
      child: Column(
        children: <Widget>[
        Container(
        height: Utils().screenHeight(context, 10),
        width: Utils().width(context),                      
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child:                              
                      
                     Padding(
                          padding: EdgeInsets.only(top : Utils().screenHeight(context, 2),
                                                  bottom : Utils().screenHeight(context, 2),
                                                  left: Utils().screenHeight(context, 4),
                                                  right: Utils().screenHeight(context, 4),
                                                  ),
                          child: Text(
                            title,
                            style: TextStyle(
                                    fontSize: 60.0,
                                    fontWeight: FontWeight.bold,
                                    color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                                                                                                                
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(10),
              ),                                    
            ),

        Container(
        height: Utils().screenHeight(context, 70),
        width: Utils().width(context),                      
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child:
                    SingleChildScrollView(
                                          child: Padding(
                        padding: const EdgeInsets.all(40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,                      
                          children: <Widget>[
                            Row(children: <Widget>[
                               Text("Show", style: TextStyle(fontSize: 32.0),),
                               Padding(
                                padding: const EdgeInsets.only(left : 20.0, top: 20.0, right: 20.0),
                                child: Container(
                                  width: 90, 
                                  height: 70,                             
                                  child: new DropdownButton(
                                    isExpanded: true,
                                    value: _currentLength,
                                    items: _dropDownMenuItems,
                                    onChanged: changedDropDownItem,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right : 250.0),
                                child: Text("Entries", style: TextStyle(fontSize: 32.0),),
                              ),                               
                            ],
                            ),
                             Padding(
                               padding: EdgeInsets.only(top : Utils().screenHeight(context, 0.2)),
                               child: searchWidget,
                             ),                          
                            // TablePriceList(cellLeft: 'Code', cellMiddle: 'Name', cellRight: 'Price',),
                            Table(children:  titleTableRows, border: TableBorder.all(color: Colors.black),), 
                            Table(children:  priceTableRows, border: TableBorder.all(color: Colors.black),),                      
                            // createTable(),
                            Row(
                              children: <Widget>[                             
                                Padding(
                                  padding: const EdgeInsets.only(top : 30.0),
                                  child: Text("Showing 1 to $_currentLength of $total entries",style: TextStyle(fontSize: 32.0)),
                                ),                             
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top : 26.0, bottom: 16.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[                             
                                  InkWell(
                                    child: Padding(
                                      padding: EdgeInsets.only(top : 10.0),
                                      child: Text("Previous ",style: TextStyle(fontSize: 32.0)),
                                    ),
                                    onTap: (){
                                         selectedPage > 1 ? selectedPage-- : selectedPage;                                         
                                         print("decSelectedPage $selectedPage");
                                         DBProvider.db.getAllProductPrice("",int.parse(_currentLength),selectedPage).then((val){
                                              print("pricelistlengths $val");  
                                                setState(() {        
                                                    List<Productprice> pricelists = val;
                                                    updateTableRows(pricelists); 
                                                });        
                                          }); 
                                    },
                                  ),
                          
                                  RaisedButton(
                                      padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                                      onPressed: () {
                                        var jumlahpages = total /  int.parse(_currentLength);
                                       jumlahpagez= jumlahpages.round();
                                        print("onpressed $jumlahpagez $jumlahpagez");
                                        setState(() {
                                          _isClicked[0] = !_isClicked[0];
                                        });
                                        getDialog(jumlahpagez);
                                      },
                                      child: Text(
                                        selectedPage.toString(),
                                        style: TextStyle(fontSize: 30)
                                      ),
                                  ),
                                   InkWell(
                                      child: Padding(
                                        padding: const EdgeInsets.only(top : 10.0, left : 10.0),
                                        child: Text("Next",style: TextStyle(fontSize: 32.0)),
                                      ),
                                      onTap: (){
                                         selectedPage >= 1 ? selectedPage++ : selectedPage;                                         
                                         print("decSelectedPage $selectedPage");
                                         DBProvider.db.getAllProductPrice("",int.parse(_currentLength),selectedPage).then((val){
                                              print("pricelistlengths $val");  
                                                setState(() {        
                                                    List<Productprice> pricelists = val;
                                                    updateTableRows(pricelists); 
                                                });        
                                          });      
                                      },
                                   ),                                                                               
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),                                                                                                                            
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(10),
              ),                                    
            ),
        ],
      )
      ),
      ],
     ),
    );
    
  }
}