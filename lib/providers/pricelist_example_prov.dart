import 'dart:convert';

import 'package:buchi/models/pricelist_response.dart';
import 'package:buchi/providers/dbprovider2.dart';
import 'package:buchi/utils/consts.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:dio/dio.dart';
import 'db_provider.dart';

class PricelistExampleApiProvider {
  Future<List<dynamic>> getAllPriceList(String length) async {
    var url = Consts.base_url+"pricelist?length=$length";
    Response response = await Dio().get(url);   
    Map<String,dynamic> responseData;
    responseData = json.decode(response.data);  
    List responseJson =responseData["data"];
    int total = responseData["total"];
    int pages = responseData["pages"];
    print("total $total  pages $pages");
    DataConfig().setDataInt("total", total);
    DataConfig().setDataInt("pages", pages);
    print("Pricelist resp $responseJson");
    return responseJson;
    // return (responseJson).map((employee) {
    //   // print('Inserting $employee');
    //   DBProvider.db.createPricelist(PriceList.fromJson(employee));
    //   //  DBProvider.db.runInsertStatements(PriceList.fromJson(employee), null);
    // }).toList();

  }
}