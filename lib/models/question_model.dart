import 'dart:convert';

Questions welcomeFromJson(String str) => Questions.fromJson(json.decode(str));

String welcomeToJson(Question data) => json.encode(data.toJson());

class Questions {
    List<Question> question;

    Questions({
        this.question,
    });

    factory Questions.fromJson(Map<String, dynamic> json) => Questions(
        question: List<Question>.from(json["question"].map((x) => Question.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "question": List<dynamic>.from(question.map((x) => x.toJson())),
    };
}

class Question {
    int id;
    String question;
    int status;
    DateTime createdAt;
    dynamic updatedAt;

    Question({
        this.id,
        this.question,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Question.fromJson(Map<String, dynamic> json) => Question(
        id: json["id"],
        question: json["question"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "question": question,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}

enum UpdatedAtEnum { THE_00011130000000 }

final updatedAtEnumValues = EnumValues({
    "-0001-11-30 00:00:00": UpdatedAtEnum.THE_00011130000000
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
