import 'dart:convert';

import 'package:buchi/models/category_response.dart';
import 'package:buchi/models/first_question_model.dart';
import 'package:buchi/models/question_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/providers/dbprovider_category.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';

class QuestionApiProvider {
  
  Future <List<Question>> getAllQuestion() async {
    var url = Consts.base_url+"questions";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    // print("RESP QUESTIONS ${response.data}");
    responseData = json.decode(response.data);//response JSON data didecode agar bisa di parse    
    // print("nodecode ${response.data["question"]}");   
    List responseJson =responseData["question"]; //ambil data json question masukan ke variabel tipe data list              

    return (responseJson).map((question) {     
      // print('Inserting $question');      
      DBProvider.db.createQuestion(Question.fromJson(question));
    }).toList();

  }
}