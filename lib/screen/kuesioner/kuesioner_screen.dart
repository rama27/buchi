import 'package:buchi/models/kuisoner_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/data_config.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/table/table_kuesioner_list.dart';
import 'package:buchi/widget/table/table_price_list.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';



class KuesionerScreen extends StatefulWidget {
  @override
  _KuesionerScreenState createState() => _KuesionerScreenState();
}

class _KuesionerScreenState extends State<KuesionerScreen> {
  String title = "Kuesioner List";
  List _lengths =
    ["10", "25", "50","100"];
  List<String> _page =
  ["1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"];  
  bool _isLastClicked = false;
  String _lastPage = "1333";
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentLength;
  Container search;
  List<bool> _isClicked = [false,false,false,false,false];
  var searchController = TextEditingController();
  Container searchWidget;
  String searchQuery= "";
  bool _isSearching=false;
  List<TableRow> isiTableRows = [];
  List<TableRow> titleTableRows = [];
  var idKuisoner;
  var selectedPage=1;
  int total;  

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentLength = _dropDownMenuItems[0].value;
    search = Container(
      width: 435,
      height: 60,     
      child: TextFormField(  
      keyboardType: TextInputType.emailAddress,
      controller: searchController,
      autofocus: false,
      validator: (value) {
      if (value.isEmpty) {
          return 'Please enter text search';
        }
        return null;
      },  
      style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
      decoration: InputDecoration(
        // labelText: 'Search',
        hintText: 'Search',
        contentPadding: new EdgeInsets.symmetric(vertical: 10.0, horizontal: 50.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),    
      ),
    );   
    
    titleTableRows.add(                    
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("Name", style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("Date", style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text("Action", style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                      ])
      );      
    super.initState();
  }

  

  @override 
  void didChangeDependencies() {
       
    DBProvider.db.getAllKuisoner(searchQuery,int.parse(_currentLength),selectedPage).then((val){
       print("didChangeDep $val");

       setState(() {        
           List<Kuisoner > pricelists = val;    
           print("length ${pricelists.length}");
           total = pricelists.length;                
           updateTableRows(pricelists); 
       });        
    });

    DataConfig().getDataInt("total_kuisoner").then((val){
        print("total_kuisoner $val");
        setState(() {
          total=val;
        });
    });

      searchWidget = Container(
              width: Utils().width(context),
              height:  Utils().screenHeight(context, 10),
              padding: EdgeInsets.only(left: 30.0, right: 30.0),
               child:Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Flexible(
                                  child:
                                 TextFormField(  
                                  onFieldSubmitted: (value) {       
                                    setState(() {
                                      if(searchQuery == null){
                                        searchQuery = "";
                                      }else{
                                          print("submited $value");
                                        _isSearching = true;
                                        searchQuery = value;
                                        DBProvider.db.getAllKuisoner(searchQuery,int.parse(_currentLength),selectedPage).then((val){
                                        print("getAllKuisoner $val");

                                          setState(() {        
                                              List<Kuisoner> listKuisoner = val;
                                              updateTableRows(listKuisoner); 
                                          });        
                                        });
                                      }         
                                    });      
                                  } ,  
                                  keyboardType: TextInputType.text,
                                  controller: searchController,
                                  autofocus: false,
                                  validator: (value) {
                                  if (value.isEmpty) {
                                      return 'Please enter text search';
                                    }
                                    return null;
                                  },  
                                  style: new TextStyle(fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30.0),
                                  decoration: InputDecoration(
                                    // labelText: 'Search',
                                    hintText: 'Search',
                                    contentPadding: new EdgeInsets.symmetric(
                                      vertical: Utils().screenHeight(context, 2), 
                                      horizontal: Utils().screenWidth(context, 10)),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                    ),    
                                  ),    
                              ),   
                              (_isSearching) ?                              
                              GestureDetector(
                               child: Container(                      
                                      padding: new EdgeInsets.all(20.0),                      
                                      child :Image.asset('assets/images/close.png', height: Utils().screenHeight(context, 2),),                                       
                                ),
                                onTap : () {                                                                  
                                  print("Search End");          
                                  setState(() {
                                     _handleSearchEnd();     
                                  });                                                                                                                                                                                                                                                                       
                                },
                              )                                
                            :
                              Container(),                            
                            ],
                          ),                   
              );
    super.didChangeDependencies();
  }

  void _handleSearchEnd() {
    setState(() {    
      _isSearching = false;
       searchController.clear();
       searchQuery = "";
    });
    //  DBProvider.db.getAllProductPrice("",int.parse(_currentLength),0).then((val){
    //    print("close $val");

    //    setState(() {        
    //        List<Productprice> pricelists = val;
    //        updateTableRows(pricelists); 
    //    });        
    //   });
  }

  
  void updateTableRows(List<Kuisoner> kuisonerList) {
    // print("priceList $pricelists");
    isiTableRows = [];      

    for(Kuisoner kuisoner in kuisonerList){
        print(kuisoner.nama);            
        // var  tanggalSurveyFormat = new DateFormat("d/MM/y").format(dateTimeParsed);
        setState(() {
           idKuisoner = kuisoner.id;
        });     
        var  tanggalFormat = new DateFormat("d MMMM y H:m").format(kuisoner.createdAt);      
        print("tanggalFormat $tanggalFormat");
        isiTableRows.add(                    
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(kuisoner.nama, style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(26.0),
                          child: Text(tanggalFormat, style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),
                        GestureDetector(
                           child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Text("Open", style: TextStyle(
                              fontSize: 30.0,
                              fontWeight: FontWeight.normal,
                              color : Colors.blue
                              ),
                            ),
                          ),
                          onTap: (){
                            print("idkuisoner ${kuisoner.id}");
                            _launchURL(kuisoner.id);
                          },
                        ),
                      ])
         );      
      }      
  }

  _launchURL(int id) async {
  // const url = 'https://flutter.dev';
  var url = 'http://buchi-indonesia.co.id/questionnaire?id=$id';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}


  // here we are creating the list needed for the DropDownButton
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _lengths) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(
          value: city,
          child: Container(           
            child: new Text(city, style: TextStyle(fontSize: 35.0),))
      ));
    }
    return items;
  }

  void changedDropDownItem(String selectedCity) {
    print("Selected per page $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentLength = selectedCity;
    });
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),
                                          
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 35)),
                  child: IconButton(
                    padding: EdgeInsets.all(5.0),
                    icon: Image.asset('assets/images/refresh.png'),
                    onPressed: () {
                      // Implement navigation to shopping cart page here...
                      print('Click Menu');
                    },
                  ),
                ),                        
              ],
          ),                                                                                                                                                 
      ),   
    );

    return 
    Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: appBar,
      body: Stack(
    children: <Widget>[
      Container(
            color: Color.fromRGBO(148, 205, 12, 100),
      ),
      Padding(
      padding: EdgeInsets.only( top : Utils().screenHeight(context, 3),
                                bottom: Utils().screenHeight(context, 1)),
      child: Column(
        children: <Widget>[
        Container(
        height: Utils().screenHeight(context, 10),
        width: Utils().width(context),                      
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child:                              
                      Padding(
                          padding: EdgeInsets.only(top : Utils().screenHeight(context, 2),
                                                  bottom : Utils().screenHeight(context, 2),
                                                  left: Utils().screenHeight(context, 4),
                                                  right: Utils().screenHeight(context, 4),
                                                  ),
                          child: Text(
                            title,
                            style: TextStyle(
                                    fontSize: 60.0,
                                    fontWeight: FontWeight.bold,
                                    color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                          ),
                        ),                                                                                                               
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(10),
              ),                                    
            ),

        Container(
        height: Utils().screenHeight(context, 70),
        width: Utils().width(context),                      
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child:
                    Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Column(                      
                        children: <Widget>[
                          Row(children: <Widget>[
                             Text("Show", style: TextStyle(fontSize: 32.0),),
                             Padding(
                              padding: const EdgeInsets.only(left : 20.0, top: 20.0, right: 20.0),
                              child: Container(
                                width: 90, 
                                height: 70,                             
                                child: new DropdownButton(
                                  isExpanded: true,
                                  value: _currentLength,
                                  items: _dropDownMenuItems,
                                  onChanged: changedDropDownItem,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right : 250.0),
                              child: Text("Entries", style: TextStyle(fontSize: 32.0),),
                            ),                            
                          ],
                          ),    
                          searchWidget,                      
                          // TableKuesinonerList(cellLeft: 'Name', cellMiddle: 'Date', cellRight: 'Action',),
                          Table(children:  titleTableRows, border: TableBorder.all(color: Colors.black),), 
                          Table(children:  isiTableRows, border: TableBorder.all(color: Colors.black),), 
                          Row(
                            children: <Widget>[                             
                              Padding(
                                padding: const EdgeInsets.only(top : 30.0),
                                child: Text("Showing 1 to $_currentLength of $total entries",style: TextStyle(fontSize: 32.0)),
                              ),                             
                            ],
                          ),
                          Row(
                            children: <Widget>[                             
                              // Text("Previous ",style: TextStyle(fontSize: 32.0)),
                            //   _isClicked[0] ? 
                            //   RaisedButton(
                            //       padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                            //       onPressed: () {
                            //         print("onpressed");
                            //         setState(() {
                            //           _isClicked[0] = !_isClicked[0];
                            //         });
                            //       },
                            //       child: Text(
                            //        _page[0],
                            //         style: TextStyle(fontSize: 30)
                            //       ),
                            //   ) : GestureDetector(
                            //       child: Padding(
                            //       padding: const EdgeInsets.all(30.0),
                            //       child: Text(
                            //            _page[0],
                            //             style: TextStyle(fontSize: 30)
                            //       ),
                            //     ),
                            //     onTap: (){
                            //       setState(() {
                            //           _isClicked[0] = !_isClicked[0];
                            //       });
                            //     },
                            //   ) ,

                            //    _isClicked[1] ? 
                            //   Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: RaisedButton(
                            //         padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                            //         onPressed: () {
                            //             setState(() {
                            //               _isClicked[1] = !_isClicked[1];
                            //             });
                            //         },
                            //         child: Text(
                            //          _page[1],
                            //           style: TextStyle(fontSize: 30)
                            //         ),
                            //     ),
                            //   ) : GestureDetector(
                            //       child: Padding(
                            //       padding: const EdgeInsets.all(30.0),
                            //       child: Text(
                            //            _page[1],
                            //             style: TextStyle(fontSize: 30)
                            //       ),
                            //     ),
                            //     onTap: () {
                            //       setState(() {
                            //             _isClicked[1] = !_isClicked[1];
                            //       });
                            //     },
                            //   ),

                            //   _isClicked[2] ? 
                            //   Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: RaisedButton(
                            //         padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                            //         onPressed: () {
                            //             setState(() {
                            //               _isClicked[2] = !_isClicked[2];
                            //             });
                            //         },
                            //         child: Text(
                            //          _page[2],
                            //           style: TextStyle(fontSize: 30)
                            //         ),
                            //     ),
                            //   ) : GestureDetector(
                            //     child: Padding(
                            //       padding: const EdgeInsets.all(30.0),
                            //       child: Text(
                            //            _page[2],
                            //             style: TextStyle(fontSize: 30)
                            //       ),
                            //     ),
                            //     onTap: () {
                            //       setState(() {
                            //             _isClicked[2] = !_isClicked[2];
                            //       });
                            //     },
                            //   ),  
                              
                            //   _isClicked[3] ? 
                            //   Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: RaisedButton(
                            //         padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                            //         onPressed: () {
                            //             setState(() {
                            //               _isClicked[3] = !_isClicked[3];
                            //             });
                            //         },
                            //         child: Text(
                            //          _page[3],
                            //           style: TextStyle(fontSize: 30)
                            //         ),
                            //     ),
                            //   ) : GestureDetector(
                            //     child: Padding(
                            //       padding: const EdgeInsets.all(30.0),
                            //       child: Text(
                            //            _page[3],
                            //             style: TextStyle(fontSize: 30)
                            //       ),
                            //     ),
                            //     onTap: () {
                            //       setState(() {
                            //             _isClicked[3] = !_isClicked[3];
                            //       });
                            //     },
                            //   ),   

                            // _isClicked[4] ? 
                            //   Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: RaisedButton(
                            //         padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                            //         onPressed: () {
                            //             setState(() {
                            //               _isClicked[4] = !_isClicked[4];
                            //             });
                            //         },
                            //         child: Text(
                            //          _page[4],
                            //           style: TextStyle(fontSize: 30)
                            //         ),
                            //     ),
                            //   ) : GestureDetector(
                            //     child: Padding(
                            //       padding: const EdgeInsets.all(30.0),
                            //       child: Text(
                            //            _page[4],
                            //             style: TextStyle(fontSize: 30)
                            //       ),
                            //     ),
                            //     onTap: () {
                            //       setState(() {
                            //             _isClicked[4] = !_isClicked[4];
                            //       });
                            //     },
                            //   ),  
                            //   Text(".....",style: TextStyle(fontSize: 30)),
                            //   _isLastClicked ? 
                            //   Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: RaisedButton(
                            //         padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 24.0),
                            //         onPressed: () {
                            //             setState(() {
                            //               _isLastClicked = !_isLastClicked;
                            //             });
                            //         },
                            //         child: Text(
                            //          _lastPage,
                            //           style: TextStyle(fontSize: 30)
                            //         ),
                            //     ),
                            //   ) : GestureDetector(
                            //     child: Padding(
                            //       padding: const EdgeInsets.all(30.0),
                            //       child: Text(
                            //           _lastPage,
                            //             style: TextStyle(fontSize: 30)
                            //       ),
                            //     ),
                            //     onTap: () {
                            //       setState(() {
                            //              _isLastClicked = _isLastClicked;
                            //       });
                            //     },
                            //   ),
                              //  Text("Next",style: TextStyle(fontSize: 32.0)),                                                   
                            ],
                          ),                          
                        ],
                      ),
                    ),                                                                                                                            
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(10),
              ),                                    
            ),
        ],
      )
      ),
      ],
     ),
    );
    
  }
}