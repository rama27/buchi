import 'dart:convert';
import 'dart:io';

import 'package:buchi/models/category_response.dart';
import 'package:buchi/models/pricelist_response.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class PriceListProv with ChangeNotifier{
   List<PriceList> _items = [];   
   List<TableRow> priceTableRows = [];
   String _pages = "0";
   String _total = "0";

   List<PriceList> get items{
     return  _items;
   }

   void setItem(List<PriceList> items){
      _items = items;
   }

   String get pages{
     return _pages;
   }

   String get total{
     return _total;
   }

  
  Future<List<PriceList>> getPrice(
    String start , String length, String search) async{
      
    Map<String,dynamic> responseData;    
    List responseJson;      
    var baseurl = Consts.base_url;
    var url = baseurl + "pricelist"; 
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');      
    // Map<String, String> headers = 
    // {'Content-Type': 'application/json',
    // 'Accept': 'application/json',
    // 'Authorization': 'bmQ2cFVIM0ljS2M3TTliTlpkS3Y5SGlUbHEzd1FhQ0RJTGNqeG9nOUZqa1k4eUx0R0RGclluNjI5ang05e31bba4eeecb',}; 
    Map<String, String> headers = 
    {'Content-Type': 'application/json',};      
    print("CATEGORY $url $headers ");
 
    try{
        final response = await http.get(
        url, 
        headers:headers
        );               
        notifyListeners();      
        responseData = json.decode(response.body);  
        print("RESP DATA $responseData");
        responseJson = responseData["data"];
        // print("response ${responseJson}");
        _pages = responseData["pages"].toString(); 
        _total = responseData["total"].toString(); 
        _items =  responseJson.map((m) => new PriceList.fromJson(m)).toList();   
        print(responseData["data"][0]);
        DBProvider.db.createPricelist(PriceList.fromJson(responseData["data"][0]));                              
        // return responseJson.map((m) => new PriceList.fromJson(m)).toList();  
                               

    } catch(error)  {
          // print("auth "+error);
          //throw HttpException;
          print("Too many request");
          print(error);
          
    }
    // return responseJson.map((m) => new PriceList.fromJson(m)).toList();  
  }  

  void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  } 
  


}