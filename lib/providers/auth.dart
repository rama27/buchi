import 'dart:convert';

import 'package:buchi/models/http_exception.dart';
import 'package:buchi/models/login_response.dart';
import 'package:buchi/utils/consts.dart';
import 'package:buchi/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;


class Auth with ChangeNotifier{
  String _token;

  bool get isAuth{
    print('Getting_token $token');
    return token != null;
  }

  String get token{    
    if (_token != null) {
      print('Getting token $_token');
      return _token;
    }
    return null;
  }

  Future<LoginResponse> _authenticate(
    String email , String password) async{

    Map<String,dynamic> responseData;          
    var baseurl =  Consts.base_url;
    var loginurl = baseurl + "login";    
    print(loginurl);
 
    try{
        final response = await http.post(
        loginurl,           
        body:{'email':  email, 'password': password}
        ); 

        responseData = json.decode(response.body);  
        // print(response.statusCode);        
        //  print('Response body: ${response.body}');  
        //  print('Response data: ${responseData}');        

        if(response.statusCode == 200){
          if(responseData['message'] != null ){
            print(' message ${responseData['message']}');             
            // throw new Exception(responseData['message']);            
          }else{
             print('200');
             Utils().setMessage("berhasil_login");                                         
          }         
        }
        
        _token = responseData['access_token'];
         print('Token $_token');    
        //Save token 
        Utils().setToken(token);              
        notifyListeners();
      
    } catch(error)  {
          print(error);
          Utils().setMessage(error.toString());         
    }    
     return LoginResponse.fromJson(responseData);      
  }

  Future<LoginResponse> login(String email, String password) async {    
    return _authenticate(email, password);    
  }

}