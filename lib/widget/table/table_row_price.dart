import 'package:buchi/providers/db_provider.dart';
import 'package:flutter/material.dart';

class TableRowPrice extends StatefulWidget {
  String nameProduct;
  String priceProduct;
  int idProduct;
  TableRowPrice({@required this.nameProduct,
                  @required this.priceProduct,
                  @required this.idProduct
                  });

  @override
  _TableRowPriceState createState() => _TableRowPriceState();
}



class _TableRowPriceState extends State<TableRowPrice> {
  int price;
  @override
  void initState() {
  //  DBProvider.db.getProduct(widget.idProduct).then((val){
  //     print("initState");
  //     print("getPloduk ${val.name}");
  //     // setState(() {
  //     //  price = val.price;
  //     // });    
  //     price = val.price;  
  //     print("pricenow $price");
  //   });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies    
    super.didChangeDependencies();  
    // DBProvider.db.getProduct(widget.idProduct).then((val){
    //   print("initState");
    //   print("getPloduk ${val.name}");
    //   // setState(() {        
    //   //   price = val.price;
    //   // }); 
    //   price = val.price;     
    //   print("pricenow $price");
    // });
  }

  @override
  Widget build(BuildContext context) {
    
    print("idProduct ${widget.idProduct}");
    print("table_row_price ${widget.priceProduct}");
    
    // DBProvider.db.getProduct(widget.idProduct).then((val){
    //   print("initState");
    //   print("getPloduk ${val.name}");
    //   setState(() {        
    //     price = val.price;
    //     print("pricenow $price");
    //   });         
    // });

     var x= widget.priceProduct;
     x = x.substring(0, 3) + "." + x.substring(3, 6) + "." + x.substring(6, x.length);
     print(x) ;
    return  Table(
          border: TableBorder.all(color: Colors.black),
          children: [
            TableRow(children: [
              Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text("Name",style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                        color : Color.fromRGBO(108,117, 125, 1.0)
                        ),
                ),
              ), 
           
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text("Price",style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),
                    ),
              ),                               
            ]),                                 
        
              TableRow(children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('${widget.nameProduct}',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Rp $x',style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.normal,
                            color : Color.fromRGBO(108,117, 125, 1.0)
                            ),),
                ), 
                                                      
              ]
              ),
           ],
        );

  }
}