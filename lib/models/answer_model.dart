import 'dart:convert';

Answers welcomeFromJson(String str) => Answers.fromJson(json.decode(str));

String welcomeToJson(Answers data) => json.encode(data.toJson());

class Answers {
    List<Answer> answer;

    Answers({
        this.answer,
    });

    factory Answers.fromJson(Map<String, dynamic> json) => Answers(
        answer: List<Answer>.from(json["answer"].map((x) => Answer.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "answer": List<dynamic>.from(answer.map((x) => x.toJson())),
    };
}

class Answer {
    int id;
    int idquestion;
    String answer;
    String img;
    int status;
    DateTime createdAt;
    String updatedAt;

    Answer({
        this.id,
        this.idquestion,
        this.answer,
        this.img,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Answer.fromJson(Map<String, dynamic> json) => Answer(
        id: json["id"],
        idquestion: json["idquestion"],
        answer: json["answer"] == null ? null : json["answer"],
        img: json["img"] == null ? null : json["img"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idquestion": idquestion,
        "answer": answer == null ? null : answer,
        "img": img == null ? null : img,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}
