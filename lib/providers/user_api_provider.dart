import 'dart:convert';

import 'package:buchi/models/user_resp.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';

class UserApiProvider {
  Future<List<UserResponse>> getAllCategory() async {
    var url = Consts.base_url+"category";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    responseData = json.decode(response.data);   //response JSON data didecode agar bisa di parse     
    // print("APIPROVIDER ${responseData["category"]}");
    List responseJson =responseData["category"];  //ambil data json  category masukan ke variabel tipe data list  

    return (responseJson).map((user) {     
      // print('Inserting $user');
      // DBProvider.db.createCategory(CategoryResp.fromJson(category));
    }).toList();

  }
}