import 'package:flutter/material.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/models/column_model.dart';

class DataTableWidget extends StatefulWidget {
  int idProduct;
  DataTableWidget({@required this.idProduct});
  @override
  _DataTableWidgetState createState() => _DataTableWidgetState();
}

class _DataTableWidgetState extends State<DataTableWidget> {
  final List<Map<String, String>> listOfColumns = [];
  Map<String, String> columns = new Map();
  // [
  //   {"Name": "AAAAAA", "Number": "1", "State": "Yes"},
  //   {"Name": "BBBBBB", "Number": "2", "State": "no"},
  //   {"Name": "CCCCCC", "Number": "3", "State": "Yes"}
  // ];

  @override
  void initState() {
  List<String> listCode= new List();
  List<String> listName= new List();
    DBProvider.db.getConfiguration(widget.idProduct).then((val){        
      for(var v in val){
        print("item konfig ${v.name}");      
       
        setState(() {
          listCode.add(v.code);
          listName.add(v.name);                     
        });                     
                 
      }  
      var indeks = 0;
      for(var i in listName){
          columns['name']=i;   
          var nameValue = columns.putIfAbsent("name", () => i);          
          listOfColumns.insert(indeks, {"code": "${listCode[indeks]}","name": "$nameValue"});            
         indeks++;
      }           
       print("listOfColumns $listOfColumns"); 
    });
    
    // DBProvider.db.getFeature(widget.idProduct).then((val){ 
    //   print("getFeature $val");
    // });

    super.initState();
  }
  
  @override
  Widget build(BuildContext context) { 
    return DataTable(
      columns: [
        DataColumn(
        label: 
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text('Code',style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                          ),
          ),
        ),
        ),
        DataColumn(
          label: 
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text('Name',style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                          ),
        ),
          ),
        ),     
      ],
      rows:
          listOfColumns // Loops through dataColumnText, each iteration assigning the value to element
              .map(
                ((element) => DataRow(
                      cells: <DataCell>[
                        DataCell(
                             Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(element["code"],style: TextStyle(
                                        fontSize: 30.0,
                                        fontWeight: FontWeight.normal,
                                        color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                ),
                              ),                         
                        ), //Extracting from Map element the value
                        DataCell(
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(element["name"],style: TextStyle(
                                        fontSize: 30.0,
                                        fontWeight: FontWeight.normal,
                                        color : Color.fromRGBO(108,117, 125, 1.0)
                                        ),
                                ),
                           ),                          
                        ),                     
                      ],
                    )),
              )
              .toList(),
    );
  }
}