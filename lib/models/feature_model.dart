class Features {
    List<Feature> feature;

    Features({
        this.feature,
    });

    factory Features.fromJson(Map<String, dynamic> json) => Features(
        feature: List<Feature>.from(json["feature"].map((x) => Feature.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "feature": List<dynamic>.from(feature.map((x) => x.toJson())),
    };
}

class Feature {
    int id;
    String name;
    int status;
    DateTime createdAt;
    dynamic updatedAt;

    Feature({
        this.id,
        this.name,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Feature.fromJson(Map<String, dynamic> json) => Feature(
        id: json["id"],
        name: json["name"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}

enum UpdatedAtEnum { THE_00011130000000 }

final updatedAtEnumValues = EnumValues({
    "-0001-11-30 00:00:00": UpdatedAtEnum.THE_00011130000000
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
