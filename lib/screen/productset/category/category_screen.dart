import 'package:buchi/models/category_response.dart';
import 'package:buchi/providers/category.dart';
import 'package:buchi/providers/category_api_provider.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/providers/dbprovider_category.dart';
import 'package:buchi/screen/productset/category/option/option_screen.dart';
import 'package:buchi/screen/productset/category/option/question/question_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/card/category_item.dart';
import 'package:buchi/widget/drawer/side_drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CaterogyScreen extends StatefulWidget { 

  @override
  _CaterogyScreenState createState() => _CaterogyScreenState();
}

class _CaterogyScreenState extends State<CaterogyScreen> {
  bool isLoading = false;
  List<CategoryResp> categoryList = [];
  String imageLink;

  @override
  void initState() {
   DBProvider.db.getAllCategory().then((val){
       print("SQLITE CATEGORY $val");

       for(var v in val){
         setState(() {
           categoryList.add(v); 
         });
          
          print("Category $categoryList");
       } 
       
                
      //  updateTableRows(val);   
     });
    super.initState();
  }


  @override
  void didChangeDependencies() {

    // Provider.of<CategoryProvider>(context, listen: false).getCategory("0", "100", "").then((_){      
    //  });

    //  DBProvider.db.getAllCategory().then((val){
    //    print("SQLITE CATEGORY $val");

    //    for(var v in val){
    //       categoryList.add(v); 
    //       print("Category $categoryList");
    //    } 
       
                
    //    updateTableRows(val);   
    //  });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // print("list category $categoryList");

  void navigationPage(int idCat) {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => OptionScreen(idCategory: idCat,)
    ));     
  }

  
   _loadFromApi() async {
    setState(() {
      isLoading = true;
    });

    var apiProvider = CategoryApiProvider();
    await apiProvider.getAllCategory().then((val){
      // print("ioo $val");
      // DBProvider.db.getAllEmployees().then((val){
      //    print("getPriceSQL $val");
      // });
    });

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
    });
  }
 
  var appBar = PreferredSize(
    preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
    child : new AppBar(                       
          backgroundColor: Colors.white,                                 
          flexibleSpace: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,           
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                  child: Container(child: GestureDetector(
                                          child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                          onTap: () => _scaffoldKey.currentState.openDrawer(),
                                          ),
                                          
                                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                  child: Image.asset(
                    'assets/images/buchi_logo2.png',                                                    
                    height: Utils().heightDivided(context, 25),
                  ),
                ), 
                Padding(
                  padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), 
                  left: Utils().screenWidth(context, 30)),
                  child: IconButton(
                    padding: EdgeInsets.all(5.0),
                    icon: Image.asset('assets/images/refresh.png'),
                    onPressed: () async {
                      // Implement navigation to shopping cart page here...                     
                      print('Click cATEGORY');                          
                       _loadFromApi();
                    },
                  ),
                ),                        
              ],
          ),                                                                                                                                                 
      ),   
    );

  
    var size = MediaQuery.of(context).size;  
    var appBarHeight = appBar.preferredSize.height;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - appBarHeight - 24) / 4;
    final double itemWidth = size.width / 2;
    final orientation = MediaQuery.of(context).orientation; 

    return Scaffold(
      key : _scaffoldKey,
      appBar : appBar,
      drawer : SideDrawer(),  
      body : 
       Stack(
         children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),

           Padding(
             padding: const EdgeInsets.all(16.0),
             child:  
            //  _buildGridView(categoryList)
              FutureBuilder(
               future: DBProvider.db.getAllCategory(),
               builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                 if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                 } else {
                 return GridView.builder(               
                  gridDelegate:  SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    // print("index $index");                 
                    return GestureDetector(
                      child: CategoryItem(path: snapshot.data[index].picture, title:snapshot.data[index].name,),
                      onTap: () {
                          // print("idCategory ${snapshot.data[index].id}");
                          navigationPage(snapshot.data[index].id);
                        },
                    );                     
                    }
                  );
                 }
               },                
            ),         
          ),
         ],
       )      
    );    
  }

   _buildGridView(List<CategoryResp> data){     
     final orientation = MediaQuery.of(context).orientation; 
     GridView.builder(               
                  gridDelegate:  SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
                  itemBuilder: (BuildContext context, int index) {                 
                    print("picture ${data[index].picture}");
                    return GestureDetector(                      
                      child: CategoryItem(path: data[index].picture, title: data[index].name,),
                      onTap: () {
                        // navigationPage();
                      }
                      ,
                    );                     
                  }
      );
   }

}