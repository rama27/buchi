import 'package:buchi/models/folder_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

class FolderApiProvider {
  
  Future <List<Folder>> getAllFolder() async {
    var url = Consts.base_url+"folder";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    // print("RESP QUESTIONS ${response.data}");
    responseData = json.decode(response.data);//response JSON data didecode agar bisa di parse    
    // print("responseData $responseData");   
     List responseJson =responseData["folder"]; //ambil data json question masukan ke variabel tipe data list              
    print("responseJsonfolder $responseJson");
    //fungsi map disini persis looping for 
    //yaitu mengeluarkan json object satu persatu data dari json array 
    return (responseJson).map((val) {           
      print('Inserting folder $val');            
      DBProvider.db.createFolder(Folder.fromJson(val));
    }).toList();

  }
}