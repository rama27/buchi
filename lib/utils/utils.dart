
import 'package:buchi/models/pricelist_response.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils{

  static Utils utility = null;  
  static String token = "token";  
  static Utils getInstance(){
    if(utility == null){
      utility = Utils();
    }
    return utility;
  }

  showAlertDialog(BuildContext context, String alertTitle, String alertMessage){
     final GlobalKey<NavigatorState> navigatorKey = GlobalKey(debugLabel: "Main Navigator");
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Tidak"),
      onPressed:  () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Ya"),
      onPressed:  () {                  
        // locator<NavigationService>().navigateTo('login'); 
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(alertTitle),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Text(alertMessage)
          ],
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );

  }

  void navigationPage(BuildContext context) {
   Navigator.of(context).pushReplacementNamed('/LoginScreen');
  }

  void checkCurrentRoute(BuildContext context){
    final newRouteName = "/NewRoute";
    bool isNewRouteSameAsCurrent = false;

    Navigator.popUntil(context, (route) {
    if (route.settings.name == newRouteName) {
      isNewRouteSameAsCurrent = true;
    }
    return true;
    });

    if (!isNewRouteSameAsCurrent) {
      Navigator.pushNamed(context, newRouteName);
    }
  }

  double width(BuildContext context){
    var width = MediaQuery.of(context).size.width; 
    return width;
  }

   double height(BuildContext context){
    var height = MediaQuery.of(context).size.height;  
    return height;
  }

  double widthDivided(BuildContext context, double dividedBy){
    var width = MediaQuery.of(context).size.width; 
    return width / dividedBy;
  }

  double heightDivided(BuildContext context, double dividedBy){
    var height = MediaQuery.of(context).size.height;  
    return height / dividedBy;
  }

  double screenWidth(BuildContext context, double times){
    var width = MediaQuery.of(context).size.width;
    var blockSize = width / 100;
    return blockSize * times;
  }
  
  double screenHeight(BuildContext context, double times){
    var height = MediaQuery.of(context).size.height;
    var blockSize = height / 100;
    return blockSize * times;
  }
  
  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token =  prefs.getString('token');
    //  print("Token skrg "+token);
     return token;
  }

  Future<void> setToken(String token) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  Future<void> setMessage(String msg) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('message', msg);
  }

  Future<String> getMessage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var msg =  prefs.getString('message');  
    return msg;
  }

}