import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';

class SharedItem extends StatelessWidget {

  String path;
  String title;
  List extensions=new List();
    
  SharedItem({@required this.path, @required this.title});



  @override
  Widget build(BuildContext context) {

   Widget _setIcon(String namafile){   
      if(!namafile.contains(".")){
          return  
            Image.asset(
                  'assets/images/folder.png', 
                  fit: BoxFit.none,
              );   
      }else{
          var extension = namafile.split(".").last;
          extensions.add(extension);
          if(extension.contains('pdf')){        
            return  
            Image.asset(
                  'assets/images/pdf.png', 
                  fit: BoxFit.none,
              );    
            // Container(
            //    height: Utils().screenHeight(context,  1.5),
            //    child : Image.asset('assets/images/pdf.png',fit: BoxFit.cover,), 
            // );
          }else if(extension.contains('xlsx') || extension.contains('xls')
                  || extension.contains('docx') || extension.contains('doc') 
                  || extension.contains('txt') || extension.contains('csv')){
            return Image.asset(
                  'assets/images/file.png', 
                  fit: BoxFit.none,
              );    
            // return  Container(
            //    height: Utils().screenHeight(context,  1.5),
            //    child : Image.asset('assets/images/file.png',fit: BoxFit.cover,), 
            // );
          }else if(extension.contains('mp4') || extension.contains('mkv') || 
          extension.contains('flv') || extension.contains('avi')) {
            return Image.asset(
                  'assets/images/video.png', 
                  fit: BoxFit.none,
            );  
            // return  Container(
            //    height:Utils().screenHeight(context,  1.5),
            //    child : Image.asset('assets/images/video.png',fit: BoxFit.cover,), 
            // ); 
          }else{
            return Image.asset(
                  'assets/images/file.png', 
                  fit: BoxFit.none,
            );  
            // return  Container(
            //    height:Utils().screenHeight(context, 1.5),
            //    child : Image.asset('assets/images/file.png',fit: BoxFit.cover,), 
            // ); 
          } 
      }              
  }
    return  Container(
                  height: Utils().screenHeight(context, 200),
                  width: Utils().screenWidth(context, 100),                      
                      child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[ 
                         
                            Padding(
                                      padding: EdgeInsets.only(left : Utils().screenWidth(context, 0)),
                                      child: RaisedButton(
                                        onPressed: () {
                                          // showDialog();
                                        },
                                        textColor: Colors.white,
                                        padding: const EdgeInsets.all(0.0),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Colors.red,                                 
                                          ),
                                          padding: EdgeInsets.fromLTRB(Utils().screenWidth(context, 2),
                                                                      Utils().screenHeight(context, 1.5),
                                                                      Utils().screenWidth(context, 2),
                                                                      Utils().screenHeight(context, 1.5),
                                                                      ),
                                          child:                                           
                                          InkWell(
                                            onTap: (){ 
                                                                                          
                                            },
                                             child: Image.asset('assets/images/close_white.png',
                                             height: Utils().screenHeight(context, 1.5),),
                                          )
                                        ),                            
                                      ),
                            ),

                           Expanded(
                             child: Center(                            
                              child: 
                                _setIcon(title),
                                // Image.asset(
                                //     path, 
                                //     fit: BoxFit.none,
                                // ),                              
                                ),
                           ),
                                                 
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Expanded(
                               child: Center(
                                  child: Text(
                                    title,
                                    style: TextStyle(
                                            fontSize: 35.0,
                                            fontWeight: FontWeight.bold,
                                            color : Color.fromRGBO(108,117, 125, 1.0)
                                    ),
                                  ),
                                ),
                              ),
                          ),          
                            ],
                          ),    
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),                                    
                );
  }
}