import 'package:flutter/material.dart';

class OptionItem extends StatelessWidget {

  String path;
  String title;

  OptionItem({@required this.path, @required this.title});

  @override
  Widget build(BuildContext context) {
    return  Container(
                  height: 1000,                      
                      child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Column(
                            children: <Widget>[
                                Expanded(
                                  child: Image.asset(
                                  path, fit: BoxFit.fill,),
                                ),
                                Text(
                                  title,
                                  style: TextStyle(
                                          fontSize: 35.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                  ),
                                ),          
                            ],
                          ),    
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),                                    
                );
  }
}