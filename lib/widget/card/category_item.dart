import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {

  String path;
  String title;

  CategoryItem({@required this.path, @required this.title});

  @override
  Widget build(BuildContext context) {
    // print("Image Url $path");
    String imageUrl = "http://buchi-indonesia.co.id/img/img/$path";
    // print(imageUrl);
    return  Container(
                  height: 500,                      
                      child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Column( 
                            children: <Widget>[
                                Expanded(
                                  child: Image.network(
                                  imageUrl, fit: BoxFit.fill,),
                                ),
                                Text(
                                  title,
                                  style: TextStyle(
                                          fontSize: 35.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                  ),
                                ),          
                            ],
                          ),    
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),                                    
                );
  }
}