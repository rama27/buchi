import 'dart:convert';

List<CategoryResp> welcomeFromJson(String str) => List<CategoryResp>.from(json.decode(str).map((x) => CategoryResp.fromJson(x)));

String welcomeToJson(List<CategoryResp> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryResp {
    int id;
    String name;
    String picture;
    int status;
    int sort;
    DateTime createdAt;
    DateTime updatedAt;

    CategoryResp({
        this.id,
        this.name,
        this.picture,
        this.status,
        this.sort,
        this.createdAt,
        this.updatedAt,
    });

    factory CategoryResp.fromJson(Map<String, dynamic> json) {
      return new CategoryResp(
        id: json["id"],
        name: json["name"],
        picture: json["picture"],
        status: json["status"],
        sort: json["sort"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );
    } 

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "picture": picture,
        "status": status,
        "sort": sort,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}