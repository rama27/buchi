class FolderModel {
    List<Folder> folder;

    FolderModel({
        this.folder,
    });

    factory FolderModel.fromJson(Map<String, dynamic> json) => FolderModel(
        folder: List<Folder>.from(json["folder"].map((x) => Folder.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "folder": List<dynamic>.from(folder.map((x) => x.toJson())),
    };
}

class Folder {
    int id;
    int categoryid;
    String name;
    int parent;
    int status;
    int userid;
    DateTime createdAt;

    Folder({
        this.id,
        this.categoryid,
        this.name,
        this.parent,
        this.status,
        this.userid,
        this.createdAt,
    });

    factory Folder.fromJson(Map<String, dynamic> json) => Folder(
        id: json["id"],
        categoryid: json["categoryid"],
        name: json["name"],
        parent: json["parent"],
        status: json["status"],
        userid: json["userid"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "categoryid": categoryid,
        "name": name,
        "parent": parent,
        "status": status,
        "userid": userid,
        "created_at": createdAt.toIso8601String(),
    };
}
