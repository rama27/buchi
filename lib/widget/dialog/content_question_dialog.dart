import 'package:buchi/models/feature_model.dart';
import 'package:buchi/models/product_img_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/screen/productset/category/option/question/question_screen.dart';
import 'package:buchi/screen/productset/category/option/question/result/result_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:flutter/material.dart';

class ContentQuestionDialog extends StatefulWidget {  
  String answer;
  String img;
  int idcat,idquestion;

  ContentQuestionDialog({
                         @required this.answer,
                         @required this.img,
                         @required this.idcat,
                         @required this.idquestion,
                        });    

  @override
  _ContentQuestionDialogState createState() => _ContentQuestionDialogState();
}

class _ContentQuestionDialogState extends State<ContentQuestionDialog> {
  String baseurlimg="http://buchi-indonesia.co.id/img/img/answer/";
  
  

  @override
  Widget build(BuildContext context) {    
    print("dialogimg ${widget.img}");

    void navigationPage() { 
      // Navigator.of(context).pushReplacementNamed('/home');
      Navigator.push(context,
      MaterialPageRoute(builder: (context) => ResultScreen(idproduct: null, featureList: <Feature>[], price: null, product: null, imageList: <Productimg>[], keterangan: null,)
      ));
    }

    double c_width = MediaQuery.of(context).size.width*0.80;
    // String keterangan = "System evaporasi yang BUCHI miliki, dilengkapi dengan Foaming Sensor. Foaming Sensor ini akan bekerja, ketika sample extract mulai berbuih, maka system akan melakukan  aerasi /menaikkan tekanan vacuum secara otomatis. Sehingga destilat yang sudah murni tidak terkontaminasi sample";
    return  Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: c_width,
                height: Utils().screenHeight(context, 80.0),  //seting heigth card                
                child: Card(
                    elevation: 10.0,                
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top : 50.0,  left : 60.0),
                                child: Text("Information",
                                      style: TextStyle(fontWeight: FontWeight.normal, fontSize: 40),),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.of(context).pop();                                                                
                                },
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(40.0,60.0,50.0,0.0),
                                  child: Image.asset('assets/images/close24.png'),                                  
                                ),                                
                              )
                            ],
                          ),
                        
                         widget.answer != null ?    
                          Padding(
                          padding: EdgeInsets.only(bottom : 15.0,left: Utils().screenHeight(context, 3),
                                                   top: Utils().screenHeight(context, 4), right:  Utils().screenHeight(context, 3) ),
                          child: new Container(
                            width: Utils().screenWidth(context, 75),
                            height: Utils().screenHeight(context, 25),
                            padding: const EdgeInsets.only(bottom : 25.0),
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                // baseurlimg+widget.img
                                image: 
                                new NetworkImage(baseurlimg+widget.img),
                                // new AssetImage('assets/images/samplebumping.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(new Radius.circular(10.0)),              
                            ),
                          ),
                        ) : 

                        Padding(
                          padding: EdgeInsets.only(bottom : 15.0,left: Utils().screenHeight(context, 3),
                                                   top: Utils().screenHeight(context, 4), right:  Utils().screenHeight(context, 3) ),
                          child: new Container(
                            width: Utils().screenWidth(context, 75),
                            height: Utils().screenHeight(context, 50),
                            padding: const EdgeInsets.only(bottom : 25.0),
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                // baseurlimg+widget.img
                                image: 
                                new NetworkImage(baseurlimg+widget.img),
                                // new AssetImage('assets/images/samplebumping.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(new Radius.circular(10.0)),              
                            ),
                          ),
                        ),

                        Expanded(
                            child: Padding(
                                  padding: const EdgeInsets.only(top : 50.0,  left : 60.0, right: 40.0),
                                  child: ListView(
                                          children: <Widget>[
                                            widget.answer != null ?
                                            Text(widget.answer,
                                            style: TextStyle(fontWeight: FontWeight.normal, fontSize: 40),) 
                                            :
                                            Text("",
                                            style: TextStyle(fontWeight: FontWeight.normal, fontSize: 40),) 
                                          ],                                          
                                  ),
                          ),
                        ),                                                 
                        //Button  
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Padding(
                                    padding: EdgeInsets.only(top: 50.0, right: 50.0, bottom: 40.0),   
                                    child : new SizedBox(      
                                    width : Utils().screenWidth(context, 15),   
                                    height: Utils().screenHeight(context, 6),
                                    child : RaisedButton(
                                      shape : new RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.circular(10.0),
                                          side: BorderSide(color: Colors.white)), 
                                      child : Padding( 
                                          child: Text('Next',style: TextStyle(
                                                                      fontWeight: FontWeight.normal,
                                                                      fontSize: 30.0,
                                                                      color: Colors.white
                                                                    ),
                                                                    ),
                                        padding: EdgeInsets.all(0),
                                      ),
                                      color: Colors.blue,
                                      textColor:  Colors.white, 
                                      onPressed: ()  {
                                        // navigationPage();   
                                        Navigator.pop(context, 'success');                                                            
                                      }
                                      ),
                                      )
                                    ),
                                  ),
                      ],
                    ),
                  ),                                      
                  margin: EdgeInsets.only(bottom: 300, left: 5.0, right: 5.0),               
                  ),
                );                  
              }
}