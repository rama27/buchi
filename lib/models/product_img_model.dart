import 'dart:convert';

Productimgs productImgFromJson(String str) => Productimgs.fromJson(json.decode(str));

String productImgToJson(Productimgs data) => json.encode(data.toJson());

class Productimgs {
    List<Productimg> productimg;

    Productimgs({
        this.productimg,
    });

    factory Productimgs.fromJson(Map<String, dynamic> json) => Productimgs(
        productimg: List<Productimg>.from(json["productimg"].map((x) => Productimg.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "productimg": List<dynamic>.from(productimg.map((x) => x.toJson())),
    };
}

class Productimg {
    int id;
    int idproduct;
    String img;
    int status;
    DateTime createdAt;
    DateTime updatedAt;

    Productimg({
        this.id,
        this.idproduct,
        this.img,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Productimg.fromJson(Map<String, dynamic> json) => Productimg(
        id: json["id"],
        idproduct: json["idproduct"],
        img: json["img"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idproduct": idproduct,
        "img": img,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
