import 'dart:convert';
import 'package:buchi/models/dokumen_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';

class DocumentApiProvider {
  Future<List<Dokumen>> getAllDocument() async {
    var url = Consts.base_url+"documents";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    responseData = json.decode(response.data);   //response JSON data didecode agar bisa di parse     
    // print("APIPROVIDER ${responseData["category"]}");
    List responseJson =responseData["document"];  //ambil data json  category masukan ke variabel tipe data list  

    return (responseJson).map((document) {     
      print('Inserting $document');
       DBProvider.db.createDocument(Dokumen.fromJson(document));
    }).toList();

  }
}