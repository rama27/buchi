import 'dart:io';
import 'package:buchi/models/category_response.dart';
import 'package:buchi/models/pricelist_response.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProviderCategory {
  static Database _database;
  static final DBProviderCategory db = DBProviderCategory._();

  DBProviderCategory._();

  Future<Database> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();

    return _database; 
  }

  // void _onUpgrade(Database db, int oldVersion, int newVersion) {
  //     // If you need to add a column
  //     if (newVersion > oldVersion) {
  //       // db.execute('DROP TABLE IF EXISTS Pricelist');
  //       // db.execute("ALTER TABLE Pricelist ADD COLUMN status INTEGER");   
  //       db.execute("ALTER TABLE Pricelist ADD COLUMN status INTEGER");      
  //     }
  // }

  // Create the database and the pricelist table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'buchi_database.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE CategoryResp(id INTEGER PRIMARY KEY,name TEXT,picture TEXT,status INTEGER,sort INTEGER,created_at TEXT,updated_at TEXT)');
    }, 
    // onUpgrade: _onUpgrade
    );      
     // UPGRADE DATABASE TABLES
  }
  
  // Insert CategoryResp on database
  createCategoryResp(CategoryResp newEmployee) async {
    await deleteAllCategoryResp();
    final db = await database;
    final res = await db.insert('CategoryResp', newEmployee.toJson());
    print("createCategoryResp $res");
    return res;
  }

  Future<int>  dropTable() async {
    final db = await database;
    final res = db.rawDelete('DROP TABLE IF EXISTS Pricelist');
    return res;
  }

  // Delete all pricelist
  Future<int> deleteAllCategoryResp() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM CategoryResp');

    return res;
  }

  Future<List<CategoryResp>> getAllCategoryResp() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM CategoryResp");

    List<CategoryResp> list =
        res.isNotEmpty ? res.map((c) => CategoryResp.fromJson(c)).toList() : [];

    return list;
  }
}