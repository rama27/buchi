import 'package:buchi/providers/answer_api_provider.dart';
import 'package:buchi/providers/category_api_provider.dart';
import 'package:buchi/providers/document_api_provider.dart';
import 'package:buchi/providers/feature_api_provider.dart';
import 'package:buchi/providers/feature_product_api_provider.dart';
import 'package:buchi/providers/kuesioner_api_provider.dart';
import 'package:buchi/providers/pricelist_api_provider.dart';
import 'package:buchi/providers/product_goods_api_provider.dart';
import 'package:buchi/providers/product_img_api_provider.dart';
import 'package:buchi/providers/product_price_api_provider.dart';
import 'package:buchi/providers/question_api_provider.dart';
import 'package:buchi/providers/product_api_provider.dart';
import 'package:buchi/providers/folder_api_provider.dart';
import 'package:buchi/providers/question_products_provider.dart';
import 'package:buchi/screen/kuesioner/kuesioner_screen.dart';
import 'package:buchi/screen/pricelist/pricelist_screen.dart';
import 'package:buchi/screen/productset/category/category_screen.dart';
import 'package:buchi/screen/service_engineer/shared_screen.dart';
import 'package:buchi/utils/utils.dart';
import 'package:buchi/widget/drawer/side_drawer.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}
    

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  
  void navigationPage() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => CaterogyScreen()
    ));    
  //  Navigator.of(context).pushReplacementNamed('/category'); 
  }

  void navigationPageMechanic() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => SharedScreen()
    ));    
  //  Navigator.of(context).pushReplacementNamed('/category'); 
  }

  void navigationPagePriceList() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => PriceListScreen()
    ));    
  //  Navigator.of(context).pushReplacementNamed('/category'); 
  }

  void navigationPageKuesioner() {   
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => KuesionerScreen()
    ));    
  //  Navigator.of(context).pushReplacementNamed('/category'); 
  }


   _loadFromApi() async {
    setState(() {
      isLoading = true;
    });
    // var apiProvider12 = KuisonerApiProvider();
    // await apiProvider12.getAllKuesioner().then((val){
    // print("kuesioner from API $val");     

    //     setState(() {
    //       isLoading = false;
    //     });  
    // });  

    // var apiProvider7 = FeatureProductApiProvider();
    // await apiProvider7.getAllFeatureProduct().then((val) async {
    // print("featureproducts from API $val");  
    //   setState(() {
    //       isLoading = false;
    //   });  
    // }); 

    var apiProvider = CategoryApiProvider();
    await apiProvider.getAllCategory().then((val) async {
      print("get category $val");     
       var apiProvider2 = PricelistApiProvider();
        await apiProvider2.getAllPriceList("10").then((val) async {
          print("get pricelist $val");                
          var apiProvider3 = QuestionProductApiProvider();
          await apiProvider3.getAllQuestionProduct().then((val) async {
            print("get question product $val");   
              var apiProvider4 = AnswerApiProvider();
              await apiProvider4.getAllAnswer().then((val) async {
                print("get answer $val");                     
                var apiProvider5 = QuestionApiProvider();
                await apiProvider5.getAllQuestion().then((val) async {
                  print("questions from API $val");                          
                    var apiProvider6 = ProductApiProvider();
                    await apiProvider6.getAllProduct().then((val) async {
                      print("products from API $val");                       
                      var apiProvider7 = FeatureProductApiProvider();
                      await apiProvider7.getAllFeatureProduct().then((val) async {
                        print("featureproducts from API $val");                          
                          var apiProvider8 = ProductGoodsApiProvider();
                          await apiProvider8.getAllProductGood().then((val) async {
                            print("productgood from API $val");                                    
                              var apiProvider9 = ProductPriceApiProvider();
                              await apiProvider9.getAllProductPrice().then((val) async {
                                print("productprice from API $val");                                  
                                  var apiProvider10 = ProductImgApiProvider();
                                  await apiProvider10.getAllProductImg().then((val) async {
                                    print("productimg from API $val");     
                                       var apiProvider11 = FeatureApiProvider();
                                        await apiProvider11.getAllFeature().then((val) async {
                                          print("feature from API $val"); 
                                            var apiProvider12 = KuisonerApiProvider();
                                            await apiProvider12.getAllKuesioner().then((val) async {
                                            print("kuesioner from API $val");     
                                                var apiProvider13 = FolderApiProvider();
                                                  await apiProvider13.getAllFolder().then((val) async {
                                                  print("folder from API $val");                                           
                                                      var apiProvider14 = DocumentApiProvider();
                                                      await apiProvider14.getAllDocument().then((val){
                                                        print("document from API $val");                                           
                                                          setState(() {
                                                            isLoading = false;
                                                          });  
                                                      });     
                                                });                                                
                                            });   
                                        });                                             
                                  });                
                              });              
                          });                
                      });                 
                    });
                });
 
              });   
          });       
        }); 
    });

    // wait for 2 seconds to simulate loading of data
    // await Future.delayed(const Duration(seconds: 5));   
  }

 

  @override
  Widget build(BuildContext context) {
    var appBar = PreferredSize(
      preferredSize: Size.fromHeight(Utils().heightDivided(context, 10.0)),
      child : new AppBar(                       
            backgroundColor: Colors.white,                                 
            flexibleSpace: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,            
              children: <Widget>[
                 Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 100)),
                    child: Container(child: GestureDetector(
                                            child : Icon(Icons.menu,size: Utils().heightDivided(context, 25)),
                                            onTap: () => _scaffoldKey.currentState.openDrawer(),
                                            ),
                                            
                                    ),
                  ), 
                  Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().widthDivided(context, 60), right:Utils().widthDivided(context, 8) ),
                    child: Image.asset(
                      'assets/images/buchi_logo2.png',                                                    
                      height: Utils().heightDivided(context, 25),
                    ),
                  ), 
                  Padding(
                    padding: EdgeInsets.only(top : Utils().heightDivided(context, 20), left: Utils().screenWidth(context, 15)),
                    child: IconButton(
                      padding: EdgeInsets.all(5.0),
                      icon: Image.asset('assets/images/refresh.png'),
                      onPressed: () {
                        // Implement navigation to shopping cart page here...                        
                        // _loadFromPrice();
                        _loadFromApi();
                        print('Load From Api');
                      },
                    ),
                  ),                                            
                ],
             ),                                                                                                                                                 
      ),   
    );
    
    return Scaffold(
      key : _scaffoldKey,
      appBar: appBar,
      drawer: SideDrawer(),  
      body: 
     Container(
      height: Utils().height(context),
      child: Center(
      child: Stack(
        children: <Widget>[
          Container(
            color: Color.fromRGBO(148, 205, 12, 100),
          ),
          Padding(
            padding:  EdgeInsets.fromLTRB(    Utils().widthDivided(context, 40),
                                              Utils().heightDivided(context, 40),
                                              Utils().widthDivided(context, 40),
                                              Utils().heightDivided(context, 40)),
            child: Column(
              children: <Widget>[
                Card(
                  child : Container(               
                  padding: EdgeInsets.fromLTRB( Utils().widthDivided(context, 20),
                                                Utils().heightDivided(context, 40),
                                                Utils().widthDivided(context, 20),
                                                Utils().heightDivided(context, 40)),  
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[                  
                     Padding(
                       padding: const EdgeInsets.only(bottom : 30.0),
                       child: Text(
                        "Hello, welcome to Buchi Configuration!",
                        style: TextStyle(
                                fontSize: 60.0
                        ),
                       ),
                     ),
                     Text(
                      "The  easy way to configure your Sales Configuration, checking pricelist, manage your files and share quisioner.",
                       style: TextStyle(
                              fontSize: 32.0
                      ),
                     ),
                    ],                
                  ),                
                 ),
                ),
                (isLoading) ?  
                 Container(
                  height: Utils().screenHeight(context, 40),
                  child : Center(
                    child : 
                        SizedBox(
                                  child : CircularProgressIndicator(),
                                  height:  100.0,
                                  width:   100.0, 
                        ),    
                    )
                )  :      
                Column(
                children: <Widget>[               
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    
                  GestureDetector(
                      child :Padding(
                      padding: EdgeInsets.only(top  : Utils().heightDivided(context, 50),
                                               left : Utils().widthDivided(context, 25)),                                                
                            child: Card(
                            child : Container(
                            // width: Utils().screenWidth(context, 40),
                            // height: Utils().screenHeight(context, 40),               
                            padding: EdgeInsets.fromLTRB(Utils().widthDivided(context, 35),
                                                         Utils().heightDivided(context, 80),
                                                         Utils().widthDivided(context, 35),
                                                         Utils().heightDivided(context, 80)),  
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[                  
                              Padding(
                                padding: const EdgeInsets.only(bottom : 10.0),
                                child: Image.asset("assets/images/productset.png")
                              ),
                               Padding(
                                padding: EdgeInsets.only(bottom :  Utils().widthDivided(context, 80), left: Utils().widthDivided(context,18)),
                                child: Text(
                                  "Product Set",
                                  style: TextStyle(
                                          fontSize: 40.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                  ),
                                ),
                              ),                 
                              ],                
                            ),                
                          ),
                          ),
                       ),
                       onTap: () { print("ontap"); navigationPage();},
                     ),                                

                    GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(top : Utils().heightDivided(context, 50),
                                                 left: Utils().widthDivided(context, 25)),
                        child: Card(
                        child : Container(               
                        padding: EdgeInsets.fromLTRB(Utils().widthDivided(context, 35),
                                                      Utils().heightDivided(context, 80),
                                                      Utils().widthDivided(context, 35),
                                                      Utils().heightDivided(context, 80)),  
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[                  
                          Padding(
                            padding: const EdgeInsets.only(bottom : 10.0),
                            child: Image.asset("assets/images/pricelist.png")
                          ),
                            Padding(
                            padding: EdgeInsets.only(bottom :  Utils().widthDivided(context, 80), 
                                                     left: Utils().screenWidth(context, 7)),
                            child: Text(
                              "Price List",
                              style: TextStyle(
                                      fontSize: 40.0,
                                      fontWeight: FontWeight.bold,
                                      color : Color.fromRGBO(108,117, 125, 1.0)
                              ),
                            ),
                          ),                 
                          ],                
                        ),                
                  ),
                  ),
                  ),
                  onTap: () {
                      navigationPagePriceList();
                      print("ontap2");
                  },
                ),         
               ],
             ),

              Row(
                  children: <Widget>[
                      GestureDetector(
                        child: Padding(
                          padding: EdgeInsets.only(top : Utils().heightDivided(context, 60),
                          left: Utils().widthDivided(context, 7)),
                          child: Card(
                            child : Container(               
                            padding: EdgeInsets.fromLTRB(Utils().widthDivided(context, 35),
                                                          Utils().heightDivided(context, 80),
                                                          Utils().widthDivided(context, 35),
                                                          Utils().heightDivided(context, 80)),  
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[                  
                              Padding(
                                padding: const EdgeInsets.only(bottom : 10.0),
                                child: Image.asset("assets/images/shared.png")
                              ),
                                Padding(
                                  padding: EdgeInsets.only(bottom :  Utils().widthDivided(context, 80), 
                                                        left: Utils().widthDivided(context, 45)),
                                child: Text(
                                  "Service Engineer Files",
                                  style: TextStyle(
                                          fontSize: 40.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                  ),
                                ),
                                    ),                 
                                ],                
                              ),                
                            ),
                          ),
                        ),
                        onTap: () {
                          navigationPageMechanic();
                        },
                      ),

                      GestureDetector(
                        child: Padding(
                        padding: EdgeInsets.only(top : Utils().heightDivided(context, 60),
                                                 left: Utils().widthDivided(context, 23)),
                          child: Card(
                            child : Container(               
                            padding: EdgeInsets.fromLTRB( Utils().widthDivided(context, 35),
                                                          Utils().heightDivided(context, 80),
                                                          Utils().widthDivided(context, 35),
                                                          Utils().heightDivided(context, 80)),  
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[                  
                              Padding(
                                padding: const EdgeInsets.only(bottom : 10.0),
                                child: Image.asset("assets/images/kuesioner.png")
                              ),
                                Padding(
                                padding: EdgeInsets.only(bottom :  Utils().widthDivided(context, 80), left: Utils().screenWidth(context, 7)),
                                child: Text(
                                  "Kuesioner",
                                  style: TextStyle(
                                          fontSize: 40.0,
                                          fontWeight: FontWeight.bold,
                                          color : Color.fromRGBO(108,117, 125, 1.0)
                                  ),
                                ),
                                ),                 
                                ],                
                              ),                
                            ),
                          ),
                        ),
                        onTap: () {
                            navigationPageKuesioner();
                        },
                      ),                          
                ],
              )
                ]),
              ],
            ),
          )
        ],
      )    
    ),
     ),
    );
  }
}