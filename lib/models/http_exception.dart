class HttpException implements Exception {
  final String message;

  /// The URL of the HTTP request or response that failed.
  HttpException(this.message);

  @override
  String toString() {
    return message;
  }
}