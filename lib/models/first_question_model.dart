import 'dart:convert';

FirsQuestion firsQuestionFromJson(String str) => FirsQuestion.fromJson(json.decode(str));

String firsQuestionToJson(FirsQuestion data) => json.encode(data.toJson());

class FirsQuestion {
    Firstquestion firstquestion;

    FirsQuestion({
        this.firstquestion,
    });

    factory FirsQuestion.fromJson(Map<String, dynamic> json) => FirsQuestion(
        firstquestion: Firstquestion.fromJson(json["firstquestion"]),
    );

    Map<String, dynamic> toJson() => {
        "firstquestion": firstquestion.toJson(),
    };
}

class Firstquestion {
    int id;
    int idcategory;
    int idquestion;
    int yes;
    dynamic nonote;
    dynamic yesnote;
    int no;
    int start;
    int noend;
    int yesend;
    int status;
    DateTime createdAt;
    String updatedAt;

    Firstquestion({
        this.id,
        this.idcategory,
        this.idquestion,
        this.yes,
        this.nonote,
        this.yesnote,
        this.no,
        this.start,
        this.noend,
        this.yesend,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Firstquestion.fromJson(Map<String, dynamic> json) => Firstquestion(
        id: json["id"],
        idcategory: json["idcategory"],
        idquestion: json["idquestion"],
        yes: json["yes"],
        nonote: json["nonote"],
        yesnote: json["yesnote"],
        no: json["no"],
        start: json["start"],
        noend: json["noend"],
        yesend: json["yesend"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idcategory": idcategory,
        "idquestion": idquestion,
        "yes": yes,
        "nonote": nonote,
        "yesnote": yesnote,
        "no": no,
        "start": start,
        "noend": noend,
        "yesend": yesend,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
    };
}