import 'dart:async';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

  
class _SplashScreenState extends State<SplashScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
         color: Colors.white,
         child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
           crossAxisAlignment: CrossAxisAlignment.center,
           children: <Widget>[
             Padding(
               padding: const EdgeInsets.only(bottom : 50.0),
               child: Image(image: AssetImage('assets/images/buchi_logo.jpg'),
                            width: MediaQuery.of(context).size.width/2.0,
                            ),
             ),
             Card(
               color: Color.fromRGBO(234, 238, 239, 1.0),                             
               child: Padding(
                 padding: const EdgeInsets.all(70.0),
                 child: Text("BUCHI CONFIGURATION",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 80.0,
                                  color: Color.fromRGBO(109, 118, 126, 1.0)
                              ),
                            ),
               ), 
             )
           ],
         )
    );    
  }

  startTime() async { 
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);       
  }
  
  void navigationPage() {
     Navigator.of(context).pushReplacementNamed('/login');
  }

  
@override
void initState() {
  super.initState();
  startTime();
}


}