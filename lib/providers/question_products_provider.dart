import 'package:buchi/models/category_response.dart';
import 'package:buchi/models/question_products_model.dart';
import 'package:buchi/providers/db_provider.dart';
import 'package:buchi/utils/consts.dart';
import 'package:dio/dio.dart';
import 'dart:convert';


class QuestionProductApiProvider {
  Future<List<Questionproduct>> getAllQuestionProduct() async {
    var url = Consts.base_url+"questionproduct";
    // print("URL $url");
    Response response = await Dio().get(url); // fetch data json dari url   
    Map<String,dynamic> responseData;
    responseData = json.decode(response.data);   //response JSON data didecode agar bisa di parse     
    // print("APIPROVIDER ${responseData["category"]}");
    List responseJson =responseData["questionproduct"];  //ambil data json  category masukan ke variabel tipe data list  

    return (responseJson).map((questionproduct) {     
      // print('Inserting $questionproduct');
      DBProvider.db.createQuestionProduct(Questionproduct.fromJson(questionproduct));
    }).toList();

  }
}