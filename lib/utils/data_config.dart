import 'package:buchi/models/pricelist_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataConfig {
  final String _token= "token";
  final String _login= "login";
  static List<PriceList> listPrice = List();
  static int sizePriceList = 0;
  
   //Method that save the token 
  Future<void> setToken(String value) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_token, value);
  } 
  
  Future<String> getToken() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_token);
  } 

   //Method that save boolean login  
  Future<void> setLogin(dynamic value) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();    
    return prefs.setBool(_login, value);
  } 

  Future<dynamic> getLogin() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();    
    return prefs.getBool(_login);
  } 

  Future<void> setData(String key, String value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  Future<String> getData(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     var data =  prefs.getString(value);     
     return data;
  }  

  Future<void> setDataInt(String key, int value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
  }

  Future<int> getDataInt(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     var data =  prefs.getInt(value);     
     return data;
  }  

}