
import 'dart:convert';

LoginResponse welcomeFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String welcomeToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
    String name;
    String email;
    String accessToken;

    LoginResponse({
        this.name,
        this.email,
        this.accessToken,
    });

    factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        name: json["name"],
        email: json["email"],
        accessToken: json["access_token"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "access_token": accessToken,
    };
}
