class Products {
    List<Product> products;

    Products({
        this.products,
    });

    factory Products.fromJson(Map<String, dynamic> json) => Products(
        products: List<Product>.from(json["product"].map((x) => Products.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "product": List<dynamic>.from(products.map((x) => x.toJson())),
    };
}

class Product {
    int id;
    String name;
    int price;
    String keterangan;
    int status;
    DateTime createdAt;
    DateTime updatedAt;

    Product({
        this.id,
        this.name,
        this.price,
        this.keterangan,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        keterangan: json["keterangan"] == null ? null : json["keterangan"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "keterangan": keterangan == null ? null : keterangan,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
